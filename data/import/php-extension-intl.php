<?php

function collator_asort(Collator $coll, array &$arr, int $sort_flag = 0) : bool {}
function collator_compare(Collator $coll, string $str1, string $str2) : int {}
function collator_create(string $locale) {}
function collator_get_attribute(Collator $coll, int $attr) : int {}
function collator_get_error_code(Collator $coll) : int {}
function collator_get_error_message(Collator $coll) : string {}
function collator_get_locale(Collator $coll, int $type) : string {}
function collator_get_sort_key(Collator $coll, string $str) : string {}
function collator_get_strength(Collator $coll) : int {}
function collator_set_attribute(Collator $coll, int $attr, int $val) : bool {}
function collator_set_strength(Collator $coll, int $strength) : bool {}
function collator_sort(Collator $coll, array &$arr, int $sort_flag = 0) : bool {}
function collator_sort_with_sort_keys(Collator $coll, array &$arr) : bool {}
function datefmt_create(string $locale, int $datetype, int $timetype, $timezone = null, $calendar = null, string $pattern = "") {}
function datefmt_format(IntlDateFormatter $fmt, $value) : string {}
function datefmt_format_object(object $object, $format = null, string $locale = null) : string {}
function datefmt_get_calendar(IntlDateFormatter $fmt) : int {}
function datefmt_get_calendar_object() {}
function datefmt_get_datetype(IntlDateFormatter $fmt) : int {}
function datefmt_get_error_code(IntlDateFormatter $fmt) : int {}
function datefmt_get_error_message(IntlDateFormatter $fmt) : string {}
function datefmt_get_locale(IntlDateFormatter $fmt, int $which = 0) : string {}
function datefmt_get_pattern(IntlDateFormatter $fmt) : string {}
function datefmt_get_timetype(IntlDateFormatter $fmt) : int {}
function datefmt_get_timezone() {}
function datefmt_get_timezone_id(IntlDateFormatter $fmt) : string {}
function datefmt_is_lenient(IntlDateFormatter $fmt) : bool {}
function datefmt_localtime(IntlDateFormatter $fmt, string $value, int &$position = 0) : array {}
function datefmt_parse(IntlDateFormatter $fmt, string $value, int &$position = 0) : int {}
function datefmt_set_calendar(IntlDateFormatter $fmt, $which) : bool {}
function datefmt_set_lenient(IntlDateFormatter $fmt, bool $lenient) : bool {}
function datefmt_set_pattern(IntlDateFormatter $fmt, string $pattern) : bool {}
function datefmt_set_timezone($zone) : boolean {}
function datefmt_set_timezone_id(IntlDateFormatter $fmt, string $zone) : bool {}
function grapheme_extract(string $haystack, int $size, int $extract_type = 0, int $start = 0, int &$next = 0) : string {}
function grapheme_stripos(string $haystack, string $needle, int $offset = 0) : int {}
function grapheme_stristr(string $haystack, string $needle, bool $before_needle = false) : string {}
function grapheme_strlen(string $input) : int {}
function grapheme_strpos(string $haystack, string $needle, int $offset = 0) : int {}
function grapheme_strripos(string $haystack, string $needle, int $offset = 0) : int {}
function grapheme_strrpos(string $haystack, string $needle, int $offset = 0) : int {}
function grapheme_strstr(string $haystack, string $needle, bool $before_needle = false) : string {}
function grapheme_substr(string $string, int $start, int $length = 0) : int {}
function idn_to_ascii(string $domain, int $options = 0, int $variant = INTL_IDNA_VARIANT_2003, array &$idna_info = null) : string {}
function idn_to_utf8(string $domain, int $options = 0, int $variant = INTL_IDNA_VARIANT_2003, array &$idna_info = null) : string {}
function intl_error_name(int $error_code) : string {}
function intl_get_error_code() : int {}
function intl_get_error_message() : string {}
function intl_is_failure(int $error_code) : bool {}
function locale_accept_from_http(string $header) : string {}
function locale_compose(array $subtags) : string {}
function locale_filter_matches(string $langtag, string $locale, bool $canonicalize = false) : bool {}
function locale_get_all_variants(string $locale) : array {}
function locale_get_default() : string {}
function locale_get_display_language(string $locale, string $in_locale = '') : string {}
function locale_get_display_name(string $locale, string $in_locale = '') : string {}
function locale_get_display_region(string $locale, string $in_locale = '') : string {}
function locale_get_display_script(string $locale, string $in_locale = '') : string {}
function locale_get_display_variant(string $locale, string $in_locale = '') : string {}
function locale_get_keywords(string $locale) : array {}
function locale_get_primary_language(string $locale) : string {}
function locale_get_region(string $locale) : string {}
function locale_get_script(string $locale) : string {}
function locale_lookup(array $langtag, string $locale, bool $canonicalize = false, string $default = '') : string {}
function locale_parse(string $locale) : array {}
function locale_set_default(string $locale) : bool {}
function msgfmt_create(string $locale, string $pattern) {}
function msgfmt_format(MessageFormatter $fmt, array $args) : string {}
function msgfmt_format_message(string $locale, string $pattern, array $args) : string {}
function msgfmt_get_error_code(MessageFormatter $fmt) : int {}
function msgfmt_get_error_message(MessageFormatter $fmt) : string {}
function msgfmt_get_locale(NumberFormatter $formatter) : string {}
function msgfmt_get_pattern(MessageFormatter $fmt) : string {}
function msgfmt_parse(MessageFormatter $fmt, string $value) : array {}
function msgfmt_parse_message(string $locale, string $pattern, string $value) : array {}
function msgfmt_set_pattern(MessageFormatter $fmt, string $pattern) : bool {}
function normalizer_is_normalized(string $input, int $form = Normalizer::FORM_C) : bool {}
function normalizer_normalize(string $input, int $form = Normalizer::FORM_C) : string {}
function numfmt_create(string $locale, int $style, string $pattern = '') {}
function numfmt_format(NumberFormatter $fmt, number $value, int $type = 0) : string {}
function numfmt_format_currency(NumberFormatter $fmt, float $value, string $currency) : string {}
function numfmt_get_attribute(NumberFormatter $fmt, int $attr) : int {}
function numfmt_get_error_code(NumberFormatter $fmt) : int {}
function numfmt_get_error_message(NumberFormatter $fmt) : string {}
function numfmt_get_locale(NumberFormatter $fmt, int $type = 0) : string {}
function numfmt_get_pattern(NumberFormatter $fmt) : string {}
function numfmt_get_symbol(NumberFormatter $fmt, int $attr) : string {}
function numfmt_get_text_attribute(NumberFormatter $fmt, int $attr) : string {}
function numfmt_parse(NumberFormatter $fmt, string $value, int $type = 0, int &$position = 0) {}
function numfmt_parse_currency(NumberFormatter $fmt, string $value, string &$currency, int &$position = 0) : float {}
function numfmt_set_attribute(NumberFormatter $fmt, int $attr, int $value) : bool {}
function numfmt_set_pattern(NumberFormatter $fmt, string $pattern) : bool {}
function numfmt_set_symbol(NumberFormatter $fmt, int $attr, string $value) : bool {}
function numfmt_set_text_attribute(NumberFormatter $fmt, int $attr, string $value) : bool {}
function resourcebundle_count(ResourceBundle $r) : int {}
function resourcebundle_create(string $locale, string $bundlename, bool $fallback = false) {}
function resourcebundle_get(ResourceBundle $r, $index) {}
function resourcebundle_get_error_code(ResourceBundle $r) : int {}
function resourcebundle_get_error_message(ResourceBundle $r) : string {}
function resourcebundle_locales(string $bundlename) : array {}
function transliterator_create(string $id, int $direction = 0) {}
function transliterator_create_from_rules(string $id, int $direction = 0) {}
function transliterator_create_inverse() {}
function transliterator_get_error_code() : int {}
function transliterator_get_error_message() : string {}
function transliterator_list_ids() : array {}
function transliterator_transliterate($transliterator, string $subject, int $start = 0, int $end = 0) {}
