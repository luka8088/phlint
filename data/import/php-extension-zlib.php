<?php

const FORCE_DEFLATE = 15;
const FORCE_GZIP = 31;
const ZLIB_BLOCK = 5;
const ZLIB_DEFAULT_STRATEGY = 0;
const ZLIB_ENCODING_DEFLATE = 15;
const ZLIB_ENCODING_GZIP = 31;
const ZLIB_ENCODING_RAW = -15;
const ZLIB_FILTERED = 1;
const ZLIB_FINISH = 4;
const ZLIB_FIXED = 4;
const ZLIB_FULL_FLUSH = 3;
const ZLIB_HUFFMAN_ONLY = 2;
const ZLIB_NO_FLUSH = 0;
const ZLIB_PARTIAL_FLUSH = 1;
const ZLIB_RLE = 3;
const ZLIB_SYNC_FLUSH = 2;
const ZLIB_VERNUM = 4736;
const ZLIB_VERSION = '1.2.8';

function gzputs($fp, $str, $length) {}
