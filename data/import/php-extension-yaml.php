<?php

function yaml_emit($data, int $encoding = YAML_ANY_ENCODING, int $linebreak = YAML_ANY_BREAK, array $callbacks = null) : string {}
function yaml_emit_file(string $filename, $data, int $encoding = YAML_ANY_ENCODING, int $linebreak = YAML_ANY_BREAK, array $callbacks = null) : bool {}
function yaml_parse(string $input, int $pos = 0, int &$ndocs = 0, array $callbacks = null) {}
function yaml_parse_file(string $filename, int $pos = 0, int &$ndocs = 0, array $callbacks = null) {}
function yaml_parse_url(string $url, int $pos = 0, int &$ndocs = 0, array $callbacks = null) {}
