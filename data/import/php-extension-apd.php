<?php

function apd_breakpoint(int $debug_level) : bool {}
function apd_callstack() : array {}
function apd_clunk(string $warning, string $delimiter = "<BR />") {}
function apd_continue(int $debug_level) : bool {}
function apd_croak(string $warning, string $delimiter = "<BR />") {}
function apd_dump_function_table() {}
function apd_dump_persistent_resources() : array {}
function apd_dump_regular_resources() : array {}
function apd_echo(string $output) : bool {}
function apd_get_active_symbols() : array {}
function apd_set_pprof_trace(string $dump_directory = ini_get("apd.dumpdir"), string $fragment = "pprof") : string {}
function apd_set_session(int $debug_level) {}
function apd_set_session_trace(int $debug_level, string $dump_directory = ini_get("apd.dumpdir")) {}
function apd_set_session_trace_socket(string $tcp_server, int $socket_type, int $port, int $debug_level) : bool {}
function override_function(string $function_name, string $function_args, string $function_code) : bool {}
function rename_function(string $original_name, string $new_name) : bool {}
