<?php

/** @var array */ $_COOKIE = [];
/** @var array */ $_ENV = [];
/** @var array */ $_FILES = [];
/** @var array */ $_GET = [];
/** @var array */ $_POST = [];
/** @var array */ $_REQUEST = [];
/** @var array */ $_SERVER = [];
/** @var array */ $_SESSION = [];
/** @var array */ $GLOBALS = [];

const ARRAY_FILTER_USE_BOTH = 1;
const ARRAY_FILTER_USE_KEY = 2;
const ASSERT_ACTIVE = 1;
const ASSERT_BAIL = 3;
const ASSERT_CALLBACK = 2;
const ASSERT_EXCEPTION = 6;
const ASSERT_QUIET_EVAL = 5;
const ASSERT_WARNING = 4;
const CASE_LOWER = 0;
const CASE_UPPER = 1;
const CHAR_MAX = 127;
const CONNECTION_ABORTED = 1;
const CONNECTION_NORMAL = 0;
const CONNECTION_TIMEOUT = 2;
const COUNT_NORMAL = 0;
const COUNT_RECURSIVE = 1;
const CREDITS_ALL = 4294967295;
const CREDITS_DOCS = 16;
const CREDITS_FULLPAGE = 32;
const CREDITS_GENERAL = 2;
const CREDITS_GROUP = 1;
const CREDITS_MODULES = 8;
const CREDITS_QA = 64;
const CREDITS_SAPI = 4;
const CRYPT_BLOWFISH = 1;
const CRYPT_EXT_DES = 1;
const CRYPT_MD5 = 1;
const CRYPT_SALT_LENGTH = 123;
const CRYPT_SHA256 = 1;
const CRYPT_SHA512 = 1;
const CRYPT_STD_DES = 1;
const DATE_ATOM = 'Y-m-d\\TH:i:sP';
const DATE_COOKIE = 'l, d-M-Y H:i:s T';
const DATE_ISO8601 = 'Y-m-d\\TH:i:sO';
const DATE_RFC1036 = 'D, d M y H:i:s O';
const DATE_RFC1123 = 'D, d M Y H:i:s O';
const DATE_RFC2822 = 'D, d M Y H:i:s O';
const DATE_RFC3339 = 'Y-m-d\\TH:i:sP';
const DATE_RFC3339_EXTENDED = 'Y-m-d\\TH:i:s.vP';
const DATE_RFC822 = 'D, d M y H:i:s O';
const DATE_RFC850 = 'l, d-M-y H:i:s T';
const DATE_RSS = 'D, d M Y H:i:s O';
const DATE_W3C = 'Y-m-d\\TH:i:sP';
const DEBUG_BACKTRACE_IGNORE_ARGS = 2;
const DEBUG_BACKTRACE_PROVIDE_OBJECT = 1;
const DEFAULT_INCLUDE_PATH = '';
const DIRECTORY_SEPARATOR = '';
const DNS_A = 1;
const DNS_A6 = 16777216;
const DNS_AAAA = 134217728;
const DNS_ALL = 251713587;
const DNS_ANY = 268435456;
const DNS_CNAME = 16;
const DNS_HINFO = 4096;
const DNS_MX = 16384;
const DNS_NAPTR = 67108864;
const DNS_NS = 2;
const DNS_PTR = 2048;
const DNS_SOA = 32;
const DNS_SRV = 33554432;
const DNS_TXT = 32768;
const E_ALL = 32767;
const E_COMPILE_ERROR = 64;
const E_COMPILE_WARNING = 128;
const E_CORE_ERROR = 16;
const E_CORE_WARNING = 32;
const E_DEPRECATED = 8192;
const E_ERROR = 1;
const E_NOTICE = 8;
const E_PARSE = 4;
const E_RECOVERABLE_ERROR = 4096;
const E_STRICT = 2048;
const E_USER_DEPRECATED = 16384;
const E_USER_ERROR = 256;
const E_USER_NOTICE = 1024;
const E_USER_WARNING = 512;
const E_WARNING = 2;
const ENT_COMPAT = 2;
const ENT_DISALLOWED = 128;
const ENT_HTML401 = 0;
const ENT_HTML5 = 48;
const ENT_IGNORE = 4;
const ENT_NOQUOTES = 0;
const ENT_QUOTES = 3;
const ENT_SUBSTITUTE = 8;
const ENT_XHTML = 32;
const ENT_XML1 = 16;
const EXTR_IF_EXISTS = 6;
const EXTR_OVERWRITE = 0;
const EXTR_PREFIX_ALL = 3;
const EXTR_PREFIX_IF_EXISTS = 5;
const EXTR_PREFIX_INVALID = 4;
const EXTR_PREFIX_SAME = 2;
const EXTR_REFS = 256;
const EXTR_SKIP = 1;
const FILE_APPEND = 8;
const FILE_BINARY = 0;
const FILE_IGNORE_NEW_LINES = 2;
const FILE_NO_DEFAULT_CONTEXT = 16;
const FILE_SKIP_EMPTY_LINES = 4;
const FILE_TEXT = 0;
const FILE_USE_INCLUDE_PATH = 1;
const FNM_CASEFOLD = 16;
const FNM_NOESCAPE = 1;
const FNM_PATHNAME = 2;
const FNM_PERIOD = 4;
const GLOB_AVAILABLE_FLAGS = 1073746108;
const GLOB_BRACE = 128;
const GLOB_ERR = 4;
const GLOB_MARK = 8;
const GLOB_NOCHECK = 16;
const GLOB_NOESCAPE = 4096;
const GLOB_NOSORT = 32;
const GLOB_ONLYDIR = 1073741824;
const HTML_ENTITIES = 1;
const HTML_SPECIALCHARS = 0;
const IMAGETYPE_BMP = 6;
const IMAGETYPE_COUNT = 18;
const IMAGETYPE_GIF = 1;
const IMAGETYPE_ICO = 17;
const IMAGETYPE_IFF = 14;
const IMAGETYPE_JB2 = 12;
const IMAGETYPE_JP2 = 10;
const IMAGETYPE_JPC = 9;
const IMAGETYPE_JPEG = 2;
const IMAGETYPE_JPEG2000 = 9;
const IMAGETYPE_JPX = 11;
const IMAGETYPE_PNG = 3;
const IMAGETYPE_PSD = 5;
const IMAGETYPE_SWC = 13;
const IMAGETYPE_SWF = 4;
const IMAGETYPE_TIFF_II = 7;
const IMAGETYPE_TIFF_MM = 8;
const IMAGETYPE_UNKNOWN = 0;
const IMAGETYPE_WBMP = 15;
const IMAGETYPE_XBM = 16;
const INFO_ALL = 4294967295;
const INFO_CONFIGURATION = 4;
const INFO_CREDITS = 2;
const INFO_ENVIRONMENT = 16;
const INFO_GENERAL = 1;
const INFO_LICENSE = 64;
const INFO_MODULES = 8;
const INFO_VARIABLES = 32;
const INI_ALL = 7;
const INI_PERDIR = 2;
const INI_SCANNER_NORMAL = 0;
const INI_SCANNER_RAW = 1;
const INI_SCANNER_TYPED = 2;
const INI_SYSTEM = 4;
const INI_USER = 1;
const JSON_BIGINT_AS_STRING = 2;
const JSON_ERROR_CTRL_CHAR = 3;
const JSON_ERROR_DEPTH = 1;
const JSON_ERROR_INF_OR_NAN = 7;
const JSON_ERROR_INVALID_PROPERTY_NAME = 9;
const JSON_ERROR_NONE = 0;
const JSON_ERROR_RECURSION = 6;
const JSON_ERROR_STATE_MISMATCH = 2;
const JSON_ERROR_SYNTAX = 4;
const JSON_ERROR_UNSUPPORTED_TYPE = 8;
const JSON_ERROR_UTF16 = 10;
const JSON_ERROR_UTF8 = 5;
const JSON_FORCE_OBJECT = 16;
const JSON_HEX_AMP = 2;
const JSON_HEX_APOS = 4;
const JSON_HEX_QUOT = 8;
const JSON_HEX_TAG = 1;
const JSON_NUMERIC_CHECK = 32;
const JSON_OBJECT_AS_ARRAY = 1;
const JSON_PARTIAL_OUTPUT_ON_ERROR = 512;
const JSON_PRESERVE_ZERO_FRACTION = 1024;
const JSON_PRETTY_PRINT = 128;
const JSON_UNESCAPED_SLASHES = 64;
const JSON_UNESCAPED_UNICODE = 256;
const LC_ALL = 0;
const LC_COLLATE = 1;
const LC_CTYPE = 2;
const LC_MONETARY = 3;
const LC_NUMERIC = 4;
const LC_TIME = 5;
const LOCK_EX = 2;
const LOCK_NB = 4;
const LOCK_SH = 1;
const LOCK_UN = 3;
const LOG_ALERT = 1;
const LOG_AUTH = 32;
const LOG_AUTHPRIV = 80;
const LOG_CONS = 2;
const LOG_CRIT = 1;
const LOG_CRON = 72;
const LOG_DAEMON = 24;
const LOG_DEBUG = 6;
const LOG_EMERG = 1;
const LOG_ERR = 4;
const LOG_INFO = 6;
const LOG_KERN = 0;
const LOG_LPR = 48;
const LOG_MAIL = 16;
const LOG_NDELAY = 8;
const LOG_NEWS = 56;
const LOG_NOTICE = 6;
const LOG_NOWAIT = 16;
const LOG_ODELAY = 4;
const LOG_PERROR = 32;
const LOG_PID = 1;
const LOG_SYSLOG = 40;
const LOG_USER = 8;
const LOG_UUCP = 64;
const LOG_WARNING = 5;
const M_1_PI = 0.3183098861837907;
const M_2_PI = 0.6366197723675814;
const M_2_SQRTPI = 1.1283791670955126;
const M_E = 2.718281828459045;
const M_EULER = 0.5772156649015329;
const M_LN10 = 2.302585092994046;
const M_LN2 = 0.6931471805599453;
const M_LNPI = 1.1447298858494002;
const M_LOG10E = 0.4342944819032518;
const M_LOG2E = 1.4426950408889634;
const M_PI = 3.141592653589793;
const M_PI_2 = 1.5707963267948966;
const M_PI_4 = 0.7853981633974483;
const M_SQRT1_2 = 0.7071067811865476;
const M_SQRT2 = 1.4142135623730951;
const M_SQRT3 = 1.7320508075688772;
const M_SQRTPI = 1.772453850905516;
const PASSWORD_BCRYPT = 1;
const PASSWORD_BCRYPT_DEFAULT_COST = 10;
const PASSWORD_DEFAULT = 1;
const PATH_SEPARATOR = ';';
const PATHINFO_BASENAME = 2;
const PATHINFO_DIRNAME = 1;
const PATHINFO_EXTENSION = 4;
const PATHINFO_FILENAME = 8;
const PEAR_EXTENSION_DIR = '';
const PEAR_INSTALL_DIR = '';
const PHP_BINARY = '';
const PHP_BINDIR = '';
const PHP_CONFIG_FILE_PATH = '';
const PHP_CONFIG_FILE_SCAN_DIR = '';
const PHP_DATADIR = '';
const PHP_DEBUG = 0;
const PHP_EOL = "\n";
const PHP_EXTENSION_DIR = '';
const PHP_EXTRA_VERSION = '';
const PHP_INT_MAX = 9223372036854775807;
const PHP_INT_MIN = -9.223372036854776E+18;
const PHP_INT_SIZE = 8;
const PHP_LIBDIR = '';
const PHP_LOCALSTATEDIR = '';
const PHP_MAJOR_VERSION = 7;
const PHP_MAXPATHLEN = 260;
const PHP_MINOR_VERSION = 0;
const PHP_OS = '';
const PHP_OUTPUT_HANDLER_CLEAN = 2;
const PHP_OUTPUT_HANDLER_CLEANABLE = 16;
const PHP_OUTPUT_HANDLER_CONT = 0;
const PHP_OUTPUT_HANDLER_DISABLED = 8192;
const PHP_OUTPUT_HANDLER_END = 8;
const PHP_OUTPUT_HANDLER_FINAL = 8;
const PHP_OUTPUT_HANDLER_FLUSH = 4;
const PHP_OUTPUT_HANDLER_FLUSHABLE = 32;
const PHP_OUTPUT_HANDLER_REMOVABLE = 64;
const PHP_OUTPUT_HANDLER_START = 1;
const PHP_OUTPUT_HANDLER_STARTED = 4096;
const PHP_OUTPUT_HANDLER_STDFLAGS = 112;
const PHP_OUTPUT_HANDLER_WRITE = 0;
const PHP_PREFIX = '';
const PHP_QUERY_RFC1738 = 1;
const PHP_QUERY_RFC3986 = 2;
const PHP_RELEASE_VERSION = 9;
const PHP_ROUND_HALF_DOWN = 2;
const PHP_ROUND_HALF_EVEN = 3;
const PHP_ROUND_HALF_ODD = 4;
const PHP_ROUND_HALF_UP = 1;
const PHP_SAPI = '';
const PHP_SHLIB_SUFFIX = '';
const PHP_SYSCONFDIR = '';
const PHP_URL_FRAGMENT = 7;
const PHP_URL_HOST = 1;
const PHP_URL_PASS = 4;
const PHP_URL_PATH = 5;
const PHP_URL_PORT = 2;
const PHP_URL_QUERY = 6;
const PHP_URL_SCHEME = 0;
const PHP_URL_USER = 3;
const PHP_VERSION = '';
const PHP_VERSION_ID = 0;
const PHP_WINDOWS_NT_DOMAIN_CONTROLLER = 2;
const PHP_WINDOWS_NT_SERVER = 3;
const PHP_WINDOWS_NT_WORKSTATION = 1;
const PHP_WINDOWS_VERSION_BUILD = 10586;
const PHP_WINDOWS_VERSION_MAJOR = 10;
const PHP_WINDOWS_VERSION_MINOR = 0;
const PHP_WINDOWS_VERSION_PLATFORM = 2;
const PHP_WINDOWS_VERSION_PRODUCTTYPE = 1;
const PHP_WINDOWS_VERSION_SP_MAJOR = 0;
const PHP_WINDOWS_VERSION_SP_MINOR = 0;
const PHP_WINDOWS_VERSION_SUITEMASK = 256;
const PHP_ZTS = 1;
const PSFS_ERR_FATAL = 0;
const PSFS_FEED_ME = 1;
const PSFS_FLAG_FLUSH_CLOSE = 2;
const PSFS_FLAG_FLUSH_INC = 1;
const PSFS_FLAG_NORMAL = 0;
const PSFS_PASS_ON = 2;
const SCANDIR_SORT_ASCENDING = 0;
const SCANDIR_SORT_DESCENDING = 1;
const SCANDIR_SORT_NONE = 2;
const SEEK_CUR = 1;
const SEEK_END = 2;
const SEEK_SET = 0;
const SORT_ASC = 4;
const SORT_DESC = 3;
const SORT_FLAG_CASE = 8;
const SORT_LOCALE_STRING = 5;
const SORT_NATURAL = 6;
const SORT_NUMERIC = 1;
const SORT_REGULAR = 0;
const SORT_STRING = 2;
const STDERR = NULL;
const STDIN = NULL;
const STDOUT = NULL;
const STR_PAD_BOTH = 2;
const STR_PAD_LEFT = 0;
const STR_PAD_RIGHT = 1;
const STREAM_BUFFER_FULL = 2;
const STREAM_BUFFER_LINE = 1;
const STREAM_BUFFER_NONE = 0;
const STREAM_CAST_AS_STREAM = 0;
const STREAM_CAST_FOR_SELECT = 3;
const STREAM_CLIENT_ASYNC_CONNECT = 2;
const STREAM_CLIENT_CONNECT = 4;
const STREAM_CLIENT_PERSISTENT = 1;
const STREAM_CRYPTO_METHOD_ANY_CLIENT = 63;
const STREAM_CRYPTO_METHOD_ANY_SERVER = 62;
const STREAM_CRYPTO_METHOD_SSLv23_CLIENT = 57;
const STREAM_CRYPTO_METHOD_SSLv23_SERVER = 56;
const STREAM_CRYPTO_METHOD_SSLv2_CLIENT = 3;
const STREAM_CRYPTO_METHOD_SSLv2_SERVER = 2;
const STREAM_CRYPTO_METHOD_SSLv3_CLIENT = 5;
const STREAM_CRYPTO_METHOD_SSLv3_SERVER = 4;
const STREAM_CRYPTO_METHOD_TLS_CLIENT = 9;
const STREAM_CRYPTO_METHOD_TLS_SERVER = 8;
const STREAM_CRYPTO_METHOD_TLSv1_0_CLIENT = 9;
const STREAM_CRYPTO_METHOD_TLSv1_0_SERVER = 8;
const STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT = 17;
const STREAM_CRYPTO_METHOD_TLSv1_1_SERVER = 16;
const STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT = 33;
const STREAM_CRYPTO_METHOD_TLSv1_2_SERVER = 32;
const STREAM_FILTER_ALL = 3;
const STREAM_FILTER_READ = 1;
const STREAM_FILTER_WRITE = 2;
const STREAM_IGNORE_URL = 2;
const STREAM_IPPROTO_IP = 0;
const STREAM_IS_URL = 1;
const STREAM_META_ACCESS = 6;
const STREAM_META_GROUP = 5;
const STREAM_META_GROUP_NAME = 4;
const STREAM_META_OWNER = 3;
const STREAM_META_OWNER_NAME = 2;
const STREAM_META_TOUCH = 1;
const STREAM_MKDIR_RECURSIVE = 1;
const STREAM_MUST_SEEK = 16;
const STREAM_NOTIFY_AUTH_REQUIRED = 3;
const STREAM_NOTIFY_AUTH_RESULT = 10;
const STREAM_NOTIFY_COMPLETED = 8;
const STREAM_NOTIFY_CONNECT = 2;
const STREAM_NOTIFY_FAILURE = 9;
const STREAM_NOTIFY_FILE_SIZE_IS = 5;
const STREAM_NOTIFY_MIME_TYPE_IS = 4;
const STREAM_NOTIFY_PROGRESS = 7;
const STREAM_NOTIFY_REDIRECTED = 6;
const STREAM_NOTIFY_RESOLVE = 1;
const STREAM_NOTIFY_SEVERITY_ERR = 2;
const STREAM_NOTIFY_SEVERITY_INFO = 0;
const STREAM_NOTIFY_SEVERITY_WARN = 1;
const STREAM_OOB = 1;
const STREAM_OPTION_BLOCKING = 1;
const STREAM_OPTION_READ_BUFFER = 2;
const STREAM_OPTION_READ_TIMEOUT = 4;
const STREAM_OPTION_WRITE_BUFFER = 3;
const STREAM_PEEK = 2;
const STREAM_PF_INET = 2;
const STREAM_PF_INET6 = 23;
const STREAM_PF_UNIX = 1;
const STREAM_REPORT_ERRORS = 8;
const STREAM_SERVER_BIND = 4;
const STREAM_SERVER_LISTEN = 8;
const STREAM_SHUT_RD = 0;
const STREAM_SHUT_RDWR = 2;
const STREAM_SHUT_WR = 1;
const STREAM_SOCK_DGRAM = 2;
const STREAM_SOCK_RAW = 3;
const STREAM_SOCK_RDM = 4;
const STREAM_SOCK_SEQPACKET = 5;
const STREAM_SOCK_STREAM = 1;
const STREAM_URL_STAT_LINK = 1;
const STREAM_URL_STAT_QUIET = 2;
const STREAM_USE_PATH = 1;
const SUNFUNCS_RET_DOUBLE = 2;
const SUNFUNCS_RET_STRING = 1;
const SUNFUNCS_RET_TIMESTAMP = 0;
const UPLOAD_ERR_CANT_WRITE = 7;
const UPLOAD_ERR_EXTENSION = 8;
const UPLOAD_ERR_FORM_SIZE = 2;
const UPLOAD_ERR_INI_SIZE = 1;
const UPLOAD_ERR_NO_FILE = 4;
const UPLOAD_ERR_NO_TMP_DIR = 6;
const UPLOAD_ERR_OK = 0;
const UPLOAD_ERR_PARTIAL = 3;

function abs($number) {}
function acos(float $arg) : float {}
function acosh(float $arg) : float {}
function addcslashes(string $str, string $charlist) : string {}
function addslashes(string $str) : string {}
function array_change_key_case(array $array, int $case = CASE_LOWER) : array {}
function array_chunk(array $array, int $size, bool $preserve_keys = false) : array {}
function array_column(array $input, $column_key, $index_key = null) : array {}
function array_combine(array $keys, array $values) : array {}
function array_count_values(array $array) : array {}

/**
 * @isolated - Invokes `__toString` on array values if they are objects.
 */
function array_diff(array $array1, array $array2, array ...$__variadic) : array {}

function array_diff_assoc(array $array1, array $array2, array ...$__variadic) : array {}
function array_diff_key(array $array1, array $array2, array ...$__variadic) : array {}
function array_diff_uassoc(array $array1, array $array2, array ...$__variadic, callable $key_compare_func = null) : array {}
function array_diff_ukey(array $array1, array $array2, array ...$__variadic, callable $key_compare_func = null) : array {}
function array_fill(int $start_index, int $num, $value) : array {}
function array_fill_keys(array $keys, $value) : array {}
function array_filter(array $array, callable $callback = null, int $flag = 0) : array {}
function array_flip(array $array) : array {}
function array_intersect(array $array1, array $array2, array ...$__variadic) : array {}
function array_intersect_assoc(array $array1, array $array2, array ...$__variadic) : array {}
function array_intersect_key(array $array1, array $array2, array ...$__variadic) : array {}
function array_intersect_uassoc(array $array1, array $array2, array ...$__variadic, callable $key_compare_func = null) : array {}
function array_intersect_ukey(array $array1, array $array2, array ...$__variadic, callable $key_compare_func = null) : array {}
function array_key_exists($key, array $array) : bool {}
function array_keys(array $array, $search_value = null, bool $strict = false) : array {}
function array_map(callable $callback, array $array1, array ...$__variadic) : array {}
function array_merge(array $array1, array ...$__variadic) : array {}
function array_merge_recursive(array $array1, array ...$__variadic) : array {}
function array_multisort(array &$array1, $array1_sort_order = SORT_ASC, $array1_sort_flags = SORT_REGULAR, ...$__variadic) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function array_pad(array $array, int $size, $value) : array {}
function array_pop(array &$array) {}
function array_product(array $array) {}
function array_push(array &$array, $value1, ...$__variadic) : int {}
function array_rand(array $array, int $num = 1) {}
function array_reduce(array $array, callable $callback, $initial = null) {}
function array_replace(array $array1, array $array2, array ...$__variadic) : array {}
function array_replace_recursive(array $array1, array $array2, array ...$__variadic) : array {}
function array_reverse(array $array, bool $preserve_keys = false) : array {}
function array_search($needle, array $haystack, bool $strict = false) {}
function array_shift(array &$array) {}
function array_slice(array $array, int $offset, int $length = null, bool $preserve_keys = false) : array {}
function array_splice(array &$input, int $offset, int $length = count($input), $replacement = null) : array {}
function array_sum(array $array) {}
function array_udiff(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null) : array {}
function array_udiff_assoc(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null) : array {}
function array_udiff_uassoc(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null, callable $key_compare_func = null) : array {}
function array_uintersect(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null) : array {}
function array_uintersect_assoc(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null) : array {}
function array_uintersect_uassoc(array $array1, array $array2, array ...$__variadic, callable $value_compare_func = null, callable $key_compare_func = null) : array {}
function array_unique(array $array, int $sort_flags = SORT_STRING) : array {}
function array_unshift(array &$array, $value1, ...$__variadic) : int {}
function array_values(array $array) : array {}
function array_walk(array &$array, callable $callback, $userdata = null) : bool {}
function array_walk_recursive(array &$array, callable $callback, $userdata = null) : bool {}
function arsort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function asin(float $arg) : float {}
function asinh(float $arg) : float {}
function asort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function assert($assertion, Throwable $exception = null) : bool {}
function assert_options(int $what, $value = null) {}
function atan(float $arg) : float {}
function atan2(float $y, float $x) : float {}
function atanh(float $arg) : float {}
function base64_decode(string $data, bool $strict = false) : string {}
function base64_encode(string $data) : string {}
function base_convert(string $number, int $frombase, int $tobase) : string {}
function basename(string $path, string $suffix = '') : string {}
function bcadd(string $left_operand, string $right_operand, int $scale = 0) : string {}
function bccomp(string $left_operand, string $right_operand, int $scale = 0) : int {}
function bcdiv(string $left_operand, string $right_operand, int $scale = 0) : string {}
function bcmod(string $left_operand, string $modulus) : string {}
function bcmul(string $left_operand, string $right_operand, int $scale = 0) : string {}
function bcpow(string $left_operand, string $right_operand, int $scale = 0) : string {}
function bcpowmod(string $left_operand, string $right_operand, string $modulus, int $scale = 0) : string {}
function bcscale(int $scale) : bool {}
function bcsqrt(string $operand, int $scale = 0) : string {}
function bcsub(string $left_operand, string $right_operand, int $scale = 0) : string {}
function bin2hex(string $str) : string {}
function bindec(string $binary_string) {}
function boolval($var) : boolean {}
function bzclose(resource $bz) : int {}
function bzcompress(string $source, int $blocksize = 4, int $workfactor = 0) {}
function bzdecompress(string $source, int $small = 0) {}
function bzerrno(resource $bz) : int {}
function bzerror(resource $bz) : array {}
function bzerrstr(resource $bz) : string {}
function bzflush(resource $bz) : bool {}
function bzopen($file, string $mode) : resource {}
function bzread(resource $bz, int $length = 1024) : string {}
function bzwrite(resource $bz, string $data, int $length = 0) : int {}
function cal_days_in_month(int $calendar, int $month, int $year) : int {}
function cal_from_jd(int $jd, int $calendar) : array {}
function cal_info(int $calendar = -1) : array {}
function cal_to_jd(int $calendar, int $month, int $day, int $year) : int {}
function call_user_func(callable $callback, $parameter = null, ...$__variadic) {}
function call_user_func_array(callable $callback, array $param_arr) {}
function ceil(float $value) : float {}
function chdir(string $directory) : bool {}
function checkdate(int $month, int $day, int $year) : bool {}
function checkdnsrr(string $host, string $type = "MX") : bool {}
function chgrp(string $filename, $group) : bool {}
function chmod(string $filename, int $mode) : bool {}
function chop($str, $character_mask) {}
function chown(string $filename, $user) : bool {}
function chr(int $ascii) : string {}
function chunk_split(string $body, int $chunklen = 76, string $end = "\r\n") : string {}
function class_alias(string $original, string $alias, bool $autoload = true) : bool {}
function class_exists(string $class_name, bool $autoload = true) : bool {}
function class_implements($class, bool $autoload = true) : array {}
function class_parents($class, bool $autoload = true) : array {}
function class_uses($class, bool $autoload = true) : array {}
function clearstatcache(bool $clear_realpath_cache = false, string $filename = '') {}
function cli_get_process_title() : string {}
function cli_set_process_title(string $title) : bool {}
function closedir(resource $dir_handle = null) {}
function closelog() : bool {}
function compact($varname1, ...$__variadic) : array {}
function connection_aborted() : int {}
function connection_status() : int {}
function constant(string $name) {}
function convert_cyr_string(string $str, string $from, string $to) : string {}
function convert_uudecode(string $data) : string {}
function convert_uuencode(string $data) : string {}
function copy(string $source, string $dest, resource $context = null) : bool {}
function cos(float $arg) : float {}
function cosh(float $arg) : float {}
function count($array_or_countable, int $mode = COUNT_NORMAL) : int {}
function count_chars(string $string, int $mode = 0) {}
function crc32(string $str) : int {}
function create_function(string $args, string $code) : string {}
function crypt(string $str, string $salt = '') : string {}
function ctype_alnum(string $text) : bool {}
function ctype_alpha(string $text) : bool {}
function ctype_cntrl(string $text) : bool {}
function ctype_digit(string $text) : bool {}
function ctype_graph(string $text) : bool {}
function ctype_lower(string $text) : bool {}
function ctype_print(string $text) : bool {}
function ctype_punct(string $text) : bool {}
function ctype_space(string $text) : bool {}
function ctype_upper(string $text) : bool {}
function ctype_xdigit(string $text) : bool {}
function curl_close(resource $ch) {}
function curl_copy_handle(resource $ch) : resource {}
function curl_errno(resource $ch) : int {}
function curl_error(resource $ch) : string {}
function curl_escape(resource $ch, string $str) : string {}
function curl_exec(resource $ch) {}
function curl_file_create(string $filename, string $mimetype = '', string $postname = '') {}
function curl_getinfo(resource $ch, int $opt = 0) {}
function curl_init(string $url = null) : resource {}
function curl_multi_add_handle(resource $mh, resource $ch) : int {}
function curl_multi_close(resource $mh) {}
function curl_multi_exec(resource $mh, int &$still_running) : int {}
function curl_multi_getcontent(resource $ch) : string {}
function curl_multi_info_read(resource $mh, int &$msgs_in_queue = null) : array {}
function curl_multi_init() : resource {}
function curl_multi_remove_handle(resource $mh, resource $ch) : int {}
function curl_multi_select(resource $mh, float $timeout = 1.0) : int {}
function curl_multi_setopt(resource $mh, int $option, $value) : bool {}
function curl_multi_strerror(int $errornum) : string {}
function curl_pause(resource $ch, int $bitmask) : int {}
function curl_reset(resource $ch) {}
function curl_setopt(resource $ch, int $option, $value) : bool {}
function curl_setopt_array(resource $ch, array $options) : bool {}
function curl_share_close(resource $sh) {}
function curl_share_init() : resource {}
function curl_share_setopt(resource $sh, int $option, string $value) : bool {}
function curl_strerror(int $errornum) : string {}
function curl_unescape(resource $ch, string $str) : string {}
function curl_version(int $age = CURLVERSION_NOW) : array {}
function current(array &$array) {}
function date(string $format, int $timestamp = 0) : string {}
function date_add(DateTime $object, DateInterval $interval) {}
function date_create(string $time = "now", DateTimeZone $timezone = null) {}
function date_create_from_format(string $format, string $time, DateTimeZone $timezone = null) {}
function date_create_immutable(string $time = "now", DateTimeZone $timezone = null) {}
function date_create_immutable_from_format(string $format, string $time, DateTimeZone $timezone = null) {}
function date_date_set(DateTime $object, int $year, int $month, int $day) {}
function date_default_timezone_get() : string {}
function date_default_timezone_set(string $timezone_identifier) : bool {}
function date_diff(DateTimeInterface $datetime1, DateTimeInterface $datetime2, bool $absolute = false) {}
function date_format(DateTimeInterface $object, string $format) : string {}
function date_get_last_errors() : array {}
function date_interval_create_from_date_string($time) {}
function date_interval_format($object, $format) {}
function date_isodate_set(DateTime $object, int $year, int $week, int $day = 1) {}
function date_modify(DateTime $object, string $modify) {}
function date_offset_get(DateTimeInterface $object) : int {}
function date_parse(string $date) : array {}
function date_parse_from_format(string $format, string $date) : array {}
function date_sub(DateTime $object, DateInterval $interval) {}
function date_sun_info(int $time, float $latitude, float $longitude) : array {}
function date_sunrise(int $timestamp, int $format = SUNFUNCS_RET_STRING, float $latitude = ini_get("date.default_latitude"), float $longitude = ini_get("date.default_longitude"), float $zenith = ini_get("date.sunrise_zenith"), float $gmt_offset = 0) {}
function date_sunset(int $timestamp, int $format = SUNFUNCS_RET_STRING, float $latitude = ini_get("date.default_latitude"), float $longitude = ini_get("date.default_longitude"), float $zenith = ini_get("date.sunset_zenith"), float $gmt_offset = 0) {}
function date_time_set(DateTime $object, int $hour, int $minute, int $second = 0) {}
function date_timestamp_get(DateTimeInterface $object) : int {}
function date_timestamp_set(DateTime $object, int $unixtimestamp) {}
function date_timezone_get(DateTimeInterface $object) {}
function date_timezone_set(DateTime $object, DateTimeZone $timezone) {}
function debug_backtrace(int $options = DEBUG_BACKTRACE_PROVIDE_OBJECT, int $limit = 0) : array {}
function debug_print_backtrace(int $options = 0, int $limit = 0) {}
function debug_zval_dump($variable, ...$__variadic) {}
function decbin(int $number) : string {}
function dechex(int $number) : string {}
function decoct(int $number) : string {}
function define(string $name, $value, bool $case_insensitive = false) : bool {}
function defined(string $name) : bool {}
function deflate_add(resource $context, string $data, int $flush_mode = ZLIB_SYNC_FLUSH) : string {}
function deflate_init(int $encoding, array $options = null) : resource {}
function deg2rad(float $number) : float {}
function dir(string $directory, resource $context = null) {}
function dirname(string $path, int $levels = 1) : string {}
function disk_free_space(string $directory) : float {}
function disk_total_space(string $directory) : float {}
function diskfreespace($path) {}
function dl(string $library) : bool {}
function dns_check_record($host, $type) {}
function dns_get_mx($hostname, $mxhosts, $weight) {}
function dns_get_record(string $hostname, int $type = DNS_ANY, array &$authns = null, array &$addtl = null, bool $raw = false) : array {}
function dom_import_simplexml(SimpleXMLElement $node) {}
function doubleval($var) {}
function each(array &$array) : array {}
function easter_date(int $year = date("Y")) : int {}
function easter_days(int $year = date("Y"), int $method = CAL_EASTER_DEFAULT) : int {}
function end(array &$array) {}
function error_clear_last() {}
function error_get_last() : array {}
function error_log(string $message, int $message_type = 0, string $destination = '', string $extra_headers = '') : bool {}
function error_reporting(int $level = 0) : int {}
function escapeshellarg(string $arg) : string {}
function escapeshellcmd(string $command) : string {}
function exec(string $command, array &$output = null, int &$return_var = 0) : string {}
function exp(float $arg) : float {}
function explode(string $delimiter, string $string, int $limit = PHP_INT_MAX) : array {}
function expm1(float $arg) : float {}
function extension_loaded(string $name) : bool {}
function extract(array &$array, int $flags = EXTR_OVERWRITE, string $prefix = null) : int {}
function ezmlm_hash(string $addr) : int {}
function fclose(resource $handle) : bool {}
function feof(resource $handle) : bool {}
function fflush(resource $handle) : bool {}
function fgetc(resource $handle) : string {}
function fgetcsv(resource $handle, int $length = 0, string $delimiter = ",", string $enclosure = '"', string $escape = "\\") : array {}
function fgets(resource $handle, int $length = 0) : string {}
function fgetss(resource $handle, int $length = 0, string $allowable_tags = '') : string {}
function file(string $filename, int $flags = 0, resource $context = null) : array {}
function file_exists(string $filename) : bool {}
function file_get_contents(string $filename, bool $use_include_path = false, resource $context = null, int $offset = 0, int $maxlen = 0) : string {}
function file_put_contents(string $filename, $data, int $flags = 0, resource $context = null) : int {}
function fileatime(string $filename) : int {}
function filectime(string $filename) : int {}
function filegroup(string $filename) : int {}
function fileinode(string $filename) : int {}
function filemtime(string $filename) : int {}
function fileowner(string $filename) : int {}
function fileperms(string $filename) : int {}
function filesize(string $filename) : int {}
function filetype(string $filename) : string {}
function filter_has_var(int $type, string $variable_name) : bool {}
function filter_id(string $filtername) : int {}
function filter_input(int $type, string $variable_name, int $filter = FILTER_DEFAULT, $options = null) {}
function filter_input_array(int $type, $definition = null, bool $add_empty = true) {}
function filter_list() : array {}
function filter_var($variable, int $filter = FILTER_DEFAULT, $options = null) {}
function filter_var_array(array $data, $definition = null, bool $add_empty = true) {}
function floatval($var) : float {}
function flock(resource $handle, int $operation, int &$wouldblock = 0) : bool {}
function floor(float $value) {}
function flush() {}
function fmod(float $x, float $y) : float {}
function fnmatch(string $pattern, string $string, int $flags = 0) : bool {}
function fopen(string $filename, string $mode, bool $use_include_path = false, resource $context = null) : resource {}
function forward_static_call(callable $function, $parameter = null, ...$__variadic) {}
function forward_static_call_array(callable $function, array $parameters) {}
function fpassthru(resource $handle) : int {}
function fprintf(resource $handle, string $format, $args = null, ...$__variadic) : int {}
function fputcsv(resource $handle, array $fields, string $delimiter = ",", string $enclosure = '"', string $escape_char = "\\") : int {}
function fputs($fp, $str, $length) {}
function fread(resource $handle, int $length) : string {}
function frenchtojd(int $month, int $day, int $year) : int {}
function fscanf(resource $handle, string $format, &...$__variadic) {}
function fseek(resource $handle, int $offset, int $whence = SEEK_SET) : int {}
function fsockopen(string $hostname, int $port = -1, int &$errno = 0, string &$errstr = '', float $timeout = ini_get("default_socket_timeout")) : resource {}
function fstat(resource $handle) : array {}
function ftell(resource $handle) : int {}
function ftruncate(resource $handle, int $size) : bool {}
function func_get_arg(int $arg_num) {}
function func_get_args() : array {}
function func_num_args() : int {}
function function_exists(string $function_name) : bool {}
function fwrite(resource $handle, string $string, int $length = 0) : int {}
function gc_collect_cycles() : int {}
function gc_disable() {}
function gc_enable() {}
function gc_enabled() : bool {}
function gc_mem_caches() : int {}
function get_browser(string $user_agent = '', bool $return_array = false) {}
function get_called_class() : string {}
function get_cfg_var(string $option) : string {}

/**
 * @param object $object
 * @return string
 */
function get_class($object = null) : string {}

function get_class_methods($class_name) : array {}
function get_class_vars(string $class_name) : array {}
function get_current_user() : string {}
function get_declared_classes() : array {}
function get_declared_interfaces() : array {}
function get_declared_traits() : array {}
function get_defined_constants(bool $categorize = false) : array {}
function get_defined_functions(bool $exclude_disabled = false) : array {}
function get_defined_vars() : array {}
function get_extension_funcs(string $module_name) : array {}
function get_headers(string $url, int $format = 0) : array {}
function get_html_translation_table(int $table = HTML_SPECIALCHARS, int $flags = ENT_COMPAT | ENT_HTML401, string $encoding = "UTF-8") : array {}
function get_include_path() : string {}
function get_included_files() : array {}
function get_loaded_extensions(bool $zend_extensions = false) : array {}
function get_magic_quotes_gpc() : bool {}
function get_magic_quotes_runtime() : bool {}
function get_meta_tags(string $filename, bool $use_include_path = false) : array {}
function get_object_vars(object $object) : array {}
function get_parent_class($object = null) : string {}
function get_required_files() {}
function get_resource_type(resource $handle) : string {}
function get_resources(string $type = '') : array {}
function getcwd() : string {}
function getdate(int $timestamp = 0) : array {}
function getenv(string $varname, bool $local_only = false) : string {}
function gethostbyaddr(string $ip_address) : string {}
function gethostbyname(string $hostname) : string {}
function gethostbynamel(string $hostname) : array {}
function gethostname() : string {}
function getimagesize(string $filename, array &$imageinfo = null) : array {}
function getimagesizefromstring(string $imagedata, array &$imageinfo = null) : array {}
function getlastmod() : int {}
function getmxrr(string $hostname, array &$mxhosts, array &$weight = null) : bool {}
function getmygid() : int {}
function getmyinode() : int {}
function getmypid() : int {}
function getmyuid() : int {}
function getopt(string $options, array $longopts = null, int &$optind = 0) : array {}
function getprotobyname(string $name) : int {}
function getprotobynumber(int $number) : string {}
function getrandmax() : int {}
function getrusage(int $who = 0) : array {}
function getservbyname(string $service, string $protocol) : int {}
function getservbyport(int $port, string $protocol) : string {}
function gettimeofday(bool $return_float = false) {}
function gettype($var) : string {}
function glob(string $pattern, int $flags = 0) : array {}
function gmdate(string $format, int $timestamp = 0) : string {}
function gmmktime(int $hour = gmdate("H"), int $minute = gmdate("i"), int $second = gmdate("s"), int $month = gmdate("n"), int $day = gmdate("j"), int $year = gmdate("Y"), int $is_dst = -1) : int {}
function gmstrftime(string $format, int $timestamp = 0) : string {}
function gregoriantojd(int $month, int $day, int $year) : int {}
function gzclose(resource $zp) : bool {}
function gzcompress(string $data, int $level = -1, int $encoding = ZLIB_ENCODING_DEFLATE) : string {}
function gzdecode(string $data, int $length = 0) : string {}
function gzdeflate(string $data, int $level = -1, int $encoding = ZLIB_ENCODING_RAW) : string {}
function gzencode(string $data, int $level = -1, int $encoding_mode = FORCE_GZIP) : string {}
function gzeof(resource $zp) : int {}
function gzfile(string $filename, int $use_include_path = 0) : array {}
function gzgetc(resource $zp) : string {}
function gzgets(resource $zp, int $length = 0) : string {}
function gzgetss(resource $zp, int $length, string $allowable_tags = '') : string {}
function gzinflate(string $data, int $length = 0) : string {}
function gzopen(string $filename, string $mode, int $use_include_path = 0) : resource {}
function gzpassthru(resource $zp) : int {}
function gzread(resource $zp, int $length) : string {}
function gzrewind(resource $zp) : bool {}
function gzseek(resource $zp, int $offset, int $whence = SEEK_SET) : int {}
function gztell(resource $zp) : int {}
function gzuncompress(string $data, int $length = 0) : string {}
function gzwrite(resource $zp, string $string, int $length = 0) : int {}
function header(string $string, bool $replace = true, int $http_response_code = 0) {}
function header_register_callback(callable $callback) : bool {}
function header_remove(string $name = '') {}
function headers_list() : array {}
function headers_sent(string &$file = '', int &$line = 0) : bool {}
function hebrev(string $hebrew_text, int $max_chars_per_line = 0) : string {}
function hebrevc(string $hebrew_text, int $max_chars_per_line = 0) : string {}
function hex2bin(string $data) : string {}
function hexdec(string $hex_string) {}
function highlight_file(string $filename, bool $return = false) {}
function highlight_string(string $str, bool $return = false) {}
function html_entity_decode(string $string, int $flags = ENT_COMPAT | ENT_HTML401, string $encoding = ini_get("default_charset")) : string {}
function htmlentities(string $string, int $flags = ENT_COMPAT | ENT_HTML401, string $encoding = ini_get("default_charset"), bool $double_encode = true) : string {}
function htmlspecialchars(string $string, int $flags = ENT_COMPAT | ENT_HTML401, string $encoding = ini_get("default_charset"), bool $double_encode = true) : string {}
function htmlspecialchars_decode(string $string, int $flags = ENT_COMPAT | ENT_HTML401) : string {}
function http_build_query($query_data, string $numeric_prefix = '', string $arg_separator = '', int $enc_type = PHP_QUERY_RFC1738) : string {}
function http_response_code(int $response_code = 0) {}
function hypot(float $x, float $y) : float {}
function iconv(string $in_charset, string $out_charset, string $str) : string {}
function iconv_get_encoding(string $type = "all") {}
function iconv_mime_decode(string $encoded_header, int $mode = 0, string $charset = ini_get("iconv.internal_encoding")) : string {}
function iconv_mime_decode_headers(string $encoded_headers, int $mode = 0, string $charset = ini_get("iconv.internal_encoding")) : array {}
function iconv_mime_encode(string $field_name, string $field_value, array $preferences = null) : string {}
function iconv_set_encoding(string $type, string $charset) : bool {}
function iconv_strlen(string $str, string $charset = ini_get("iconv.internal_encoding")) : int {}
function iconv_strpos(string $haystack, string $needle, int $offset = 0, string $charset = ini_get("iconv.internal_encoding")) : int {}
function iconv_strrpos(string $haystack, string $needle, string $charset = ini_get("iconv.internal_encoding")) : int {}
function iconv_substr(string $str, int $offset, int $length = iconv_strlen($str, $charset), string $charset = ini_get("iconv.internal_encoding")) : string {}
function idate(string $format, int $timestamp = 0) : int {}
function ignore_user_abort(bool $value = false) : int {}
function image_type_to_extension(int $imagetype, bool $include_dot = true) : string {}
function image_type_to_mime_type(int $imagetype) : string {}
function implode(array $pieces) : string {}
function in_array($needle, array $haystack, bool $strict = false) : bool {}
function inet_ntop(string $in_addr) : string {}
function inet_pton(string $address) : string {}
function inflate_add(resource $context, string $encoded_data, int $flush_mode = ZLIB_SYNC_FLUSH) : string {}
function inflate_init(int $encoding, array $options = null) : resource {}
function ini_alter($varname, $newvalue) {}
function ini_get(string $varname) : string {}
function ini_get_all(string $extension = '', bool $details = true) : array {}
function ini_restore(string $varname) {}
function ini_set(string $varname, string $newvalue) : string {}
function intdiv(int $dividend, int $divisor) : int {}
function interface_exists(string $interface_name, bool $autoload = true) : bool {}
function intval($var, int $base = 10) : int {}
function ip2long(string $ip_address) : int {}
function iptcembed(string $iptcdata, string $jpeg_file_name, int $spool = 0) {}
function iptcparse(string $iptcblock) : array {}
function is_a(object $object, string $class_name, bool $allow_string = false) : bool {}
function is_array($var) : bool {}
function is_bool($var) : bool {}
function is_callable($var, bool $syntax_only = false, string &$callable_name = '') : bool {}
function is_dir(string $filename) : bool {}
function is_double($var) {}
function is_executable(string $filename) : bool {}
function is_file(string $filename) : bool {}
function is_finite(float $val) : bool {}
function is_float($var) : bool {}
function is_infinite(float $val) : bool {}
function is_int($var) : bool {}
function is_integer($var) {}
function is_link(string $filename) : bool {}
function is_long($var) {}
function is_nan(float $val) : bool {}
function is_null($var) : bool {}
function is_numeric($var) : bool {}
function is_object($var) : bool {}
function is_readable(string $filename) : bool {}
function is_real($var) {}
function is_resource($var) : bool {}
function is_scalar($var) : bool {}
function is_soap_fault($object) : bool {}
function is_string($var) : bool {}
function is_subclass_of($object, string $class_name, bool $allow_string = true) : bool {}
function is_uploaded_file(string $filename) : bool {}
function is_writable(string $filename) : bool {}
function is_writeable($filename) {}
function iterator_apply(Traversable $iterator, callable $function, array $args = null) : int {}
function iterator_count(Traversable $iterator) : int {}
function iterator_to_array(Traversable $iterator, bool $use_keys = true) : array {}
function jddayofweek(int $julianday, int $mode = CAL_DOW_DAYNO) {}
function jdmonthname(int $julianday, int $mode) : string {}
function jdtofrench(int $juliandaycount) : string {}
function jdtogregorian(int $julianday) : string {}
function jdtojewish(int $juliandaycount, bool $hebrew = false, int $fl = 0) : string {}
function jdtojulian(int $julianday) : string {}
function jdtounix(int $jday) : int {}
function jewishtojd(int $month, int $day, int $year) : int {}
function join($glue, $pieces) {}
function json_decode(string $json, bool $assoc = false, int $depth = 512, int $options = 0) {}
function json_encode($value, int $options = 0, int $depth = 512) : string {}
function json_last_error() : int {}
function json_last_error_msg() : string {}
function juliantojd(int $month, int $day, int $year) : int {}
function key(array &$array) {}
function key_exists($key, $search) {}
function krsort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function ksort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function lcfirst(string $str) : string {}
function lcg_value() : float {}
function ldap_add(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_bind(resource $link_identifier, string $bind_rdn = null, string $bind_password = null) : bool {}
function ldap_compare(resource $link_identifier, string $dn, string $attribute, string $value) {}
function ldap_connect(string $host = null, int $port = 389) : resource {}
function ldap_control_paged_result(resource $link, int $pagesize, bool $iscritical = false, string $cookie = "") : bool {}
function ldap_control_paged_result_response(resource $link, resource $result, string &$cookie = '', int &$estimated = 0) : bool {}
function ldap_count_entries(resource $link_identifier, resource $result_identifier) : int {}
function ldap_delete(resource $link_identifier, string $dn) : bool {}
function ldap_dn2ufn(string $dn) : string {}
function ldap_err2str(int $errno) : string {}
function ldap_errno(resource $link_identifier) : int {}
function ldap_error(resource $link_identifier) : string {}
function ldap_escape(string $value, string $ignore = '', int $flags = 0) : string {}
function ldap_explode_dn(string $dn, int $with_attrib) : array {}
function ldap_first_attribute(resource $link_identifier, resource $result_entry_identifier) : string {}
function ldap_first_entry(resource $link_identifier, resource $result_identifier) : resource {}
function ldap_first_reference(resource $link, resource $result) : resource {}
function ldap_free_result(resource $result_identifier) : bool {}
function ldap_get_attributes(resource $link_identifier, resource $result_entry_identifier) : array {}
function ldap_get_dn(resource $link_identifier, resource $result_entry_identifier) : string {}
function ldap_get_entries(resource $link_identifier, resource $result_identifier) : array {}
function ldap_get_option(resource $link_identifier, int $option, &$retval) : bool {}
function ldap_get_values(resource $link_identifier, resource $result_entry_identifier, string $attribute) : array {}
function ldap_get_values_len(resource $link_identifier, resource $result_entry_identifier, string $attribute) : array {}
function ldap_list(resource $link_identifier, string $base_dn, string $filter, array $attributes = null, int $attrsonly = 0, int $sizelimit = 0, int $timelimit = 0, int $deref = 0) : resource {}
function ldap_mod_add(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_mod_del(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_mod_replace(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_modify(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_modify_batch(resource $link_identifier, string $dn, array $entry) : bool {}
function ldap_next_attribute(resource $link_identifier, resource $result_entry_identifier) : string {}
function ldap_next_entry(resource $link_identifier, resource $result_entry_identifier) : resource {}
function ldap_next_reference(resource $link, resource $entry) : resource {}
function ldap_parse_reference(resource $link, resource $entry, array &$referrals) : bool {}
function ldap_parse_result(resource $link, resource $result, int &$errcode, string &$matcheddn = '', string &$errmsg = '', array &$referrals = null) : bool {}
function ldap_read(resource $link_identifier, string $base_dn, string $filter, array $attributes = null, int $attrsonly = 0, int $sizelimit = 0, int $timelimit = 0, int $deref = 0) : resource {}
function ldap_rename(resource $link_identifier, string $dn, string $newrdn, string $newparent, bool $deleteoldrdn) : bool {}
function ldap_sasl_bind(resource $link, string $binddn = null, string $password = null, string $sasl_mech = null, string $sasl_realm = null, string $sasl_authc_id = null, string $sasl_authz_id = null, string $props = null) : bool {}
function ldap_search(resource $link_identifier, string $base_dn, string $filter, array $attributes = null, int $attrsonly = 0, int $sizelimit = 0, int $timelimit = 0, int $deref = 0) : resource {}
function ldap_set_option(resource $link_identifier, int $option, $newval) : bool {}
function ldap_sort(resource $link, resource $result, string $sortfilter) : bool {}
function ldap_start_tls(resource $link) : bool {}
function ldap_unbind(resource $link_identifier) : bool {}
function levenshtein(string $str1, string $str2, int $cost_ins, int $cost_rep, int $cost_del) : int {}
function libxml_clear_errors() {}
function libxml_disable_entity_loader(bool $disable = true) : bool {}
function libxml_get_errors() : array {}
function libxml_get_last_error() {}
function libxml_set_external_entity_loader(callable $resolver_function) {}
function libxml_set_streams_context(resource $streams_context) {}
function libxml_use_internal_errors(bool $use_errors = false) : bool {}
function link(string $target, string $link) : bool {}
function linkinfo(string $path) : int {}
function localeconv() : array {}
function localtime(int $timestamp = 0, bool $is_associative = false) : array {}
function log(float $arg, float $base = M_E) : float {}
function log10(float $arg) : float {}
function log1p(float $number) : float {}
function long2ip(string $proper_address) : string {}
function lstat(string $filename) : array {}
function ltrim(string $str, string $character_mask = '') : string {}
function mail(string $to, string $subject, string $message, string $additional_headers = '', string $additional_parameters = '') : bool {}
function max($value1, $value2, ...$__variadic) {}
function mb_check_encoding(string $var = null, string $encoding = '') : bool {}
function mb_convert_case(string $str, int $mode, string $encoding = '') : string {}
function mb_convert_encoding(string $str, string $to_encoding, $from_encoding = null) : string {}
function mb_convert_kana(string $str, string $option = "KV", string $encoding = '') : string {}
function mb_convert_variables(string $to_encoding, $from_encoding, &$vars, &...$__variadic) : string {}
function mb_decode_mimeheader(string $str) : string {}
function mb_decode_numericentity(string $str, array $convmap, string $encoding = '') : string {}
function mb_detect_encoding(string $str, $encoding_list = null, bool $strict = false) : string {}
function mb_detect_order($encoding_list = null) {}
function mb_encode_mimeheader(string $str, string $charset = '', string $transfer_encoding = "B", string $linefeed = "\r\n", int $indent = 0) : string {}
function mb_encode_numericentity(string $str, array $convmap, string $encoding = '', bool $is_hex = false) : string {}
function mb_encoding_aliases(string $encoding) : array {}
function mb_ereg(string $pattern, string $string, array &$regs = null) : int {}
function mb_ereg_match(string $pattern, string $string, string $option = "msr") : bool {}
function mb_ereg_replace(string $pattern, string $replacement, string $string, string $option = "msr") : string {}
function mb_ereg_replace_callback(string $pattern, callable $callback, string $string, string $option = "msr") : string {}
function mb_ereg_search(string $pattern = '', string $option = "ms") : bool {}
function mb_ereg_search_getpos() : int {}
function mb_ereg_search_getregs() : array {}
function mb_ereg_search_init(string $string, string $pattern = '', string $option = "msr") : bool {}
function mb_ereg_search_pos(string $pattern = '', string $option = "ms") : array {}
function mb_ereg_search_regs(string $pattern = '', string $option = "ms") : array {}
function mb_ereg_search_setpos(int $position) : bool {}
function mb_eregi(string $pattern, string $string, array &$regs = null) : int {}
function mb_eregi_replace(string $pattern, string $replace, string $string, string $option = "msri") : string {}
function mb_get_info(string $type = "all") {}
function mb_http_input(string $type = "") {}
function mb_http_output(string $encoding = '') {}
function mb_internal_encoding(string $encoding = '') {}
function mb_language(string $language = '') {}
function mb_list_encodings() : array {}
function mb_output_handler(string $contents, int $status) : string {}
function mb_parse_str(string $encoded_string, array &$result = null) : bool {}
function mb_preferred_mime_name(string $encoding) : string {}
function mb_regex_encoding(string $encoding = '') {}
function mb_regex_set_options(string $options = '') : string {}
function mb_send_mail(string $to, string $subject, string $message, string $additional_headers = null, string $additional_parameter = null) : bool {}
function mb_split(string $pattern, string $string, int $limit = -1) : array {}
function mb_strcut(string $str, int $start, int $length = null, string $encoding = '') : string {}
function mb_strimwidth(string $str, int $start, int $width, string $trimmarker = "", string $encoding = '') : string {}
function mb_stripos(string $haystack, string $needle, int $offset = 0, string $encoding = '') : int {}
function mb_stristr(string $haystack, string $needle, bool $before_needle = false, string $encoding = '') : string {}
function mb_strlen(string $str, string $encoding = '') {}
function mb_strpos(string $haystack, string $needle, int $offset = 0, string $encoding = '') : int {}
function mb_strrchr(string $haystack, string $needle, bool $part = false, string $encoding = '') : string {}
function mb_strrichr(string $haystack, string $needle, bool $part = false, string $encoding = '') : string {}
function mb_strripos(string $haystack, string $needle, int $offset = 0, string $encoding = '') : int {}
function mb_strrpos(string $haystack, string $needle, int $offset = 0, string $encoding = '') : int {}
function mb_strstr(string $haystack, string $needle, bool $before_needle = false, string $encoding = '') : string {}
function mb_strtolower(string $str, string $encoding = '') : string {}
function mb_strtoupper(string $str, string $encoding = '') : string {}
function mb_strwidth(string $str, string $encoding = '') : int {}
function mb_substitute_character($substrchar = null) {}
function mb_substr(string $str, int $start, int $length = null, string $encoding = '') : string {}
function mb_substr_count(string $haystack, string $needle, string $encoding = '') : int {}
function mcrypt_create_iv(int $size, int $source = MCRYPT_DEV_URANDOM) : string {}
function mcrypt_decrypt(string $cipher, string $key, string $data, string $mode, string $iv = '') : string {}
function mcrypt_enc_get_algorithms_name(resource $td) : string {}
function mcrypt_enc_get_block_size(resource $td) : int {}
function mcrypt_enc_get_iv_size(resource $td) : int {}
function mcrypt_enc_get_key_size(resource $td) : int {}
function mcrypt_enc_get_modes_name(resource $td) : string {}
function mcrypt_enc_get_supported_key_sizes(resource $td) : array {}
function mcrypt_enc_is_block_algorithm(resource $td) : bool {}
function mcrypt_enc_is_block_algorithm_mode(resource $td) : bool {}
function mcrypt_enc_is_block_mode(resource $td) : bool {}
function mcrypt_enc_self_test(resource $td) : int {}
function mcrypt_encrypt(string $cipher, string $key, string $data, string $mode, string $iv = '') : string {}
function mcrypt_generic(resource $td, string $data) : string {}
function mcrypt_generic_deinit(resource $td) : bool {}
function mcrypt_generic_init(resource $td, string $key, string $iv) : int {}
function mcrypt_get_block_size(string $cipher, string $mode) : int {}
function mcrypt_get_cipher_name(string $cipher) : string {}
function mcrypt_get_iv_size(string $cipher, string $mode) : int {}
function mcrypt_get_key_size(string $cipher, string $mode) : int {}
function mcrypt_list_algorithms(string $lib_dir = ini_get("mcrypt.algorithms_dir")) : array {}
function mcrypt_list_modes(string $lib_dir = ini_get("mcrypt.modes_dir")) : array {}
function mcrypt_module_close(resource $td) : bool {}
function mcrypt_module_get_algo_block_size(string $algorithm, string $lib_dir = '') : int {}
function mcrypt_module_get_algo_key_size(string $algorithm, string $lib_dir = '') : int {}
function mcrypt_module_get_supported_key_sizes(string $algorithm, string $lib_dir = '') : array {}
function mcrypt_module_is_block_algorithm(string $algorithm, string $lib_dir = '') : bool {}
function mcrypt_module_is_block_algorithm_mode(string $mode, string $lib_dir = '') : bool {}
function mcrypt_module_is_block_mode(string $mode, string $lib_dir = '') : bool {}
function mcrypt_module_open(string $algorithm, string $algorithm_directory, string $mode, string $mode_directory) : resource {}
function mcrypt_module_self_test(string $algorithm, string $lib_dir = '') : bool {}
function md5(string $str, bool $raw_output = false) : string {}
function md5_file(string $filename, bool $raw_output = false) : string {}
function mdecrypt_generic(resource $td, string $data) : string {}
function memory_get_peak_usage(bool $real_usage = false) : int {}
function memory_get_usage(bool $real_usage = false) : int {}
function metaphone(string $str, int $phonemes = 0) : string {}
function method_exists($object, string $method_name) : bool {}
function mhash(int $hash, string $data, string $key = '') : string {}
function mhash_count() : int {}
function mhash_get_block_size(int $hash) : int {}
function mhash_get_hash_name(int $hash) : string {}
function mhash_keygen_s2k(int $hash, string $password, string $salt, int $bytes) : string {}
function microtime(bool $get_as_float = false) {}
function min($value1, $value2, ...$__variadic) {}
function mkdir(string $pathname, int $mode = 0777, bool $recursive = false, resource $context = null) : bool {}
function mktime(int $hour = date("H"), int $minute = date("i"), int $second = date("s"), int $month = date("n"), int $day = date("j"), int $year = date("Y"), int $is_dst = -1) : int {}
function move_uploaded_file(string $filename, string $destination) : bool {}
function mt_getrandmax() : int {}
function mt_rand(int $min, int $max) : int {}
function mt_srand(int $seed = 0, int $mode = MT_RAND_MT19937) {}
function natcasesort(array &$array) : bool {}
function natsort(array &$array) : bool {}
function next(array &$array) {}
function nl2br(string $string, bool $is_xhtml = true) : string {}
function number_format(float $number, int $decimals, string $dec_point, string $thousands_sep) : string {}
function ob_clean() {}
function ob_end_clean() : bool {}
function ob_end_flush() : bool {}
function ob_flush() {}
function ob_get_clean() : string {}
function ob_get_contents() : string {}
function ob_get_flush() : string {}
function ob_get_length() : int {}
function ob_get_level() : int {}
function ob_get_status(bool $full_status = false) : array {}
function ob_gzhandler(string $buffer, int $mode) : string {}
function ob_implicit_flush(int $flag = true) {}
function ob_list_handlers() : array {}
function ob_start(callable $output_callback = null, int $chunk_size = 0, int $flags = PHP_OUTPUT_HANDLER_STDFLAGS) : bool {}
function octdec(string $octal_string) {}
function opendir(string $path, resource $context = null) : resource {}
function openlog(string $ident, int $option, int $facility) : bool {}
function openssl_cipher_iv_length(string $method) : int {}
function openssl_csr_export(resource $csr, string &$out, bool $notext = true) : bool {}
function openssl_csr_export_to_file(resource $csr, string $outfilename, bool $notext = true) : bool {}
function openssl_csr_get_public_key($csr, bool $use_shortnames = true) : resource {}
function openssl_csr_get_subject($csr, bool $use_shortnames = true) : array {}
function openssl_csr_new(array $dn, resource &$privkey, array $configargs = null, array $extraattribs = null) {}
function openssl_csr_sign($csr, $cacert, $priv_key, int $days, array $configargs = null, int $serial = 0) : resource {}
function openssl_decrypt(string $data, string $method, string $key, int $options = 0, string $iv = "", string $tag = "", string $aad = "") : string {}
function openssl_dh_compute_key(string $pub_key, resource $dh_key) : string {}
function openssl_digest(string $data, string $method, bool $raw_output = false) : string {}
function openssl_encrypt(string $data, string $method, string $key, int $options = 0, string $iv = "", string &$tag = null, string $aad = "", int $tag_length = 16) : string {}
function openssl_error_string() : string {}
function openssl_free_key(resource $key_identifier) {}
function openssl_get_cert_locations() : array {}
function openssl_get_cipher_methods(bool $aliases = false) : array {}
function openssl_get_md_methods(bool $aliases = false) : array {}
function openssl_open(string $sealed_data, string &$open_data, string $env_key, $priv_key_id, string $method = '') : bool {}
function openssl_pbkdf2(string $password, string $salt, int $key_length, int $iterations, string $digest_algorithm = '') : string {}
function openssl_pkcs12_export($x509, string &$out, $priv_key, string $pass, array $args = null) : bool {}
function openssl_pkcs12_export_to_file($x509, string $filename, $priv_key, string $pass, array $args = null) : bool {}
function openssl_pkcs12_read(string $pkcs12, array &$certs, string $pass) : bool {}
function openssl_pkcs7_decrypt(string $infilename, string $outfilename, $recipcert, $recipkey = null) : bool {}
function openssl_pkcs7_encrypt(string $infile, string $outfile, $recipcerts, array $headers, int $flags = 0, int $cipherid = OPENSSL_CIPHER_RC2_40) : bool {}
function openssl_pkcs7_sign(string $infilename, string $outfilename, $signcert, $privkey, array $headers, int $flags = PKCS7_DETACHED, string $extracerts = '') : bool {}
function openssl_pkcs7_verify(string $filename, int $flags, string $outfilename = '', array $cainfo = null, string $extracerts = '', string $content = '') {}
function openssl_pkey_export($key, string &$out, string $passphrase = '', array $configargs = null) : bool {}
function openssl_pkey_export_to_file($key, string $outfilename, string $passphrase = '', array $configargs = null) : bool {}
function openssl_pkey_free(resource $key) {}
function openssl_pkey_get_details(resource $key) : array {}
function openssl_pkey_get_private($key, string $passphrase = "") : resource {}
function openssl_pkey_get_public($certificate) : resource {}
function openssl_pkey_new(array $configargs = null) : resource {}
function openssl_private_decrypt(string $data, string &$decrypted, $key, int $padding = OPENSSL_PKCS1_PADDING) : bool {}
function openssl_private_encrypt(string $data, string &$crypted, $key, int $padding = OPENSSL_PKCS1_PADDING) : bool {}
function openssl_public_decrypt(string $data, string &$decrypted, $key, int $padding = OPENSSL_PKCS1_PADDING) : bool {}
function openssl_public_encrypt(string $data, string &$crypted, $key, int $padding = OPENSSL_PKCS1_PADDING) : bool {}
function openssl_random_pseudo_bytes(int $length, bool &$crypto_strong = false) : string {}
function openssl_seal(string $data, string &$sealed_data, array &$env_keys, array $pub_key_ids, string $method = "RC4", $iv) : int {}
function openssl_sign(string $data, string &$signature, $priv_key_id, $signature_alg = OPENSSL_ALGO_SHA1) : bool {}
function openssl_spki_export(string &$spkac) : string {}
function openssl_spki_export_challenge(string &$spkac) : string {}
function openssl_spki_new(resource &$privkey, string &$challenge, int $algorithm = 0) : string {}
function openssl_spki_verify(string &$spkac) : string {}
function openssl_verify(string $data, string $signature, $pub_key_id, $signature_alg = OPENSSL_ALGO_SHA1) : int {}
function openssl_x509_check_private_key($cert, $key) : bool {}
function openssl_x509_checkpurpose($x509cert, int $purpose, array $cainfo = null, string $untrustedfile = '') : int {}
function openssl_x509_export($x509, string &$output, bool $notext = true) : bool {}
function openssl_x509_export_to_file($x509, string $outfilename, bool $notext = true) : bool {}
function openssl_x509_fingerprint($x509, string $hash_algorithm = "sha1", bool $raw_output = false) : bool {}
function openssl_x509_free(resource $x509cert) {}
function openssl_x509_parse($x509cert, bool $shortnames = true) : array {}
function openssl_x509_read($x509certdata) : resource {}
function ord(string $string) : int {}
function output_add_rewrite_var(string $name, string $value) : bool {}
function output_reset_rewrite_vars() : bool {}
function pack(string $format, $args = null, ...$__variadic) : string {}
function parse_ini_file(string $filename, bool $process_sections = false, int $scanner_mode = INI_SCANNER_NORMAL) : array {}
function parse_ini_string(string $ini, bool $process_sections = false, int $scanner_mode = INI_SCANNER_NORMAL) : array {}
function parse_str(string $encoded_string, array &$result = null) {}
function parse_url(string $url, int $component = -1) {}
function passthru(string $command, int &$return_var = 0) {}
function password_get_info(string $hash) : array {}
function password_hash(string $password, integer $algo, array $options = null) : string {}
function password_needs_rehash(string $hash, integer $algo, array $options = null) : boolean {}
function password_verify(string $password, string $hash) : boolean {}
function pathinfo(string $path, int $options = PATHINFO_DIRNAME | PATHINFO_BASENAME | PATHINFO_EXTENSION | PATHINFO_FILENAME) {}
function pclose(resource $handle) : int {}
function pfsockopen(string $hostname, int $port = -1, int &$errno = 0, string &$errstr = '', float $timeout = ini_get("default_socket_timeout")) : resource {}
function php_ini_loaded_file() : string {}
function php_ini_scanned_files() : string {}
function php_sapi_name() : string {}
function php_strip_whitespace(string $filename) : string {}
function php_uname(string $mode = "a") : string {}
function phpcredits(int $flag = CREDITS_ALL) : bool {}
function phpinfo(int $what = INFO_ALL) : bool {}
function phpversion(string $extension = '') : string {}
function pi() : float {}
function popen(string $command, string $mode) : resource {}
function pos($arg) {}
function pow(number $base, number $exp) {}
function preg_filter($pattern, $replacement, $subject, int $limit = -1, int &$count = 0) {}
function preg_grep(string $pattern, array $input, int $flags = 0) : array {}
function preg_last_error() : int {}
function preg_match(string $pattern, string $subject, /** @out */ array &$matches = null, int $flags = 0, int $offset = 0) : int {}
function preg_match_all(string $pattern, string $subject, /** @out */ array &$matches = null, int $flags = PREG_PATTERN_ORDER, int $offset = 0) : int {}
function preg_quote(string $str, string $delimiter = null) : string {}
function preg_replace($pattern, $replacement, $subject, int $limit = -1, int &$count = 0) {}
function preg_replace_callback($pattern, callable $callback, $subject, int $limit = -1, int &$count = 0) {}
function preg_replace_callback_array(array $patterns_and_callbacks, $subject, int $limit = -1, int &$count = 0) {}
function preg_split(string $pattern, string $subject, int $limit = -1, int $flags = 0) : array {}
function prev(array &$array) {}
function print_r($expression, bool $return = false) {}
function printf(string $format, /** @var string[] */ ...$args) : int {}
function proc_close(resource $process) : int {}
function proc_get_status(resource $process) : array {}
function proc_open(string $cmd, array $descriptorspec, array &$pipes, string $cwd = '', array $env = null, array $other_options = null) : resource {}
function proc_terminate(resource $process, int $signal = 15) : bool {}
function property_exists($class, string $property) : bool {}
function putenv(string $setting) : bool {}
function quoted_printable_decode(string $str) : string {}
function quoted_printable_encode(string $str) : string {}
function quotemeta(string $str) : string {}
function rad2deg(float $number) : float {}
function rand(int $min, int $max) : int {}
function random_bytes(int $length) : string {}
function random_int(int $min, int $max) : int {}
function range($start, $end, number $step = 1) : array {}
function rawurldecode(string $str) : string {}
function rawurlencode(string $str) : string {}
function readdir(resource $dir_handle = null) : string {}
function readfile(string $filename, bool $use_include_path = false, resource $context = null) : int {}
function readgzfile(string $filename, int $use_include_path = 0) : int {}
function readlink(string $path) : string {}
function realpath(string $path) : string {}
function realpath_cache_get() : array {}
function realpath_cache_size() : int {}
function register_shutdown_function(callable $callback, $parameter = null, ...$__variadic) {}
function register_tick_function(callable $function, $arg = null, ...$__variadic) : bool {}
function rename(string $oldname, string $newname, resource $context = null) : bool {}
function reset(array &$array) {}
function restore_error_handler() : bool {}
function restore_exception_handler() : bool {}
function restore_include_path() {}
function rewind(resource $handle) : bool {}
function rewinddir(resource $dir_handle = null) {}
function rmdir(string $dirname, resource $context = null) : bool {}
function round(float $val, int $precision = 0, int $mode = PHP_ROUND_HALF_UP) : float {}
function rsort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function rtrim(string $str, string $character_mask = '') : string {}
function scandir(string $directory, int $sorting_order = SCANDIR_SORT_ASCENDING, resource $context = null) : array {}
function serialize($value) : string {}
function session_abort() {}
function session_cache_expire(string $new_cache_expire = '') : int {}
function session_cache_limiter(string $cache_limiter = '') : string {}
function session_commit() {}
function session_decode(string $data) : bool {}
function session_destroy() : bool {}
function session_encode() : string {}
function session_get_cookie_params() : array {}
function session_id(string $id = '') : string {}
function session_module_name(string $module = '') : string {}
function session_name(string $name = '') : string {}
function session_regenerate_id(bool $delete_old_session = false) : bool {}
function session_register_shutdown() {}
function session_reset() {}
function session_save_path(string $path = '') : string {}
function session_set_cookie_params(int $lifetime, string $path = '', string $domain = '', bool $secure = false, bool $httponly = false) {}
function session_set_save_handler(SessionHandlerInterface $sessionhandler, bool $register_shutdown = true) : bool {}
function session_start(array $options = []) : bool {}
function session_status() : int {}
function session_unset() {}
function session_write_close() {}
function set_error_handler(callable $error_handler, int $error_types = E_ALL | E_STRICT) {}
function set_exception_handler(callable $exception_handler) {}
function set_file_buffer($fp, $buffer) {}
function set_include_path(string $new_include_path) : string {}
function set_time_limit(int $seconds) : bool {}
function setcookie(string $name, string $value = "", int $expire = 0, string $path = "", string $domain = "", bool $secure = false, bool $httponly = false) : bool {}
function setlocale(int $category, array $locale) : string {}
function setrawcookie(string $name, string $value = '', int $expire = 0, string $path = '', string $domain = '', bool $secure = false, bool $httponly = false) : bool {}
function settype(&$var, string $type) : bool {}
function sha1(string $str, bool $raw_output = false) : string {}
function sha1_file(string $filename, bool $raw_output = false) : string {}
function shell_exec(string $cmd) : string {}
function show_source($file_name, $return) {}
function shuffle(array &$array) : bool {}
function similar_text(string $first, string $second, float &$percent = null) : int {}
function simplexml_import_dom(DOMNode $node, string $class_name = "SimpleXMLElement") {}
function simplexml_load_file(string $filename, string $class_name = "SimpleXMLElement", int $options = 0, string $ns = "", bool $is_prefix = false) {}
function simplexml_load_string(string $data, string $class_name = "SimpleXMLElement", int $options = 0, string $ns = "", bool $is_prefix = false) {}
function sin(float $arg) : float {}
function sinh(float $arg) : float {}
function sizeof($var, $mode) {}
function sleep(int $seconds) : int {}
function socket_get_status($fp) {}
function socket_set_blocking($socket, $mode) {}
function socket_set_timeout($stream, $seconds, $microseconds) {}
function sort(array &$array, int $sort_flags = SORT_REGULAR) : bool {
  if ($sort_flags === SORT_LOCALE_STRING) {
    /** @__isolationBreach('Depends on the current locale.') */
  }
}
function soundex(string $str) : string {}
function spl_autoload(string $class_name, string $file_extensions = '') {}
function spl_autoload_call(string $class_name) {}
function spl_autoload_extensions(string $file_extensions = '') : string {}
function spl_autoload_functions() : array {}
function spl_autoload_register(callable $autoload_function = null, bool $throw = true, bool $prepend = false) : bool {}
function spl_autoload_unregister($autoload_function) : bool {}
function spl_classes() : array {}
function spl_object_hash(object $obj) : string {}
function sprintf(string $format, /** @var string[] */ ...$args) : string {}
function sqrt(float $arg) : float {}
function srand(int $seed = 0) {}
function sscanf(string $str, string $format, &...$__variadic) {}
function stat(string $filename) : array {}
function str_getcsv(string $input, string $delimiter = ",", string $enclosure = '"', string $escape = "\\") : array {}
function str_ireplace($search, $replace, $subject, int &$count = 0) {}
function str_pad(string $input, int $pad_length, string $pad_string = " ", int $pad_type = STR_PAD_RIGHT) : string {}
function str_repeat(string $input, int $multiplier) : string {}
function str_replace($search, $replace, $subject, int &$count = 0) {}
function str_rot13(string $str) : string {}
function str_shuffle(string $str) : string {}
function str_split(string $string, int $split_length = 1) : array {}
function str_word_count(string $string, int $format = 0, string $charlist = '') {}
function strcasecmp(string $str1, string $str2) : int {}
function strchr($haystack, $needle, $part) {}
function strcmp(string $str1, string $str2) : int {}
function strcoll(string $str1, string $str2) : int {}
function strcspn(string $subject, string $mask, int $start = 0, int $length = 0) : int {}
function stream_bucket_append(resource $brigade, object $bucket) {}
function stream_bucket_make_writeable(resource $brigade) : object {}
function stream_bucket_new(resource $stream, string $buffer) : object {}
function stream_bucket_prepend(resource $brigade, object $bucket) {}
function stream_context_create(array $options = null, array $params = null) : resource {}
function stream_context_get_default(array $options = null) : resource {}
function stream_context_get_options(resource $stream_or_context) : array {}
function stream_context_get_params(resource $stream_or_context) : array {}
function stream_context_set_default(array $options) : resource {}
function stream_context_set_option(resource $stream_or_context, array $options) : bool {}
function stream_context_set_params(resource $stream_or_context, array $params) : bool {}
function stream_copy_to_stream(resource $source, resource $dest, int $maxlength = -1, int $offset = 0) : int {}
function stream_filter_append(resource $stream, string $filtername, int $read_write = 0, $params = null) : resource {}
function stream_filter_prepend(resource $stream, string $filtername, int $read_write = 0, $params = null) : resource {}
function stream_filter_register(string $filtername, string $classname) : bool {}
function stream_filter_remove(resource $stream_filter) : bool {}
function stream_get_contents(resource $handle, int $maxlength = -1, int $offset = -1) : string {}
function stream_get_filters() : array {}
function stream_get_line(resource $handle, int $length, string $ending = '') : string {}
function stream_get_meta_data(resource $stream) : array {}
function stream_get_transports() : array {}
function stream_get_wrappers() : array {}
function stream_is_local($stream_or_url) : bool {}
function stream_register_wrapper($protocol, $classname, $flags) {}
function stream_resolve_include_path(string $filename) : string {}
function stream_select(array &$read, array &$write, array &$except, int $tv_sec, int $tv_usec = 0) : int {}
function stream_set_blocking(resource $stream, bool $mode) : bool {}
function stream_set_chunk_size(resource $fp, int $chunk_size) : int {}
function stream_set_read_buffer(resource $stream, int $buffer) : int {}
function stream_set_timeout(resource $stream, int $seconds, int $microseconds = 0) : bool {}
function stream_set_write_buffer(resource $stream, int $buffer) : int {}
function stream_socket_accept(resource $server_socket, float $timeout = ini_get("default_socket_timeout"), string &$peername = '') : resource {}
function stream_socket_client(string $remote_socket, int &$errno = 0, string &$errstr = '', float $timeout = ini_get("default_socket_timeout"), int $flags = STREAM_CLIENT_CONNECT, resource $context = null) : resource {}
function stream_socket_enable_crypto(resource $stream, bool $enable, int $crypto_type = 0, resource $session_stream = null) {}
function stream_socket_get_name(resource $handle, bool $want_peer) : string {}
function stream_socket_pair(int $domain, int $type, int $protocol) : array {}
function stream_socket_recvfrom(resource $socket, int $length, int $flags = 0, string &$address = '') : string {}
function stream_socket_sendto(resource $socket, string $data, int $flags = 0, string $address = '') : int {}
function stream_socket_server(string $local_socket, int &$errno = 0, string &$errstr = '', int $flags = STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, resource $context = null) : resource {}
function stream_socket_shutdown(resource $stream, int $how) : bool {}
function stream_supports_lock(resource $stream) : bool {}
function stream_wrapper_register(string $protocol, string $classname, int $flags = 0) : bool {}
function stream_wrapper_restore(string $protocol) : bool {}
function stream_wrapper_unregister(string $protocol) : bool {}
function strftime(string $format, int $timestamp = 0) : string {}
function strip_tags(string $str, string $allowable_tags = '') : string {}
function stripcslashes(string $str) : string {}
function stripos(string $haystack, string $needle, int $offset = 0) {}
function stripslashes(string $str) : string {}
function stristr(string $haystack, $needle, bool $before_needle = false) : string {}
function strlen(string $string) : int {}
function strnatcasecmp(string $str1, string $str2) : int {}
function strnatcmp(string $str1, string $str2) : int {}
function strncasecmp(string $str1, string $str2, int $len) : int {}
function strncmp(string $str1, string $str2, int $len) : int {}
function strpbrk(string $haystack, string $char_list) : string {}
function strpos(string $haystack, $needle, int $offset = 0) {}
function strrchr(string $haystack, $needle) : string {}
function strrev(string $string) : string {}
function strripos(string $haystack, string $needle, int $offset = 0) : int {}
function strrpos(string $haystack, string $needle, int $offset = 0) : int {}
function strspn(string $subject, string $mask, int $start = 0, int $length = 0) : int {}
function strstr(string $haystack, $needle, bool $before_needle = false) : string {}
function strtok(string $token) : string {}
function strtolower(string $string) : string {}
function strtotime(string $time, int $now = 0) : int {}
function strtoupper(string $string) : string {}
function strtr(string $str, array $replace_pairs) : string {}
function strval($var) : string {}
function substr(string $string, int $start, int $length = 0) : string {}
function substr_compare(string $main_str, string $str, int $offset, int $length = 0, bool $case_insensitivity = false) : int {}
function substr_count(string $haystack, string $needle, int $offset = 0, int $length = 0) : int {}
function substr_replace($string, $replacement, $start, $length = null) {}
function symlink(string $target, string $link) : bool {}
function sys_get_temp_dir() : string {}
function syslog(int $priority, string $message) : bool {}
function system(string $command, int &$return_var = 0) : string {}
function tan(float $arg) : float {}
function tanh(float $arg) : float {}
function tempnam(string $dir, string $prefix) : string {}
function time() : int {}
function time_nanosleep(int $seconds, int $nanoseconds) {}
function time_sleep_until(float $timestamp) : bool {}
function timezone_abbreviations_list() : array {}
function timezone_identifiers_list(int $what = DateTimeZone::ALL, string $country = null) : array {}
function timezone_location_get(DateTimeZone $object) : array {}
function timezone_name_from_abbr(string $abbr, int $gmtOffset = -1, int $isdst = -1) : string {}
function timezone_name_get(DateTimeZone $object) : string {}
function timezone_offset_get(DateTimeZone $object, DateTime $datetime) : int {}
function timezone_open(string $timezone) {}
function timezone_transitions_get(DateTimeZone $object, int $timestamp_begin = 0, int $timestamp_end = 0) : array {}
function timezone_version_get() : string {}
function tmpfile() : resource {}
function token_get_all(string $source, int $flags = 0) : array {}
function token_name(int $token) : string {}
function touch(string $filename, int $time = 0, int $atime = 0) : bool {}
function trait_exists(string $traitname, bool $autoload = false) : bool {}
function trigger_error(string $error_msg, int $error_type = E_USER_NOTICE) : bool {}
function trim(string $str, string $character_mask = "") : string {}
function uasort(array &$array, callable $value_compare_func) : bool {}
function ucfirst(string $str) : string {}
function ucwords(string $str, string $delimiters = "") : string {}
function uksort(array &$array, callable $key_compare_func) : bool {}
function umask(int $mask = 0) : int {}
function uniqid(string $prefix = "", bool $more_entropy = false) : string {}
function unixtojd(int $timestamp = 0) : int {}
function unlink(string $filename, resource $context = null) : bool {}
function unpack(string $format, string $data) : array {}
function unregister_tick_function(string $function_name) {}
function unserialize(string $str, array $options = null) {}
function urldecode(string $str) : string {}
function urlencode(string $str) : string {}
function use_soap_error_handler(bool $handler = true) : bool {}
function user_error($message, $error_type) {}
function usleep(int $micro_seconds) {}
function usort(array &$array, callable $value_compare_func) : bool {}
function utf8_decode(string $data) : string {}
function utf8_encode(string $data) : string {}
function var_dump($expression, ...$__variadic) {}
function var_export($expression, bool $return = false) {}
function version_compare(string $version1, string $version2, string $operator = '') {}
function vfprintf(resource $handle, string $format, array $args) : int {}
function vprintf(string $format, array $args) : int {}
function vsprintf(string $format, array $args) : string {}
function wddx_add_vars(resource $packet_id, $var_name, ...$__variadic) : bool {}
function wddx_deserialize(string $packet) {}
function wddx_packet_end(resource $packet_id) : string {}
function wddx_packet_start(string $comment = '') : resource {}
function wddx_serialize_value($var, string $comment = '') : string {}
function wddx_serialize_vars($var_name, ...$__variadic) : string {}
function wordwrap(string $str, int $width = 75, string $break = "\n", bool $cut = false) : string {}
function xml_error_string(int $code) : string {}
function xml_get_current_byte_index(resource $parser) : int {}
function xml_get_current_column_number(resource $parser) : int {}
function xml_get_current_line_number(resource $parser) : int {}
function xml_get_error_code(resource $parser) : int {}
function xml_parse(resource $parser, string $data, bool $is_final = false) : int {}
function xml_parse_into_struct(resource $parser, string $data, array &$values, array &$index = null) : int {}
function xml_parser_create(string $encoding = '') : resource {}
function xml_parser_create_ns(string $encoding = '', string $separator = ":") : resource {}
function xml_parser_free(resource $parser) : bool {}
function xml_parser_get_option(resource $parser, int $option) {}
function xml_parser_set_option(resource $parser, int $option, $value) : bool {}
function xml_set_character_data_handler(resource $parser, callable $handler) : bool {}
function xml_set_default_handler(resource $parser, callable $handler) : bool {}
function xml_set_element_handler(resource $parser, callable $start_element_handler, callable $end_element_handler) : bool {}
function xml_set_end_namespace_decl_handler(resource $parser, callable $handler) : bool {}
function xml_set_external_entity_ref_handler(resource $parser, callable $handler) : bool {}
function xml_set_notation_decl_handler(resource $parser, callable $handler) : bool {}
function xml_set_object(resource $parser, object &$object) : bool {}
function xml_set_processing_instruction_handler(resource $parser, callable $handler) : bool {}
function xml_set_start_namespace_decl_handler(resource $parser, callable $handler) : bool {}
function xml_set_unparsed_entity_decl_handler(resource $parser, callable $handler) : bool {}
function zend_version() : string {}
function zlib_decode(string $data, string $max_decoded_len = '') : string {}
function zlib_encode(string $data, string $encoding, string $level = -1) : string {}
function zlib_get_coding_type() : string {}

class __PHP_Incomplete_Class
{
}

class AppendIterator extends IteratorIterator implements OuterIterator
{
    function append(Iterator $iterator) : void {}
    function current() {}
    function getArrayIterator() : ArrayIterator {}
    function getInnerIterator() : Iterator {}
    function getIteratorIndex() : int {}
    function key() : scalar {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class ArithmeticError extends Error
{
}

class ArrayIterator implements ArrayAccess, SeekableIterator, Countable, Serializable
{
    function append($value) : void {}
    function asort() : void {}
    function __construct($array = null, int $flags = 0) {}
    function count() : int {}
    function current() {}
    function getArrayCopy() : array {}
    function getFlags() : void {}
    function key() {}
    function ksort() : void {}
    function natcasesort() : void {}
    function natsort() : void {}
    function next() : void {}
    function offsetExists(string $index) : void {}
    function offsetGet(string $index) {}
    function offsetSet(string $index, string $newval) : void {}
    function offsetUnset(string $index) : void {}
    function rewind() : void {}
    function seek(int $position) : void {}
    function serialize() : string {}
    function setFlags(string $flags) : void {}
    function uasort(string $cmp_function) : void {}
    function uksort(string $cmp_function) : void {}
    function unserialize(string $serialized) : string {}
    function valid() : bool {}
}

class ArrayObject implements IteratorAggregate, ArrayAccess, Serializable, Countable
{
    function append($value) : void {}
    function asort() : void {}
    function count() : int {}
    function exchangeArray($input) : array {}
    function getArrayCopy() : array {}
    function getFlags() : int {}
    function getIterator() : ArrayIterator {}
    function getIteratorClass() : string {}
    function ksort() : void {}
    function natcasesort() : void {}
    function natsort() : void {}
    function offsetExists($index) : bool {}
    function offsetGet($index) {}
    function offsetSet($index, $newval) : void {}
    function offsetUnset($index) : void {}
    function serialize() : string {}
    function setFlags(int $flags) : void {}
    function setIteratorClass(string $iterator_class) : void {}
    function uasort(callable $cmp_function) : void {}
    function uksort(callable $cmp_function) : void {}
    function unserialize(string $serialized) : void {}
}

class AssertionError extends Error
{
}

class BadFunctionCallException extends LogicException {}

class BadMethodCallException extends BadFunctionCallException {}

class CachingIterator extends IteratorIterator implements OuterIterator, ArrayAccess, Countable
{
    function __construct(Iterator $iterator, int $flags = self::CALL_TOSTRING) {}
    function count() : int {}
    function current() : void {}
    function getCache() : array {}
    function getFlags() : int {}
    function getInnerIterator() : Iterator {}
    function hasNext() : void {}
    function key() : scalar {}
    function next() : void {}
    function offsetExists(string $index) : void {}
    function offsetGet(string $index) : void {}
    function offsetSet(string $index, string $newval) : void {}
    function offsetUnset(string $index) : void {}
    function rewind() : void {}
    function setFlags(int $flags) : void {}
    function __toString() : void {}
    function valid() : void {}
}

class CallbackFilterIterator extends FilterIterator implements OuterIterator
{
    function accept() : string {}
}

class ClosedGeneratorException
{
}

class Closure
{
    function __construct() {}
    function bind(Closure $closure, object $newthis, $newscope = "static") : Closure {}
    function bindTo(object $newthis, $newscope = "static") : Closure {}
    function call(object $newthis, ...$__variadic) {}
    function fromCallable(callable $callable) : Closure {}
}

class CURLFile
{
    function __construct(string $filename, string $mimetype = '', string $postname = '') {}
    function getFilename() : string {}
    function getMimeType() : string {}
    function getPostFilename() : string {}
    function setMimeType(string $mime) : void {}
    function setPostFilename(string $postname) : void {}
    function __wakeup() : void {}
}

class DateInterval
{
    function createFromDateString(string $time) : DateInterval {}
    function format(string $format) : string {}
}

class DatePeriod implements Traversable
{
    function getDateInterval() : DateInterval {}
    function getEndDate() : DateTimeInterface {}
    function getStartDate() : DateTimeInterface {}
}

class DateTime implements DateTimeInterface
{
    function add(DateInterval $interval) : DateTime {}
    function createFromFormat(string $format, string $time, DateTimeZone $timezone = null) : DateTime {}
    function getLastErrors() : array {}
    function modify(string $modify) : DateTime {}
    function __set_state(array $array) : DateTime {}
    function setDate(int $year, int $month, int $day) : DateTime {}
    function setISODate(int $year, int $week, int $day = 1) : DateTime {}
    function setTime(int $hour, int $minute, int $second = 0) : DateTime {}
    function setTimestamp(int $unixtimestamp) : DateTime {}
    function setTimezone(DateTimeZone $timezone) : DateTime {}
    function sub(DateInterval $interval) : DateTime {}
    function diff(DateTimeInterface $datetime2, bool $absolute = false) : DateInterval {}
    function format(string $format) : string {}
    function getOffset() : int {}
    function getTimestamp() : int {}
    function getTimezone() : DateTimeZone {}
    function __wakeup() {}
}

class DateTimeImmutable implements DateTimeInterface
{
    function add(DateInterval $interval) : DateTimeImmutable {}
    function createFromFormat(string $format, string $time, DateTimeZone $timezone = null) : DateTimeImmutable {}
    function createFromMutable(DateTime $datetime) : DateTimeImmutable {}
    function getLastErrors() : array {}
    function modify(string $modify) : DateTimeImmutable {}
    function __set_state(array $array) : DateTimeImmutable {}
    function setDate(int $year, int $month, int $day) : DateTimeImmutable {}
    function setISODate(int $year, int $week, int $day = 1) : DateTimeImmutable {}
    function setTime(int $hour, int $minute, int $second = 0) : DateTimeImmutable {}
    function setTimestamp(int $unixtimestamp) : DateTimeImmutable {}
    function setTimezone(DateTimeZone $timezone) : DateTimeImmutable {}
    function sub(DateInterval $interval) : DateTimeImmutable {}
    function diff(DateTimeInterface $datetime2, bool $absolute = false) : DateInterval {}
    function format(string $format) : string {}
    function getOffset() : int {}
    function getTimestamp() : int {}
    function getTimezone() : DateTimeZone {}
    function __wakeup() {}
}

interface DateTimeInterface
{
    function format(string $format) : string {}
}

class DateTimeZone
{
    function getLocation() : array {}
    function getName() : string {}
    function getOffset(DateTime $datetime) : int {}
    function getTransitions(int $timestamp_begin = 0, int $timestamp_end = 0) : array {}
    function listAbbreviations() : array {}
    function listIdentifiers(int $what = DateTimeZone::ALL, string $country = null) : array {}
}

class Directory
{
    function close(resource $dir_handle = null) : void {}
    function read(resource $dir_handle = null) : string {}
    function rewind(resource $dir_handle = null) : void {}
}

class DirectoryIterator extends SplFileInfo implements SeekableIterator
{
    function current() : DirectoryIterator {}
    function getATime() : int {}
    function getBasename(string $suffix = '') : string {}
    function getCTime() : int {}
    function getExtension() : string {}
    function getFilename() : string {}
    function getGroup() : int {}
    function getInode() : int {}
    function getMTime() : int {}
    function getOwner() : int {}
    function getPath() : string {}
    function getPathname() : string {}
    function getPerms() : int {}
    function getSize() : int {}
    function getType() : string {}
    function isDir() : bool {}
    function isDot() : bool {}
    function isExecutable() : bool {}
    function isFile() : bool {}
    function isLink() : bool {}
    function isReadable() : bool {}
    function isWritable() : bool {}
    function key() : string {}
    function next() : void {}
    function rewind() : void {}
    function seek(int $position) : void {}
    function __toString() : string {}
    function valid() : bool {}
}

class DivisionByZeroError extends ArithmeticError
{
}

class DomainException extends LogicException
{
}

class DOMAttr extends DOMNode
{
    function __construct(string $name, string $value = '') {}
    function isId() : bool {}
}

class DOMCdataSection extends DOMText
{
}

class DOMCharacterData extends DOMNode
{
    function appendData(string $data) : void {}
    function deleteData(int $offset, int $count) : void {}
    function insertData(int $offset, string $data) : void {}
    function replaceData(int $offset, int $count, string $data) : void {}
    function substringData(int $offset, int $count) : string {}
}

class DOMComment extends DOMCharacterData
{
    function __construct(string $value = '') {}
}

class DOMDocument extends DOMNode
{
    function __construct(string $version = '', string $encoding = '') {}
    function createAttribute(string $name) : DOMAttr {}
    function createAttributeNS(string $namespaceURI, string $qualifiedName) : DOMAttr {}
    function createCDATASection(string $data) : DOMCDATASection {}
    function createComment(string $data) : DOMComment {}
    function createDocumentFragment() : DOMDocumentFragment {}
    function createElement(string $name, string $value = '') : DOMElement {}
    function createElementNS(string $namespaceURI, string $qualifiedName, string $value = '') : DOMElement {}
    function createEntityReference(string $name) : DOMEntityReference {}
    function createProcessingInstruction(string $target, string $data = '') : DOMProcessingInstruction {}
    function createTextNode(string $content) : DOMText {}
    function getElementById(string $elementId) : DOMElement {}
    function getElementsByTagName(string $name) : DOMNodeList {}
    function getElementsByTagNameNS(string $namespaceURI, string $localName) : DOMNodeList {}
    function importNode(DOMNode $importedNode, bool $deep = false) : DOMNode {}
    function load(string $filename, int $options = 0) {}
    function loadHTML(string $source, int $options = 0) : bool {}
    function loadHTMLFile(string $filename, int $options = 0) : bool {}
    function loadXML(string $source, int $options = 0) {}
    function normalizeDocument() : void {}
    function registerNodeClass(string $baseclass, string $extendedclass) : bool {}
    function relaxNGValidate(string $filename) : bool {}
    function relaxNGValidateSource(string $source) : bool {}
    function save(string $filename, int $options = 0) : int {}
    function saveHTML(DOMNode $node = null) : string {}
    function saveHTMLFile(string $filename) : int {}
    function saveXML(DOMNode $node = null, int $options = 0) : string {}
    function schemaValidate(string $filename, int $flags = 0) : bool {}
    function schemaValidateSource(string $source, int $flags = 0) : bool {}
    function validate() : bool {}
    function xinclude(int $options = 0) : int {}
}

class DOMDocumentFragment extends DOMNode
{
    function appendXML(string $data) : bool {}
}

class DOMDocumentType extends DOMNode
{
}

class DOMElement extends DOMNode
{
    function __construct(string $name, string $value = '', string $namespaceURI = '') {}
    function getAttribute(string $name) : string {}
    function getAttributeNode(string $name) : DOMAttr {}
    function getAttributeNodeNS(string $namespaceURI, string $localName) : DOMAttr {}
    function getAttributeNS(string $namespaceURI, string $localName) : string {}
    function getElementsByTagName(string $name) : DOMNodeList {}
    function getElementsByTagNameNS(string $namespaceURI, string $localName) : DOMNodeList {}
    function hasAttribute(string $name) : bool {}
    function hasAttributeNS(string $namespaceURI, string $localName) : bool {}
    function removeAttribute(string $name) : bool {}
    function removeAttributeNode(DOMAttr $oldnode) : bool {}
    function removeAttributeNS(string $namespaceURI, string $localName) : bool {}
    function setAttribute(string $name, string $value) : DOMAttr {}
    function setAttributeNode(DOMAttr $attr) : DOMAttr {}
    function setAttributeNodeNS(DOMAttr $attr) : DOMAttr {}
    function setAttributeNS(string $namespaceURI, string $qualifiedName, string $value) : void {}
    function setIdAttribute(string $name, bool $isId) : void {}
    function setIdAttributeNode(DOMAttr $attr, bool $isId) : void {}
    function setIdAttributeNS(string $namespaceURI, string $localName, bool $isId) : void {}
}

class DOMEntity extends DOMNode
{
}

class DOMEntityReference extends DOMNode
{
    function __construct(string $name) {}
}

class DOMException extends Exception
{
}

class DOMImplementation
{
    function __construct() {}
    function createDocument(string $namespaceURI = null, string $qualifiedName = null, DOMDocumentType $doctype = null) : DOMDocument {}
    function createDocumentType(string $qualifiedName = null, string $publicId = null, string $systemId = null) : DOMDocumentType {}
    function hasFeature(string $feature, string $version) : bool {}
}

class DOMNamedNodeMap implements Traversable
{
    function getNamedItem(string $name) : DOMNode {}
    function getNamedItemNS(string $namespaceURI, string $localName) : DOMNode {}
    function item(int $index) : DOMNode {}
}

class DOMNode
{
    function appendChild(DOMNode $newnode) : DOMNode {}
    function C14N(bool $exclusive = false, bool $with_comments = false, array $xpath = null, array $ns_prefixes = null) : string {}
    function C14NFile(string $uri, bool $exclusive = false, bool $with_comments = false, array $xpath = null, array $ns_prefixes = null) : int {}
    function cloneNode(bool $deep = false) : DOMNode {}
    function getLineNo() : int {}
    function getNodePath() : string {}
    function hasAttributes() : bool {}
    function hasChildNodes() : bool {}
    function insertBefore(DOMNode $newnode, DOMNode $refnode = null) : DOMNode {}
    function isDefaultNamespace(string $namespaceURI) : bool {}
    function isSameNode(DOMNode $node) : bool {}
    function isSupported(string $feature, string $version) : bool {}
    function lookupNamespaceURI(string $prefix) : string {}
    function lookupPrefix(string $namespaceURI) : string {}
    function normalize() : void {}
    function removeChild(DOMNode $oldnode) : DOMNode {}
    function replaceChild(DOMNode $newnode, DOMNode $oldnode) : DOMNode {}
}

class DOMNodeList implements Traversable
{
    function item(int $index) : DOMElement {}
}

class DOMNotation extends DOMNode
{
}

class DOMProcessingInstruction extends DOMNode
{
    function __construct(string $name, string $value = '') {}
}

class DOMText extends DOMCharacterData
{
    function isWhitespaceInElementContent() : bool {}
    function splitText(int $offset) : DOMText {}
}

class DOMXPath
{
    function __construct(DOMDocument $doc) {}
    function evaluate(string $expression, DOMNode $contextnode = null, bool $registerNodeNS = true) {}
    function query(string $expression, DOMNode $contextnode = null, bool $registerNodeNS = true) : DOMNodeList {}
    function registerNamespace(string $prefix, string $namespaceURI) : bool {}
    function registerPhpFunctions($restrict = null) : void {}
}

class EmptyIterator implements Iterator
{
    function current() {}
    function key() : scalar {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class Error implements Throwable
{
    function getMessage() : string {}
    function getPrevious() : Throwable {}
    function getCode() {}
    function getFile() : string {}
    function getLine() : int {}
    function getTrace() : array {}
    function getTraceAsString() : string {}
    function __toString() : string {}
    function __clone() : void {}
}

class ErrorException extends Exception
{
    function getSeverity() : int {}
}

class Exception implements Throwable
{
    function getMessage() : string {}
    function getPrevious() : Throwable {}
    function getCode() {}
    function getFile() : string {}
    function getLine() : int {}
    function getTrace() : array {}
    function getTraceAsString() : string {}
    function __toString() : string {}
    function __clone() : void {}
}

class FilesystemIterator extends DirectoryIterator implements SeekableIterator
{
    function __construct(string $path, int $flags = FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS) {}
    function current() {}
    function getFlags() : int {}
    function key() : string {}
    function next() : void {}
    function rewind() : void {}
    function setFlags(int $flags = 0) : void {}
}

abstract class FilterIterator extends IteratorIterator implements OuterIterator
{
    function accept() : bool {}
    function __construct(Iterator $iterator) {}
    function current() {}
    function getInnerIterator() : Iterator {}
    function key() {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class finfo
{
    function buffer(string $string, int $options = FILEINFO_NONE, resource $context = null) : string {}
    function file(string $file_name, int $options = FILEINFO_NONE, resource $context = null) : string {}
    function set_flags(int $options) : bool {}
}

class Generator implements Iterator
{
    function current() {}
    function getReturn() {}
    function key() {}
    function next() : void {}
    function rewind() : void {}
    function send($value) {}
    function throw(Throwable $exception) {}
    function valid() : bool {}
    function __wakeup() : void {}
}

class GlobIterator extends FilesystemIterator implements SeekableIterator, Countable
{
    function __construct(string $path, int $flags = FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO) {}
    function count() : int {}
}

class InfiniteIterator extends IteratorIterator implements OuterIterator
{
    function __construct(Iterator $iterator) {}
    function next() : void {}
}

class InvalidArgumentException extends LogicException
{
}

class IteratorIterator implements OuterIterator
{
    function __construct(Traversable $iterator) {}
    function current() {}
    function getInnerIterator() : Traversable {}
    function key() : scalar {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class LengthException extends LogicException
{
}

class libXMLError
{
}

class LimitIterator extends IteratorIterator implements OuterIterator
{
    function __construct(Iterator $iterator, int $offset = 0, int $count = -1) {}
    function current() {}
    function getInnerIterator() : Iterator {}
    function getPosition() : int {}
    function key() {}
    function next() : void {}
    function rewind() : void {}
    function seek(int $position) : int {}
    function valid() : bool {}
}

class LogicException extends Exception
{
}

class MultipleIterator implements Iterator
{
    function attachIterator(Iterator $iterator, string $infos = '') : void {}
    function containsIterator(Iterator $iterator) : bool {}
    function countIterators() : int {}
    function current() : array {}
    function detachIterator(Iterator $iterator) : void {}
    function getFlags() : int {}
    function key() : array {}
    function next() : void {}
    function rewind() : void {}
    function setFlags(int $flags) : void {}
    function valid() : bool {}
}

class mysqli
{
    function autocommit(bool $mode) : bool {}
    function change_user(string $user, string $password, string $database) : bool {}
    function character_set_name() : string {}
    function close() : bool {}
    function commit(int $flags = 0, string $name = '') : bool {}
    function debug(string $message) : bool {}
    function dump_debug_info() : bool {}
    function get_charset() : object {}
    function get_client_info() : string {}
    function get_connection_stats() : bool {}
    function get_warnings() : mysqli_warning {}
    function init() : mysqli {}
    function kill(int $processid) : bool {}
    function more_results() : bool {}
    function multi_query(string $query) : bool {}
    function next_result() : bool {}
    function options(int $option, $value) : bool {}
    function ping() : bool {}
    function poll(array &$read, array &$error, array &$reject, int $sec, int $usec = 0) : int {}
    function prepare(string $query) : mysqli_stmt {}
    function query(string $query, int $resultmode = MYSQLI_STORE_RESULT) {}
    function real_connect(string $host = '', string $username = '', string $passwd = '', string $dbname = '', int $port = 0, string $socket = '', int $flags = 0) : bool {}
    function escape_string(string $escapestr) : string {}
    function real_query(string $query) : bool {}
    function reap_async_query() : mysqli_result {}
    function refresh(int $options) : bool {}
    function rollback(int $flags = 0, string $name = '') : bool {}
    function rpl_query_type(string $query) : int {}
    function select_db(string $dbname) : bool {}
    function send_query(string $query) : bool {}
    function set_charset(string $charset) : bool {}
    function set_local_infile_handler(mysqli $link, callable $read_func) : bool {}
    function ssl_set(string $key, string $cert, string $ca, string $capath, string $cipher) : bool {}
    function stat() : string {}
    function stmt_init() : mysqli_stmt {}
    function store_result(int $option = 0) : mysqli_result {}
    function use_result() : mysqli_result {}
}

class NoRewindIterator extends IteratorIterator
{
    function __construct(Iterator $iterator) {}
    function current() {}
    function getInnerIterator() : iterator {}
    function key() {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class OutOfBoundsException extends RuntimeException
{
}

class OutOfRangeException extends LogicException
{
}

class OverflowException extends RuntimeException
{
}

class ParentIterator extends RecursiveFilterIterator implements RecursiveIterator, OuterIterator
{
    function accept() : bool {}
    function __construct(RecursiveIterator $iterator) {}
    function getChildren() : ParentIterator {}
    function hasChildren() : bool {}
    function next() : void {}
    function rewind() : void {}
}

class ParseError extends Error
{
}

class PDO
{
    function beginTransaction() : bool {}
    function commit() : bool {}
    function errorCode() {}
    function errorInfo() : array {}
    function exec(string $statement) : int {}
    function getAttribute(int $attribute) {}
    function getAvailableDrivers() : array {}
    function inTransaction() : bool {}
    function lastInsertId(string $name = null) : string {}
    function prepare(string $statement, array $driver_options = null) : PDOStatement {}
    function query(string $statement) : PDOStatement {}
    function quote(string $string, int $parameter_type = PDO::PARAM_STR) : string {}
    function rollBack() : bool {}
    function setAttribute(int $attribute, $value) : bool {}
}

class PDOException extends RuntimeException
{
}

class PDOStatement implements Traversable
{
    function bindColumn($column, &$param, int $type = 0, int $maxlen = 0, $driverdata = null) : bool {}
    function bindParam($parameter, &$variable, int $data_type = PDO::PARAM_STR, int $length = 0, $driver_options = null) : bool {}
    function bindValue($parameter, $value, int $data_type = PDO::PARAM_STR) : bool {}
    function closeCursor() : bool {}
    function columnCount() : int {}
    function debugDumpParams() : void {}
    function errorCode() : string {}
    function errorInfo() : array {}
    function execute(array $input_parameters = null) : bool {}
    function fetch(int $fetch_style = 0, int $cursor_orientation = PDO::FETCH_ORI_NEXT, int $cursor_offset = 0) {}
    function fetchAll(int $fetch_style = 0, $fetch_argument = null, array $ctor_args = null) : array {}
    function fetchColumn(int $column_number = 0) {}
    function fetchObject(string $class_name = "stdClass", array $ctor_args = null) {}
    function getAttribute(int $attribute) {}
    function getColumnMeta(int $column) : array {}
    function nextRowset() : bool {}
    function rowCount() : int {}
    function setAttribute(int $attribute, $value) : bool {}
    function setFetchMode(int $mode) : bool {}
}

class Phar extends RecursiveDirectoryIterator implements Countable, ArrayAccess
{
    function addEmptyDir(string $dirname) : void {}
    function addFile(string $file, string $localname = '') : void {}
    function addFromString(string $localname, string $contents) : void {}
    function apiVersion() : string {}
    function buildFromDirectory(string $base_dir, string $regex = '') : array {}
    function buildFromIterator(Iterator $iter, string $base_directory = '') : array {}
    function canCompress(int $type = 0) : bool {}
    function canWrite() : bool {}
    function compress(int $compression, string $extension = '') : object {}
    function compressAllFilesBZIP2() : bool {}
    function compressAllFilesGZ() : bool {}
    function compressFiles(int $compression) : void {}
    function __construct(string $fname, int $flags = 0, string $alias = '') {}
    function convertToData(int $format = 9021976, int $compression = 9021976, string $extension = '') : PharData {}
    function convertToExecutable(int $format = 9021976, int $compression = 9021976, string $extension = '') : Phar {}
    function copy(string $oldfile, string $newfile) : bool {}
    function count() : int {}
    function createDefaultStub(string $indexfile = '', string $webindexfile = '') : string {}
    function decompress(string $extension = '') : object {}
    function decompressFiles() : bool {}
    function delMetadata() : bool {}
    function delete(string $entry) : bool {}
    function extractTo(string $pathto, $files = null, bool $overwrite = false) : bool {}
    function getMetadata() {}
    function getModified() : bool {}
    function getSignature() : array {}
    function getStub() : string {}
    function getSupportedCompression() : array {}
    function getSupportedSignatures() : array {}
    function getVersion() : string {}
    function hasMetadata() : bool {}
    function interceptFileFuncs() : void {}
    function isBuffering() : bool {}
    function isCompressed() {}
    function isFileFormat(int $format) : bool {}
    function isValidPharFilename(string $filename, bool $executable = true) : bool {}
    function isWritable() : bool {}
    function loadPhar(string $filename, string $alias = '') : bool {}
    function mapPhar(string $alias = '', int $dataoffset = 0) : bool {}
    function mount(string $pharpath, string $externalpath) : void {}
    function mungServer(array $munglist) : void {}
    function offsetExists(string $offset) : bool {}
    function offsetGet(string $offset) : int {}
    function offsetSet(string $offset, string $value) : void {}
    function offsetUnset(string $offset) : bool {}
    function running(bool $retphar = true) : string {}
    function setAlias(string $alias) : bool {}
    function setDefaultStub(string $index = '', string $webindex = '') : bool {}
    function setMetadata($metadata) : void {}
    function setSignatureAlgorithm(int $sigtype, string $privatekey = '') : void {}
    function setStub(string $stub, int $len = -1) : bool {}
    function startBuffering() : void {}
    function stopBuffering() : void {}
    function uncompressAllFiles() : bool {}
    function unlinkArchive(string $archive) : bool {}
    function webPhar(string $alias = '', string $index = "index.php", string $f404 = '', array $mimetypes = null, callable $rewrites = null) : void {}
}

class PharData extends Phar
{
    function addEmptyDir(string $dirname) : bool {}
    function addFromString(string $localname, string $contents) : bool {}
    function buildFromIterator(Iterator $iter, string $base_directory = '') : array {}
    function compress(int $compression, string $extension = '') : object {}
    function compressFiles(int $compression) : bool {}
    function __construct(string $fname, int $flags = 0, string $alias = '', int $format = Phar::TAR) {}
    function convertToData(int $format = 0, int $compression = 0, string $extension = '') : PharData {}
    function convertToExecutable(int $format = 0, int $compression = 0, string $extension = '') : Phar {}
    function copy(string $oldfile, string $newfile) : bool {}
    function decompress(string $extension = '') : object {}
    function decompressFiles() : bool {}
    function delMetadata() : bool {}
    function delete(string $entry) : bool {}
    function extractTo(string $pathto, $files = null, bool $overwrite = false) : bool {}
    function isWritable() : bool {}
    function offsetSet(string $offset, string $value) : void {}
    function offsetUnset(string $offset) : bool {}
    function setAlias(string $alias) : bool {}
    function setDefaultStub(string $index = '', string $webindex = '') : bool {}
    function setStub(string $stub, int $len = -1) : bool {}
}

class PharException extends Exception
{
}

class PharFileInfo extends SplFileInfo
{
    function chmod(int $permissions) : void {}
    function compress(int $compression) : bool {}
    function __construct(string $entry) {}
    function decompress() : bool {}
    function delMetadata() : bool {}
    function getCRC32() : int {}
    function getCompressedSize() : int {}
    function getMetadata() {}
    function getPharFlags() : int {}
    function hasMetadata() : bool {}
    function isCRCChecked() : bool {}
    function isCompressed(int $compression_type = 9021976) : bool {}
    function isCompressedBZIP2() : bool {}
    function isCompressedGZ() : bool {}
    function setCompressedBZIP2() : bool {}
    function setCompressedGZ() : bool {}
    function setMetadata($metadata) : void {}
    function setUncompressed() : bool {}
}

class php_user_filter
{
    function filter(resource $in, resource $out, int &$consumed, bool $closing) : int {}
    function onClose() : void {}
    function onCreate() : bool {}
}

class RangeException extends RuntimeException
{
}

class RecursiveArrayIterator extends ArrayIterator implements RecursiveIterator
{
    function getChildren() : RecursiveArrayIterator {}
    function hasChildren() : bool {}
}

class RecursiveCachingIterator extends CachingIterator implements Countable, ArrayAccess, OuterIterator, RecursiveIterator
{
    function __construct(Iterator $iterator, string $flags = self::CALL_TOSTRING) {}
    function getChildren() : RecursiveCachingIterator {}
    function hasChildren() : bool {}
}

class RecursiveCallbackFilterIterator extends CallbackFilterIterator implements OuterIterator, RecursiveIterator
{
    function getChildren() : RecursiveCallbackFilterIterator {}
    function hasChildren() : bool {}
}

class RecursiveDirectoryIterator extends FilesystemIterator implements SeekableIterator, RecursiveIterator
{
    function __construct(string $path, int $flags = FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO) {}
    function getChildren() {}
    function getSubPath() : string {}
    function getSubPathname() : string {}
    function hasChildren(bool $allow_links = false) : bool {}
    function key() : string {}
    function next() : void {}
    function rewind() : void {}
}

abstract class RecursiveFilterIterator extends FilterIterator implements OuterIterator, RecursiveIterator
{
    function __construct(RecursiveIterator $iterator) {}
    function getChildren() : RecursiveFilterIterator {}
    function hasChildren() : bool {}
}

class RecursiveIteratorIterator implements OuterIterator
{
    function beginChildren() : void {}
    function beginIteration() : void {}
    function callGetChildren() : RecursiveIterator {}
    function callHasChildren() : bool {}
    function __construct(Traversable $iterator, int $mode = RecursiveIteratorIterator::LEAVES_ONLY, int $flags = 0) {}
    function current() {}
    function endChildren() : void {}
    function endIteration() : void {}
    function getDepth() : int {}
    function getInnerIterator() : iterator {}
    function getMaxDepth() {}
    function getSubIterator(int $level = 0) : RecursiveIterator {}
    function key() {}
    function next() : void {}
    function nextElement() : void {}
    function rewind() : void {}
    function setMaxDepth(string $max_depth = -1) : void {}
    function valid() : bool {}
}

class RecursiveRegexIterator extends RegexIterator implements RecursiveIterator
{
    function getChildren() : RecursiveRegexIterator {}
    function hasChildren() : bool {}
}

class RecursiveTreeIterator extends RecursiveIteratorIterator implements OuterIterator
{
    function beginChildren() : void {}
    function beginIteration() : RecursiveIterator {}
    function callGetChildren() : RecursiveIterator {}
    function callHasChildren() : bool {}
    function __construct($it, int $flags = RecursiveTreeIterator::BYPASS_KEY, int $cit_flags = CachingIterator::CATCH_GET_CHILD, int $mode = RecursiveIteratorIterator::SELF_FIRST) {}
    function current() : string {}
    function endChildren() : void {}
    function endIteration() : void {}
    function getEntry() : string {}
    function getPostfix() : void {}
    function getPrefix() : string {}
    function key() : string {}
    function next() : void {}
    function nextElement() : void {}
    function rewind() : void {}
    function setPrefixPart(int $part, string $value) : void {}
    function valid() : bool {}
}

class Reflection
{
    function export(Reflector $reflector, bool $return = false) : string {}
    function getModifierNames(int $modifiers) : array {}
}

class ReflectionClass implements Reflector
{
    function __construct($argument) {}
    function export($argument, bool $return = false) : string {}
    function getConstant(string $name) {}
    function getConstants() : array {}
    function getConstructor() : ReflectionMethod {}
    function getDefaultProperties() : array {}
    function getDocComment() : string {}
    function getEndLine() : int {}
    function getExtension() : ReflectionExtension {}
    function getExtensionName() : string {}
    function getFileName() : string {}
    function getInterfaceNames() : array {}
    function getInterfaces() : array {}
    function getMethod(string $name) : ReflectionMethod {}
    function getMethods(int $filter = 0) : array {}
    function getModifiers() : int {}
    function getName() : string {}
    function getNamespaceName() : string {}
    function getParentClass() : ReflectionClass {}
    function getProperties(int $filter = 0) : array {}
    function getProperty(string $name) : ReflectionProperty {}
    function getShortName() : string {}
    function getStartLine() : int {}
    function getStaticProperties() : array {}
    function getStaticPropertyValue(string $name, &$def_value = null) {}
    function getTraitAliases() : array {}
    function getTraitNames() : array {}
    function getTraits() : array {}
    function hasConstant(string $name) : bool {}
    function hasMethod(string $name) : bool {}
    function hasProperty(string $name) : bool {}
    function implementsInterface(string $interface) : bool {}
    function inNamespace() : bool {}
    function isAbstract() : bool {}
    function isAnonymous() : bool {}
    function isCloneable() : bool {}
    function isFinal() : bool {}
    function isInstance(object $object) : bool {}
    function isInstantiable() : bool {}
    function isInterface() : bool {}
    function isInternal() : bool {}
    function isIterateable() : bool {}
    function isSubclassOf(string $class) : bool {}
    function isTrait() : bool {}
    function isUserDefined() : bool {}
    function newInstance($args, ...$__variadic) : object {}
    function newInstanceArgs(array $args = null) : object {}
    function newInstanceWithoutConstructor() : object {}
    function setStaticPropertyValue(string $name, string $value) : void {}
    function __toString() : string {}
}

class ReflectionException extends Exception
{
}

class ReflectionExtension implements Reflector
{
    function __clone() : void {}
    function __construct(string $name) {}
    function export(string $name, string $return = false) : string {}
    function getClasses() : array {}
    function getClassNames() : array {}
    function getConstants() : array {}
    function getDependencies() : array {}
    function getFunctions() : array {}
    function getINIEntries() : array {}
    function getName() : string {}
    function getVersion() : string {}
    function info() : void {}
    function isPersistent() : void {}
    function isTemporary() : void {}
    function __toString() : string {}
}

class ReflectionFunction extends ReflectionFunctionAbstract implements Reflector
{
    function __construct($name) {}
    function export(string $name, string $return = '') : string {}
    function getClosure() : Closure {}
    function invoke($parameter = null, ...$__variadic) {}
    function invokeArgs(array $args) {}
    function isDisabled() : bool {}
    function __toString() : string {}
}

class ReflectionFunctionAbstract implements Reflector
{
    function __clone() : void {}
    function getClosureScopeClass() : ReflectionClass {}
    function getClosureThis() : object {}
    function getDocComment() : string {}
    function getEndLine() : int {}
    function getExtension() : ReflectionExtension {}
    function getExtensionName() : string {}
    function getFileName() : string {}
    function getName() : string {}
    function getNamespaceName() : string {}
    function getNumberOfParameters() : int {}
    function getNumberOfRequiredParameters() : int {}
    function getParameters() : array {}
    function getReturnType() : ReflectionType {}
    function getShortName() : string {}
    function getStartLine() : int {}
    function getStaticVariables() : array {}
    function hasReturnType() : bool {}
    function inNamespace() : bool {}
    function isClosure() : bool {}
    function isDeprecated() : bool {}
    function isGenerator() : bool {}
    function isInternal() : bool {}
    function isUserDefined() : bool {}
    function isVariadic() : bool {}
    function returnsReference() : bool {}
    function __toString() : void {}
}

class ReflectionGenerator
{
    function __construct(Generator $generator) {}
    function getExecutingFile() : string {}
    function getExecutingGenerator() : Generator {}
    function getExecutingLine() : int {}
    function getFunction() : ReflectionFunctionAbstract {}
    function getThis() : object {}
    function getTrace(int $options = DEBUG_BACKTRACE_PROVIDE_OBJECT) : array {}
}

class ReflectionMethod extends ReflectionFunctionAbstract implements Reflector
{
    function __construct($class, string $name) {}
    function export(string $class, string $name, bool $return = false) : string {}
    function getClosure(object $object) : Closure {}
    function getDeclaringClass() : ReflectionClass {}
    function getModifiers() : int {}
    function getPrototype() : ReflectionMethod {}
    function invoke(object $object, $parameter = null, ...$__variadic) {}
    function invokeArgs(object $object, array $args) {}
    function isAbstract() : bool {}
    function isConstructor() : bool {}
    function isDestructor() : bool {}
    function isFinal() : bool {}
    function isPrivate() : bool {}
    function isProtected() : bool {}
    function isPublic() : bool {}
    function isStatic() : bool {}
    function setAccessible(bool $accessible) : void {}
    function __toString() : string {}
}

class ReflectionObject extends ReflectionClass implements Reflector
{
    function __construct(object $argument) {}
    function export(string $argument, bool $return = false) : string {}
}

class ReflectionParameter implements Reflector
{
    function allowsNull() : bool {}
    function canBePassedByValue() : bool {}
    function __clone() : void {}
    function __construct(string $function, string $parameter) {}
    function export(string $function, string $parameter, bool $return = false) : string {}
    function getClass() : ReflectionClass {}
    function getDeclaringClass() : ReflectionClass {}
    function getDeclaringFunction() : ReflectionFunctionAbstract {}
    function getDefaultValue() {}
    function getDefaultValueConstantName() : string {}
    function getName() : string {}
    function getPosition() : int {}
    function getType() : ReflectionType {}
    function hasType() : bool {}
    function isArray() : bool {}
    function isCallable() : bool {}
    function isDefaultValueAvailable() : bool {}
    function isDefaultValueConstant() : bool {}
    function isOptional() : bool {}
    function isPassedByReference() : bool {}
    function isVariadic() : bool {}
    function __toString() : string {}
}

class ReflectionProperty implements Reflector
{
    function __clone() : void {}
    function __construct($class, string $name) {}
    function export($class, string $name, bool $return = false) : string {}
    function getDeclaringClass() : ReflectionClass {}
    function getDocComment() : string {}
    function getModifiers() : int {}
    function getName() : string {}
    function getValue(object $object = null) {}
    function isDefault() : bool {}
    function isPrivate() : bool {}
    function isProtected() : bool {}
    function isPublic() : bool {}
    function isStatic() : bool {}
    function setAccessible(bool $accessible) : void {}
    function setValue(object $object, $value) : void {}
    function __toString() : string {}
}

class ReflectionType
{
    function allowsNull() : bool {}
    function isBuiltin() : bool {}
    function __toString() : string {}
}

class ReflectionZendExtension implements Reflector
{
    function __clone() : void {}
    function __construct(string $name) {}
    function export(string $name, string $return = '') : string {}
    function getAuthor() : string {}
    function getCopyright() : string {}
    function getName() : string {}
    function getURL() : string {}
    function getVersion() : string {}
    function __toString() : string {}
}

class RegexIterator extends FilterIterator
{
    function accept() : bool {}
    function getFlags() : int {}
    function getMode() : int {}
    function getPregFlags() : int {}
    function getRegex() : string {}
    function setFlags(int $flags) : void {}
    function setMode(int $mode) : void {}
    function setPregFlags(int $preg_flags) : void {}
}

class RuntimeException extends Exception
{
}

class SessionHandler implements SessionHandlerInterface
{
    function close() : bool {}
    function create_sid() : string {}
    function destroy(string $session_id) : bool {}
    function gc(int $maxlifetime) : bool {}
    function open(string $save_path, string $session_name) : bool {}
    function read(string $session_id) : string {}
    function write(string $session_id, string $session_data) : bool {}
}

class SimpleXMLElement implements Traversable
{
    function addAttribute(string $name, string $value = '', string $namespace = '') : void {}
    function addChild(string $name, string $value = '', string $namespace = '') : SimpleXMLElement {}
    function asXML(string $filename = '') {}
    function attributes(string $ns = null, bool $is_prefix = false) : SimpleXMLElement {}
    function children(string $ns = '', bool $is_prefix = false) : SimpleXMLElement {}
    function count() : int {}
    function getDocNamespaces(bool $recursive = false, bool $from_root = true) : array {}
    function getName() : string {}
    function getNamespaces(bool $recursive = false) : array {}
    function registerXPathNamespace(string $prefix, string $ns) : bool {}
    function __toString() : string {}
    function xpath(string $path) : array {}
}

class SimpleXMLIterator extends SimpleXMLElement implements RecursiveIterator, Countable
{
    function current() {}
    function getChildren() : SimpleXMLIterator {}
    function hasChildren() : bool {}
    function key() {}
    function next() : void {}
    function rewind() : void {}
    function valid() : bool {}
}

class SoapClient
{
    function __call(string $function_name, string $arguments) {}
    function SoapClient($wsdl, array $options = null) {}
    function __doRequest(string $request, string $location, string $action, int $version, int $one_way = 0) : string {}
    function __getFunctions() : array {}
    function __getLastRequest() : string {}
    function __getLastRequestHeaders() : string {}
    function __getLastResponse() : string {}
    function __getLastResponseHeaders() : string {}
    function __getTypes() : array {}
    function __setCookie(string $name, string $value = '') : void {}
    function __setLocation(string $new_location = '') : string {}
    function __setSoapHeaders($soapheaders = null) : bool {}
    function __soapCall(string $function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null) {}
    function SoapClient($wsdl, array $options = null) {}
}

class SoapFault extends Exception
{
    function __construct(string $faultcode, string $faultstring, string $faultactor = '', string $detail = '', string $faultname = '', string $headerfault = '') {}
    function SoapFault(string $faultcode, string $faultstring, string $faultactor = '', string $detail = '', string $faultname = '', string $headerfault = '') {}
    function __toString() : string {}
}

class SoapHeader
{
    function __construct(string $namespace, string $name, $data = null, bool $mustunderstand = false, string $actor = '') {}
    function SoapHeader(string $namespace, string $name, $data = null, bool $mustunderstand = false, string $actor = '') {}
}

class SoapParam
{
    function __construct($data, string $name) {}
    function SoapParam($data, string $name) {}
}

class SoapServer
{
    function addFunction($functions) : void {}
    function addSoapHeader(SoapHeader $object) : void {}
    function __construct($wsdl, array $options = null) {}
    function fault(string $code, string $string, string $actor = '', string $details = '', string $name = '') : void {}
    function getFunctions() : array {}
    function handle(string $soap_request = '') : void {}
    function setClass(string $class_name, $args = null, ...$__variadic) : void {}
    function setObject(object $object) : void {}
    function setPersistence(int $mode) : void {}
    function SoapServer($wsdl, array $options = null) {}
}

class SoapVar
{
    function __construct($data, string $encoding, string $type_name = '', string $type_namespace = '', string $node_name = '', string $node_namespace = '') {}
    function SoapVar($data, string $encoding, string $type_name = '', string $type_namespace = '', string $node_name = '', string $node_namespace = '') {}
}

class SplDoublyLinkedList implements Iterator, ArrayAccess, Countable
{
    function add($index, $newval) : void {}
    function bottom() {}
    function count() : int {}
    function current() {}
    function getIteratorMode() : int {}
    function isEmpty() : bool {}
    function key() {}
    function next() : void {}
    function offsetExists($index) : bool {}
    function offsetGet($index) {}
    function offsetSet($index, $newval) : void {}
    function offsetUnset($index) : void {}
    function pop() {}
    function prev() : void {}
    function push($value) : void {}
    function rewind() : void {}
    function serialize() : string {}
    function setIteratorMode(int $mode) : void {}
    function shift() {}
    function top() {}
    function unserialize(string $serialized) : void {}
    function unshift($value) : void {}
    function valid() : bool {}
}

class SplFileInfo
{
    function __construct(string $file_name) {}
    function getATime() : int {}
    function getBasename(string $suffix = '') : string {}
    function getCTime() : int {}
    function getExtension() : string {}
    function getFileInfo(string $class_name = '') : SplFileInfo {}
    function getFilename() : string {}
    function getGroup() : int {}
    function getInode() : int {}
    function getLinkTarget() : string {}
    function getMTime() : int {}
    function getOwner() : int {}
    function getPath() : string {}
    function getPathInfo(string $class_name = '') : SplFileInfo {}
    function getPathname() : string {}
    function getPerms() : int {}
    function getRealPath() : string {}
    function getSize() : int {}
    function getType() : string {}
    function isDir() : bool {}
    function isExecutable() : bool {}
    function isFile() : bool {}
    function isLink() : bool {}
    function isReadable() : bool {}
    function isWritable() : bool {}
    function openFile(string $open_mode = "r", bool $use_include_path = false, resource $context = null) : SplFileObject {}
    function setFileClass(string $class_name = "SplFileObject") : void {}
    function setInfoClass(string $class_name = "SplFileInfo") : void {}
    function __toString() : void {}
}

class SplFileObject extends SplFileInfo implements RecursiveIterator, SeekableIterator
{
    function current() {}
    function eof() : bool {}
    function fflush() : bool {}
    function fgetc() : string {}
    function fgetcsv(string $delimiter = ",", string $enclosure = "\"", string $escape = "\\") : array {}
    function fgets() : string {}
    function fgetss(string $allowable_tags = '') : string {}
    function flock(int $operation, int &$wouldblock = 0) : bool {}
    function fpassthru() : int {}
    function fputcsv(array $fields, string $delimiter = ",", string $enclosure = '"', string $escape = "\\") : int {}
    function fread(int $length) : string {}
    function fscanf(string $format, &...$__variadic) {}
    function fseek(int $offset, int $whence = SEEK_SET) : int {}
    function fstat() : array {}
    function ftell() : int {}
    function ftruncate(int $size) : bool {}
    function fwrite(string $str, int $length = 0) : int {}
    function getChildren() : void {}
    function getCsvControl() : array {}
    function getFlags() : int {}
    function getMaxLineLen() : int {}
    function hasChildren() : bool {}
    function key() : int {}
    function next() : void {}
    function rewind() : void {}
    function seek(int $line_pos) : void {}
    function setCsvControl(string $delimiter = ",", string $enclosure = "\"", string $escape = "\\") : void {}
    function setFlags(int $flags) : void {}
    function setMaxLineLen(int $max_len) : void {}
    function __toString() : void {}
    function valid() : bool {}
}

class SplFixedArray implements Iterator, ArrayAccess, Countable
{
    function count() : int {}
    function current() {}
    function fromArray(array $array, bool $save_indexes = true) : SplFixedArray {}
    function getSize() : int {}
    function key() : int {}
    function next() : void {}
    function offsetExists(int $index) : bool {}
    function offsetGet(int $index) {}
    function offsetSet(int $index, $newval) : void {}
    function offsetUnset(int $index) : void {}
    function rewind() : void {}
    function setSize(int $size) : int {}
    function toArray() : array {}
    function valid() : bool {}
    function __wakeup() : void {}
}

abstract class SplHeap implements Iterator, Countable
{
    function compare($value1, $value2) : int {}
    function count() : int {}
    function current() {}
    function extract() {}
    function insert($value) : void {}
    function isEmpty() : bool {}
    function key() {}
    function next() : void {}
    function recoverFromCorruption() : void {}
    function rewind() : void {}
    function top() {}
    function valid() : bool {}
}

class SplMaxHeap extends SplHeap implements Iterator, Countable
{
    function compare($value1, $value2) : int {}
}

class SplMinHeap extends SplHeap implements Iterator, Countable
{
    function compare($value1, $value2) : int {}
}

class SplObjectStorage implements Countable, Iterator, Serializable, ArrayAccess
{
    function addAll(SplObjectStorage $storage) : void {}
    function attach(object $object, $data = null) : void {}
    function contains(object $object) : bool {}
    function count() : int {}
    function current() : object {}
    function detach(object $object) : void {}
    function getHash(object $object) : string {}
    function getInfo() {}
    function key() : int {}
    function next() : void {}
    function offsetExists(object $object) : bool {}
    function offsetGet(object $object) {}
    function offsetSet(object $object, $data = null) : void {}
    function offsetUnset(object $object) : void {}
    function removeAll(SplObjectStorage $storage) : void {}
    function removeAllExcept(SplObjectStorage $storage) : void {}
    function rewind() : void {}
    function serialize() : string {}
    function setInfo($data) : void {}
    function unserialize(string $serialized) : void {}
    function valid() : bool {}
}

class SplPriorityQueue implements Iterator, Countable
{
    function compare($priority1, $priority2) : int {}
    function count() : int {}
    function current() {}
    function extract() {}
    function insert($value, $priority) : void {}
    function isEmpty() : bool {}
    function key() {}
    function next() : void {}
    function recoverFromCorruption() : void {}
    function rewind() : void {}
    function setExtractFlags(int $flags) : void {}
    function top() {}
    function valid() : bool {}
}

class SplQueue extends SplDoublyLinkedList implements Iterator, ArrayAccess, Countable
{
    function dequeue() {}
    function enqueue($value) : void {}
    function setIteratorMode(int $mode) : void {}
}

class SplStack extends SplDoublyLinkedList implements Iterator, ArrayAccess, Countable
{
    function setIteratorMode(int $mode) : void {}
}

class SplTempFileObject extends SplFileObject implements SeekableIterator, RecursiveIterator
{
    function __construct(int $max_memory = 0) {}
}

class stdClass
{
}

interface Throwable
{
    abstract public function getMessage() : string;
    abstract public function getCode() : int;
    abstract public function getFile() : string;
    abstract public function getLine() : int;
    abstract public function getTrace() : array;
    abstract public function getTraceAsString() : string;
    abstract public function getPrevious() : Throwable;
    abstract public function __toString() : string;
}

class TypeError extends Error
{
}

class UnderflowException extends RuntimeException
{
}

class UnexpectedValueException extends RuntimeException
{
}

class XMLReader
{
    function close() : bool {}
    function expand(DOMNode $basenode = null) : DOMNode {}
    function getAttribute(string $name) : string {}
    function getAttributeNo(int $index) : string {}
    function getAttributeNs(string $localName, string $namespaceURI) : string {}
    function getParserProperty(int $property) : bool {}
    function isValid() : bool {}
    function lookupNamespace(string $prefix) : string {}
    function moveToAttribute(string $name) : bool {}
    function moveToAttributeNo(int $index) : bool {}
    function moveToAttributeNs(string $localName, string $namespaceURI) : bool {}
    function moveToElement() : bool {}
    function moveToFirstAttribute() : bool {}
    function moveToNextAttribute() : bool {}
    function next(string $localname = '') : bool {}
    function open(string $URI, string $encoding = '', int $options = 0) : bool {}
    function read() : bool {}
    function readInnerXML() : string {}
    function readOuterXML() : string {}
    function readString() : string {}
    function setParserProperty(int $property, bool $value) : bool {}
    function setRelaxNGSchema(string $filename) : bool {}
    function setRelaxNGSchemaSource(string $source) : bool {}
    function setSchema(string $filename) : bool {}
    function xml(string $source, string $encoding = '', int $options = 0) : bool {}
}

class XSLTProcessor
{
    function getParameter(string $namespaceURI, string $localName) : string {}
    function hasExsltSupport() : bool {}
    function importStylesheet(object $stylesheet) : bool {}
    function registerPHPFunctions($restrict = null) : void {}
    function removeParameter(string $namespaceURI, string $localName) : bool {}
    function setParameter(string $namespace, string $name, string $value) : bool {}
    function setProfiling(string $filename) : bool {}
    function transformToDoc(DOMNode $doc) : DOMDocument {}
    function transformToURI(DOMDocument $doc, string $uri) : int {}
    function transformToXML(object $doc) : string {}
}

class ZipArchive
{
    function addEmptyDir(string $dirname) : bool {}
    function addFile(string $filename, string $localname = null, int $start = 0, int $length = 0) : bool {}
    function addFromString(string $localname, string $contents) : bool {}
    function addGlob(string $pattern, int $flags = 0, array $options = null) : bool {}
    function addPattern(string $pattern, string $path = ".", array $options = null) : bool {}
    function close() : bool {}
    function deleteIndex(int $index) : bool {}
    function deleteName(string $name) : bool {}
    function extractTo(string $destination, $entries = null) : bool {}
    function getArchiveComment(int $flags = 0) : string {}
    function getCommentIndex(int $index, int $flags = 0) : string {}
    function getCommentName(string $name, int $flags = 0) : string {}
    function GetExternalAttributesIndex(int $index, int &$opsys, int &$attr, int $flags = 0) : bool {}
    function getExternalAttributesName(string $name, int &$opsys, int &$attr, int $flags = 0) : bool {}
    function getFromIndex(int $index, int $length = 0, int $flags = 0) : string {}
    function getFromName(string $name, int $length = 0, int $flags = 0) : string {}
    function getNameIndex(int $index, int $flags = 0) : string {}
    function getStatusString() : string {}
    function getStream(string $name) : resource {}
    function locateName(string $name, int $flags = 0) : int {}
    function open(string $filename, int $flags = 0) {}
    function renameIndex(int $index, string $newname) : bool {}
    function renameName(string $name, string $newname) : bool {}
    function setArchiveComment(string $comment) : bool {}
    function setCommentIndex(int $index, string $comment) : bool {}
    function setCommentName(string $name, string $comment) : bool {}
    function setCompressionIndex(int $index, int $comp_method, int $comp_flags = 0) : bool {}
    function setCompressionName(string $name, int $comp_method, int $comp_flags = 0) : bool {}
    function setExternalAttributesIndex(int $index, int $opsys, int $attr, int $flags = 0) : bool {}
    function setExternalAttributesName(string $name, int $opsys, int $attr, int $flags = 0) : bool {}
    function setPassword(string $password) : bool {}
    function statIndex(int $index, int $flags = 0) : array {}
    function statName(string $name, int $flags = 0) : array {}
    function unchangeAll() : bool {}
    function unchangeArchive() : bool {}
    function unchangeIndex(int $index) : bool {}
    function unchangeName(string $name) : bool {}
}
