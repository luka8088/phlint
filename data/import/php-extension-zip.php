<?php

function zip_close(resource $zip) {}
function zip_entry_close(resource $zip_entry) : bool {}
function zip_entry_compressedsize(resource $zip_entry) : int {}
function zip_entry_compressionmethod(resource $zip_entry) : string {}
function zip_entry_filesize(resource $zip_entry) : int {}
function zip_entry_name(resource $zip_entry) : string {}
function zip_entry_open(resource $zip, resource $zip_entry, string $mode = '') : bool {}
function zip_entry_read(resource $zip_entry, int $length = 1024) : string {}
function zip_open(string $filename) : resource {}
function zip_read(resource $zip) : resource {}
