<?php

const FILEINFO_CONTINUE = 32;
const FILEINFO_DEVICES = 8;
const FILEINFO_MIME = 1040;
const FILEINFO_MIME_ENCODING = 1024;
const FILEINFO_MIME_TYPE = 16;
const FILEINFO_NONE = 0;
const FILEINFO_PRESERVE_ATIME = 128;
const FILEINFO_RAW = 256;
const FILEINFO_SYMLINK = 2;

function finfo_buffer(resource $finfo, string $string, int $options = FILEINFO_NONE, resource $context = null) : string {}
function finfo_close(resource $finfo) : bool {}
function finfo_file(resource $finfo, string $file_name, int $options = FILEINFO_NONE, resource $context = null) : string {}
function finfo_open(int $options = FILEINFO_NONE, string $magic_file = null) : resource {}
function finfo_set_flags(resource $finfo, int $options) : bool {}
function mime_content_type(string $filename) : string {}
