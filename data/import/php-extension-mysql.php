<?php

function mysql_affected_rows(resource $link_identifier = null) : int {}
function mysql_client_encoding(resource $link_identifier = null) : string {}
function mysql_close(resource $link_identifier = null) : bool {}
function mysql_connect(string $server = ini_get("mysql.default_host"), string $username = ini_get("mysql.default_user"), string $password = ini_get("mysql.default_password"), bool $new_link = false, int $client_flags = 0) : resource {}
function mysql_create_db(string $database_name, resource $link_identifier = null) : bool {}
function mysql_data_seek(resource $result, int $row_number) : bool {}
function mysql_db_name(resource $result, int $row, $field = null) : string {}
function mysql_db_query(string $database, string $query, resource $link_identifier = null) : resource {}
function mysql_drop_db(string $database_name, resource $link_identifier = null) : bool {}
function mysql_errno(resource $link_identifier = null) : int {}
function mysql_error(resource $link_identifier = null) : string {}
function mysql_escape_string(string $unescaped_string) : string {}
function mysql_fetch_array(resource $result, int $result_type = MYSQL_BOTH) : array {}
function mysql_fetch_assoc(resource $result) : array {}
function mysql_fetch_field(resource $result, int $field_offset = 0) : object {}
function mysql_fetch_lengths(resource $result) : array {}
function mysql_fetch_object(resource $result, string $class_name = '', array $params = null) : object {}
function mysql_fetch_row(resource $result) : array {}
function mysql_field_flags(resource $result, int $field_offset) : string {}
function mysql_field_len(resource $result, int $field_offset) : int {}
function mysql_field_name(resource $result, int $field_offset) : string {}
function mysql_field_seek(resource $result, int $field_offset) : bool {}
function mysql_field_table(resource $result, int $field_offset) : string {}
function mysql_field_type(resource $result, int $field_offset) : string {}
function mysql_free_result(resource $result) : bool {}
function mysql_get_client_info() : string {}
function mysql_get_host_info(resource $link_identifier = null) : string {}
function mysql_get_proto_info(resource $link_identifier = null) : int {}
function mysql_get_server_info(resource $link_identifier = null) : string {}
function mysql_info(resource $link_identifier = null) : string {}
function mysql_insert_id(resource $link_identifier = null) : int {}
function mysql_list_dbs(resource $link_identifier = null) : resource {}
function mysql_list_fields(string $database_name, string $table_name, resource $link_identifier = null) : resource {}
function mysql_list_processes(resource $link_identifier = null) : resource {}
function mysql_list_tables(string $database, resource $link_identifier = null) : resource {}
function mysql_num_fields(resource $result) : int {}
function mysql_num_rows(resource $result) : int {}
function mysql_pconnect(string $server = ini_get("mysql.default_host"), string $username = ini_get("mysql.default_user"), string $password = ini_get("mysql.default_password"), int $client_flags = 0) : resource {}
function mysql_ping(resource $link_identifier = null) : bool {}
function mysql_query(string $query, resource $link_identifier = null) {}
function mysql_real_escape_string(string $unescaped_string, resource $link_identifier = null) : string {}
function mysql_result(resource $result, int $row, $field = 0) : string {}
function mysql_select_db(string $database_name, resource $link_identifier = null) : bool {}
function mysql_set_charset(string $charset, resource $link_identifier = null) : bool {}
function mysql_stat(resource $link_identifier = null) : string {}
function mysql_tablename(resource $result, int $i) : string {}
function mysql_thread_id(resource $link_identifier = null) : int {}
function mysql_unbuffered_query(string $query, resource $link_identifier = null) : resource {}
