<?php

const MYSQLI_ASSOC = 1;
const MYSQLI_ASYNC = 8;
const MYSQLI_AUTO_INCREMENT_FLAG = 512;
const MYSQLI_BINARY_FLAG = 128;
const MYSQLI_BLOB_FLAG = 16;
const MYSQLI_BOTH = 3;
const MYSQLI_CLIENT_CAN_HANDLE_EXPIRED_PASSWORDS = 4194304;
const MYSQLI_CLIENT_COMPRESS = 32;
const MYSQLI_CLIENT_FOUND_ROWS = 2;
const MYSQLI_CLIENT_IGNORE_SPACE = 256;
const MYSQLI_CLIENT_INTERACTIVE = 1024;
const MYSQLI_CLIENT_NO_SCHEMA = 16;
const MYSQLI_CLIENT_SSL = 2048;
const MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT = 64;
const MYSQLI_CLIENT_SSL_VERIFY_SERVER_CERT = 1073741824;
const MYSQLI_CURSOR_TYPE_FOR_UPDATE = 2;
const MYSQLI_CURSOR_TYPE_NO_CURSOR = 0;
const MYSQLI_CURSOR_TYPE_READ_ONLY = 1;
const MYSQLI_CURSOR_TYPE_SCROLLABLE = 4;
const MYSQLI_DATA_TRUNCATED = 101;
const MYSQLI_DEBUG_TRACE_ENABLED = 0;
const MYSQLI_ENUM_FLAG = 256;
const MYSQLI_GROUP_FLAG = 32768;
const MYSQLI_INIT_COMMAND = 3;
const MYSQLI_MULTIPLE_KEY_FLAG = 8;
const MYSQLI_NO_DATA = 100;
const MYSQLI_NO_DEFAULT_VALUE_FLAG = 4096;
const MYSQLI_NOT_NULL_FLAG = 1;
const MYSQLI_NUM = 2;
const MYSQLI_NUM_FLAG = 32768;
const MYSQLI_ON_UPDATE_NOW_FLAG = 8192;
const MYSQLI_OPT_CAN_HANDLE_EXPIRED_PASSWORDS = 29;
const MYSQLI_OPT_CONNECT_TIMEOUT = 0;
const MYSQLI_OPT_INT_AND_FLOAT_NATIVE = 201;
const MYSQLI_OPT_LOCAL_INFILE = 8;
const MYSQLI_OPT_NET_CMD_BUFFER_SIZE = 202;
const MYSQLI_OPT_NET_READ_BUFFER_SIZE = 203;
const MYSQLI_OPT_SSL_VERIFY_SERVER_CERT = 21;
const MYSQLI_PART_KEY_FLAG = 16384;
const MYSQLI_PRI_KEY_FLAG = 2;
const MYSQLI_READ_DEFAULT_FILE = 4;
const MYSQLI_READ_DEFAULT_GROUP = 5;
const MYSQLI_REFRESH_BACKUP_LOG = 2097152;
const MYSQLI_REFRESH_GRANT = 1;
const MYSQLI_REFRESH_HOSTS = 8;
const MYSQLI_REFRESH_LOG = 2;
const MYSQLI_REFRESH_MASTER = 128;
const MYSQLI_REFRESH_SLAVE = 64;
const MYSQLI_REFRESH_STATUS = 16;
const MYSQLI_REFRESH_TABLES = 4;
const MYSQLI_REFRESH_THREADS = 32;
const MYSQLI_REPORT_ALL = 255;
const MYSQLI_REPORT_ERROR = 1;
const MYSQLI_REPORT_INDEX = 4;
const MYSQLI_REPORT_OFF = 0;
const MYSQLI_REPORT_STRICT = 2;
const MYSQLI_SERVER_PS_OUT_PARAMS = 4096;
const MYSQLI_SERVER_PUBLIC_KEY = 27;
const MYSQLI_SERVER_QUERY_NO_GOOD_INDEX_USED = 16;
const MYSQLI_SERVER_QUERY_NO_INDEX_USED = 32;
const MYSQLI_SERVER_QUERY_WAS_SLOW = 2048;
const MYSQLI_SET_CHARSET_DIR = 6;
const MYSQLI_SET_CHARSET_NAME = 7;
const MYSQLI_SET_FLAG = 2048;
const MYSQLI_STMT_ATTR_CURSOR_TYPE = 1;
const MYSQLI_STMT_ATTR_PREFETCH_ROWS = 2;
const MYSQLI_STMT_ATTR_UPDATE_MAX_LENGTH = 0;
const MYSQLI_STORE_RESULT = 0;
const MYSQLI_STORE_RESULT_COPY_DATA = 16;
const MYSQLI_TIMESTAMP_FLAG = 1024;
const MYSQLI_TRANS_COR_AND_CHAIN = 1;
const MYSQLI_TRANS_COR_AND_NO_CHAIN = 2;
const MYSQLI_TRANS_COR_NO_RELEASE = 8;
const MYSQLI_TRANS_COR_RELEASE = 4;
const MYSQLI_TRANS_START_READ_ONLY = 4;
const MYSQLI_TRANS_START_READ_WRITE = 2;
const MYSQLI_TRANS_START_WITH_CONSISTENT_SNAPSHOT = 1;
const MYSQLI_TYPE_BIT = 16;
const MYSQLI_TYPE_BLOB = 252;
const MYSQLI_TYPE_CHAR = 1;
const MYSQLI_TYPE_DATE = 10;
const MYSQLI_TYPE_DATETIME = 12;
const MYSQLI_TYPE_DECIMAL = 0;
const MYSQLI_TYPE_DOUBLE = 5;
const MYSQLI_TYPE_ENUM = 247;
const MYSQLI_TYPE_FLOAT = 4;
const MYSQLI_TYPE_GEOMETRY = 255;
const MYSQLI_TYPE_INT24 = 9;
const MYSQLI_TYPE_INTERVAL = 247;
const MYSQLI_TYPE_JSON = 245;
const MYSQLI_TYPE_LONG = 3;
const MYSQLI_TYPE_LONG_BLOB = 251;
const MYSQLI_TYPE_LONGLONG = 8;
const MYSQLI_TYPE_MEDIUM_BLOB = 250;
const MYSQLI_TYPE_NEWDATE = 14;
const MYSQLI_TYPE_NEWDECIMAL = 246;
const MYSQLI_TYPE_NULL = 6;
const MYSQLI_TYPE_SET = 248;
const MYSQLI_TYPE_SHORT = 2;
const MYSQLI_TYPE_STRING = 254;
const MYSQLI_TYPE_TIME = 11;
const MYSQLI_TYPE_TIMESTAMP = 7;
const MYSQLI_TYPE_TINY = 1;
const MYSQLI_TYPE_TINY_BLOB = 249;
const MYSQLI_TYPE_VAR_STRING = 253;
const MYSQLI_TYPE_YEAR = 13;
const MYSQLI_UNIQUE_KEY_FLAG = 4;
const MYSQLI_UNSIGNED_FLAG = 32;
const MYSQLI_USE_RESULT = 1;
const MYSQLI_ZEROFILL_FLAG = 64;

function mysqli_affected_rows(mysqli $link) : int {}
function mysqli_autocommit(mysqli $link, bool $mode) : bool {}
function mysqli_begin_transaction(mysqli $link, int $flags = 0, string $name = '') : bool {}
function mysqli_change_user(mysqli $link, string $user, string $password, string $database) : bool {}
function mysqli_character_set_name(mysqli $link) : string {}
function mysqli_close(mysqli $link) : bool {}
function mysqli_commit(mysqli $link, int $flags = 0, string $name = '') : bool {}
function mysqli_connect(string $host = ini_get("mysqli.default_host"), string $username = ini_get("mysqli.default_user"), string $passwd = ini_get("mysqli.default_pw"), string $dbname = "", int $port = ini_get("mysqli.default_port"), string $socket = ini_get("mysqli.default_socket")) {}
function mysqli_connect_errno() : int {}
function mysqli_connect_error() : string {}
function mysqli_data_seek(mysqli_result $result, int $offset) : bool {}
function mysqli_debug(string $message) : bool {}
function mysqli_disable_reads_from_master(mysqli $link) : bool {}
function mysqli_disable_rpl_parse(mysqli $link) : bool {}
function mysqli_dump_debug_info(mysqli $link) : bool {}
function mysqli_embedded_server_end() {}
function mysqli_embedded_server_start(bool $start, array $arguments, array $groups) : bool {}
function mysqli_enable_reads_from_master(mysqli $link) : bool {}
function mysqli_enable_rpl_parse(mysqli $link) : bool {}
function mysqli_errno(mysqli $link) : int {}
function mysqli_error(mysqli $link) : string {}
function mysqli_error_list(mysqli $link) : array {}
function mysqli_escape_string($link, $query) {}
function mysqli_execute($stmt) {}
function mysqli_fetch_all(mysqli_result $result, int $resulttype = MYSQLI_NUM) {}
function mysqli_fetch_array(mysqli_result $result, int $resulttype = MYSQLI_BOTH) {}
function mysqli_fetch_assoc(mysqli_result $result) : array {}
function mysqli_fetch_field(mysqli_result $result) : object {}
function mysqli_fetch_field_direct(mysqli_result $result, int $fieldnr) : object {}
function mysqli_fetch_fields(mysqli_result $result) : array {}
function mysqli_fetch_lengths(mysqli_result $result) : array {}
function mysqli_fetch_object(mysqli_result $result, string $class_name = "stdClass", array $params = null) : object {}
function mysqli_fetch_row(mysqli_result $result) {}
function mysqli_field_count(mysqli $link) : int {}
function mysqli_field_seek(mysqli_result $result, int $fieldnr) : bool {}
function mysqli_field_tell(mysqli_result $result) : int {}
function mysqli_free_result(mysqli_result $result) {}
function mysqli_get_cache_stats() : array {}
function mysqli_get_charset(mysqli $link) : object {}
function mysqli_get_client_info(mysqli $link) : string {}
function mysqli_get_client_stats() : array {}
function mysqli_get_client_version(mysqli $link) : int {}
function mysqli_get_connection_stats(mysqli $link) : array {}
function mysqli_get_host_info(mysqli $link) : string {}
function mysqli_get_links_stats() : array {}
function mysqli_get_proto_info(mysqli $link) : int {}
function mysqli_get_server_info(mysqli $link) : string {}
function mysqli_get_server_version(mysqli $link) : int {}
function mysqli_get_warnings(mysqli $link) {}
function mysqli_info(mysqli $link) : string {}
function mysqli_init() {}
function mysqli_insert_id(mysqli $link) {}
function mysqli_kill(mysqli $link, int $processid) : bool {}
function mysqli_master_query(mysqli $link, string $query) : bool {}
function mysqli_more_results(mysqli $link) : bool {}
function mysqli_multi_query(mysqli $link, string $query) : bool {}
function mysqli_next_result(mysqli $link) : bool {}
function mysqli_num_fields(mysqli_result $result) : int {}
function mysqli_num_rows(mysqli_result $result) : int {}
function mysqli_options(mysqli $link, int $option, $value) : bool {}
function mysqli_ping(mysqli $link) : bool {}
function mysqli_poll(array &$read, array &$error, array &$reject, int $sec, int $usec = 0) : int {}
function mysqli_prepare(mysqli $link, string $query) {}
function mysqli_query(mysqli $link, string $query, int $resultmode = MYSQLI_STORE_RESULT) {}
function mysqli_real_connect(mysqli $link, string $host = '', string $username = '', string $passwd = '', string $dbname = '', int $port = 0, string $socket = '', int $flags = 0) : bool {}
function mysqli_real_escape_string(mysqli $link, string $escapestr) : string {}
function mysqli_real_query(mysqli $link, string $query) : bool {}
function mysqli_reap_async_query(mysqli $link) {}
function mysqli_refresh(resource $link, int $options) : int {}
function mysqli_release_savepoint(mysqli $link, string $name) : bool {}
function mysqli_report(int $flags) : bool {}
function mysqli_rollback(mysqli $link, int $flags = 0, string $name = '') : bool {}
function mysqli_rpl_parse_enabled(mysqli $link) : int {}
function mysqli_rpl_probe(mysqli $link) : bool {}
function mysqli_rpl_query_type(mysqli $link, string $query) : int {}
function mysqli_savepoint(mysqli $link, string $name) : bool {}
function mysqli_select_db(mysqli $link, string $dbname) : bool {}
function mysqli_send_query(mysqli $link, string $query) : bool {}
function mysqli_set_charset(mysqli $link, string $charset) : bool {}
function mysqli_set_local_infile_default(mysqli $link) {}
function mysqli_set_local_infile_handler(mysqli $link, callable $read_func) : bool {}
function mysqli_set_opt() {}
function mysqli_slave_query(mysqli $link, string $query) : bool {}
function mysqli_sqlstate(mysqli $link) : string {}
function mysqli_ssl_set(mysqli $link, string $key, string $cert, string $ca, string $capath, string $cipher) : bool {}
function mysqli_stat(mysqli $link) : string {}
function mysqli_stmt_affected_rows(mysqli_stmt $stmt) : int {}
function mysqli_stmt_attr_get(mysqli_stmt $stmt, int $attr) : int {}
function mysqli_stmt_attr_set(mysqli_stmt $stmt, int $attr, int $mode) : bool {}
function mysqli_stmt_bind_param(mysqli_stmt $stmt, string $types, &$var1, &...$__variadic) : bool {}
function mysqli_stmt_bind_result(mysqli_stmt $stmt, &$var1, &...$__variadic) : bool {}
function mysqli_stmt_close(mysqli_stmt $stmt) : bool {}
function mysqli_stmt_data_seek(mysqli_stmt $stmt, int $offset) {}
function mysqli_stmt_errno(mysqli_stmt $stmt) : int {}
function mysqli_stmt_error(mysqli_stmt $stmt) : string {}
function mysqli_stmt_error_list(mysqli_stmt $stmt) : array {}
function mysqli_stmt_execute(mysqli_stmt $stmt) : bool {}
function mysqli_stmt_fetch(mysqli_stmt $stmt) : bool {}
function mysqli_stmt_field_count(mysqli_stmt $stmt) : int {}
function mysqli_stmt_free_result(mysqli_stmt $stmt) {}
function mysqli_stmt_get_result(mysqli_stmt $stmt) {}
function mysqli_stmt_get_warnings(mysqli_stmt $stmt) : object {}
function mysqli_stmt_init(mysqli $link) {}
function mysqli_stmt_insert_id(mysqli_stmt $stmt) {}
function mysqli_stmt_more_results(mysql_stmt $stmt) : bool {}
function mysqli_stmt_next_result(mysql_stmt $stmt) : bool {}
function mysqli_stmt_num_rows(mysqli_stmt $stmt) : int {}
function mysqli_stmt_param_count(mysqli_stmt $stmt) : int {}
function mysqli_stmt_prepare(mysqli_stmt $stmt, string $query) : bool {}
function mysqli_stmt_reset(mysqli_stmt $stmt) : bool {}
function mysqli_stmt_result_metadata(mysqli_stmt $stmt) {}
function mysqli_stmt_send_long_data(mysqli_stmt $stmt, int $param_nr, string $data) : bool {}
function mysqli_stmt_sqlstate(mysqli_stmt $stmt) : string {}
function mysqli_stmt_store_result(mysqli_stmt $stmt) : bool {}
function mysqli_store_result(mysqli $link, int $option = 0) {}
function mysqli_thread_id(mysqli $link) : int {}
function mysqli_thread_safe() : bool {}
function mysqli_use_result(mysqli $link) {}
function mysqli_warning_count(mysqli $link) : int {}

class mysqli_driver
{
    function embedded_server_end() : void {}
    function embedded_server_start(bool $start, array $arguments, array $groups) : bool {}
}

class mysqli_result implements Traversable
{
    function data_seek(int $offset) : bool {}
    function fetch_all(int $resulttype = MYSQLI_NUM) {}
    function fetch_array(int $resulttype = MYSQLI_BOTH) {}
    function fetch_assoc() : array {}
    function fetch_field_direct(int $fieldnr) : object {}
    function fetch_field() : object {}
    function fetch_fields() : array {}
    function fetch_object(string $class_name = "stdClass", array $params = null) : object {}
    function fetch_row() {}
    function field_seek(int $fieldnr) : bool {}
    function free() : void {}
}

class mysqli_sql_exception extends RuntimeException
{
}

class mysqli_stmt
{
    function attr_get(int $attr) : int {}
    function attr_set(int $attr, int $mode) : bool {}
    function bind_param(string $types, &$var1, &...$__variadic) : bool {}
    function bind_result(&$var1, &...$__variadic) : bool {}
    function close() : bool {}
    function data_seek(int $offset) : void {}
    function execute() : bool {}
    function fetch() : bool {}
    function free_result() : void {}
    function get_result() : mysqli_result {}
    function get_warnings(mysqli_stmt $stmt) : object {}
    function prepare(string $query) {}
    function reset() : bool {}
    function result_metadata() : mysqli_result {}
    function send_long_data(int $param_nr, string $data) : bool {}
    function store_result() : bool {}
}

class mysqli_warning
{
    function __construct() {}
    function next() : void {}
}
