<?php

const HASH_HMAC = 1;
const MHASH_ADLER32 = 18;
const MHASH_CRC32 = 0;
const MHASH_CRC32B = 9;
const MHASH_FNV132 = 29;
const MHASH_FNV164 = 31;
const MHASH_FNV1A32 = 30;
const MHASH_FNV1A64 = 32;
const MHASH_GOST = 8;
const MHASH_HAVAL128 = 13;
const MHASH_HAVAL160 = 12;
const MHASH_HAVAL192 = 11;
const MHASH_HAVAL224 = 10;
const MHASH_HAVAL256 = 3;
const MHASH_JOAAT = 33;
const MHASH_MD2 = 28;
const MHASH_MD4 = 16;
const MHASH_MD5 = 1;
const MHASH_RIPEMD128 = 23;
const MHASH_RIPEMD160 = 5;
const MHASH_RIPEMD256 = 24;
const MHASH_RIPEMD320 = 25;
const MHASH_SHA1 = 2;
const MHASH_SHA224 = 19;
const MHASH_SHA256 = 17;
const MHASH_SHA384 = 21;
const MHASH_SHA512 = 20;
const MHASH_SNEFRU256 = 27;
const MHASH_TIGER = 7;
const MHASH_TIGER128 = 14;
const MHASH_TIGER160 = 15;
const MHASH_WHIRLPOOL = 22;

function hash(string $algo, string $data, bool $raw_output = false) : string {}
function hash_algos() : array {}
function hash_copy(resource $context) : resource {}
function hash_equals(string $known_string, string $user_string) : bool {}
function hash_file(string $algo, string $filename, bool $raw_output = false) : string {}
function hash_final(resource $context, bool $raw_output = false) : string {}
function hash_hkdf(string $algo, string $ikm, int $length = 0, string $info = '', string $salt = '') : string {}
function hash_hmac(string $algo, string $data, string $key, bool $raw_output = false) : string {}
function hash_hmac_file(string $algo, string $filename, string $key, bool $raw_output = false) : string {}
function hash_init(string $algo, int $options = 0, string $key = null) : resource {}
function hash_pbkdf2(string $algo, string $password, string $salt, int $iterations, int $length = 0, bool $raw_output = false) : string {}
function hash_update(resource $context, string $data) : bool {}
function hash_update_file(resource $hcontext, string $filename, resource $scontext = null) : bool {}
function hash_update_stream(resource $context, resource $handle, int $length = -1) : int {}
