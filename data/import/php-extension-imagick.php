<?php


class Imagick implements Iterator
{
    function adaptiveBlurImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function adaptiveResizeImage(int $columns, int $rows, bool $bestfit = false) : bool {}
    function adaptiveSharpenImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function adaptiveThresholdImage(int $width, int $height, int $offset) : bool {}
    function addImage(Imagick $source) : bool {}
    function addNoiseImage(int $noise_type, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function affineTransformImage(ImagickDraw $matrix) : bool {}
    function animateImages(string $x_server) : bool {}
    function annotateImage(ImagickDraw $draw_settings, float $x, float $y, float $angle, string $text) : bool {}
    function appendImages(bool $stack) : Imagick {}
    function autoLevelImage(string $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function averageImages() : Imagick {}
    function blackThresholdImage($threshold) : bool {}
    function blueShiftImage(float $factor = 1.5) : void {}
    function blurImage(float $radius, float $sigma, int $channel = 0) : bool {}
    function borderImage($bordercolor, int $width, int $height) : bool {}
    function brightnessContrastImage(string $brightness, string $contrast, string $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function charcoalImage(float $radius, float $sigma) : bool {}
    function chopImage(int $width, int $height, int $x, int $y) : bool {}
    function clampImage(string $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function clear() : bool {}
    function clipImage() : bool {}
    function clipImagePath(string $pathname, string $inside) : void {}
    function clipPathImage(string $pathname, bool $inside) : bool {}
    function clone() : Imagick {}
    function clutImage(Imagick $lookup_table, float $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function coalesceImages() : Imagick {}
    function colorFloodfillImage($fill, float $fuzz, $bordercolor, int $x, int $y) : bool {}
    function colorizeImage($colorize, $opacity) : bool {}
    function colorMatrixImage(string $color_matrix) : void {}
    function combineImages(int $channelType) : Imagick {}
    function commentImage(string $comment) : bool {}
    function compareImageChannels(Imagick $image, int $channelType, int $metricType) : array {}
    function compareImageLayers(int $method) : Imagick {}
    function compareImages(Imagick $compare, int $metric) : array {}
    function compositeImage(Imagick $composite_object, int $composite, int $x, int $y, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function __construct($files) {}
    function contrastImage(bool $sharpen) : bool {}
    function contrastStretchImage(float $black_point, float $white_point, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function convolveImage(array $kernel, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function count(string $mode = '') : void {}
    function cropImage(int $width, int $height, int $x, int $y) : bool {}
    function cropThumbnailImage(int $width, int $height) : bool {}
    function current() : Imagick {}
    function cycleColormapImage(int $displace) : bool {}
    function decipherImage(string $passphrase) : bool {}
    function deconstructImages() : Imagick {}
    function deleteImageArtifact(string $artifact) : bool {}
    function deleteImageProperty(string $name) : void {}
    function deskewImage(float $threshold) : bool {}
    function despeckleImage() : bool {}
    function destroy() : bool {}
    function displayImage(string $servername) : bool {}
    function displayImages(string $servername) : bool {}
    function distortImage(int $method, array $arguments, bool $bestfit) : bool {}
    function drawImage(ImagickDraw $draw) : bool {}
    function edgeImage(float $radius) : bool {}
    function embossImage(float $radius, float $sigma) : bool {}
    function encipherImage(string $passphrase) : bool {}
    function enhanceImage() : bool {}
    function equalizeImage() : bool {}
    function evaluateImage(int $op, float $constant, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function exportImagePixels(int $x, int $y, int $width, int $height, string $map, int $STORAGE) : array {}
    function extentImage(int $width, int $height, int $x, int $y) : bool {}
    function filter(ImagickKernel $ImagickKernel, int $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function flattenImages() : Imagick {}
    function flipImage() : bool {}
    function floodFillPaintImage($fill, float $fuzz, $target, int $x, int $y, bool $invert, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function flopImage() : bool {}
    function forwardFourierTransformimage(bool $magnitude) : void {}
    function frameImage($matte_color, int $width, int $height, int $inner_bevel, int $outer_bevel) : bool {}
    function functionImage(int $function, array $arguments, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function fxImage(string $expression, int $channel = Imagick::CHANNEL_ALL) : Imagick {}
    function gammaImage(float $gamma, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function gaussianBlurImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function getColorspace() : int {}
    function getCompression() : int {}
    function getCompressionQuality() : int {}
    function getCopyright() : string {}
    function getFilename() : string {}
    function getFont() : string {}
    function getFormat() : string {}
    function getGravity() : int {}
    function getHomeURL() : string {}
    function getImage() : Imagick {}
    function getImageAlphaChannel() : int {}
    function getImageArtifact(string $artifact) : string {}
    function getImageAttribute(string $key) : string {}
    function getImageBackgroundColor() : ImagickPixel {}
    function getImageBlob() : string {}
    function getImageBluePrimary() : array {}
    function getImageBorderColor() : ImagickPixel {}
    function getImageChannelDepth(int $channel) : int {}
    function getImageChannelDistortion(Imagick $reference, int $channel, int $metric) : float {}
    function getImageChannelDistortions(Imagick $reference, int $metric, int $channel = Imagick::CHANNEL_DEFAULT) : float {}
    function getImageChannelExtrema(int $channel) : array {}
    function getImageChannelKurtosis(int $channel = Imagick::CHANNEL_DEFAULT) : array {}
    function getImageChannelMean(int $channel) : array {}
    function getImageChannelRange(int $channel) : array {}
    function getImageChannelStatistics() : array {}
    function getImageClipMask() : Imagick {}
    function getImageColormapColor(int $index) : ImagickPixel {}
    function getImageColors() : int {}
    function getImageColorspace() : int {}
    function getImageCompose() : int {}
    function getImageCompression() : int {}
    function getImageCompressionQuality() : int {}
    function getImageDelay() : int {}
    function getImageDepth() : int {}
    function getImageDispose() : int {}
    function getImageDistortion(MagickWand $reference, int $metric) : float {}
    function getImageExtrema() : array {}
    function getImageFilename() : string {}
    function getImageFormat() : string {}
    function getImageGamma() : float {}
    function getImageGeometry() : array {}
    function getImageGravity() : int {}
    function getImageGreenPrimary() : array {}
    function getImageHeight() : int {}
    function getImageHistogram() : array {}
    function getImageIndex() : int {}
    function getImageInterlaceScheme() : int {}
    function getImageInterpolateMethod() : int {}
    function getImageIterations() : int {}
    function getImageLength() : int {}
    function getImageMagickLicense() : string {}
    function getImageMatte() : bool {}
    function getImageMatteColor() : ImagickPixel {}
    function getImageMimeType() : string {}
    function getImageOrientation() : int {}
    function getImagePage() : array {}
    function getImagePixelColor(int $x, int $y) : ImagickPixel {}
    function getImageProfile(string $name) : string {}
    function getImageProfiles(string $pattern = "*", bool $include_values = true) : array {}
    function getImageProperties(string $pattern = "*", bool $include_values = true) : array {}
    function getImageProperty(string $name) : string {}
    function getImageRedPrimary() : array {}
    function getImageRegion(int $width, int $height, int $x, int $y) : Imagick {}
    function getImageRenderingIntent() : int {}
    function getImageResolution() : array {}
    function getImagesBlob() : string {}
    function getImageScene() : int {}
    function getImageSignature() : string {}
    function getImageSize() : int {}
    function getImageTicksPerSecond() : int {}
    function getImageTotalInkDensity() : float {}
    function getImageType() : int {}
    function getImageUnits() : int {}
    function getImageVirtualPixelMethod() : int {}
    function getImageWhitePoint() : array {}
    function getImageWidth() : int {}
    function getInterlaceScheme() : int {}
    function getIteratorIndex() : int {}
    function getNumberImages() : int {}
    function getOption(string $key) : string {}
    function getPackageName() : string {}
    function getPage() : array {}
    function getPixelIterator() : ImagickPixelIterator {}
    function getPixelRegionIterator(int $x, int $y, int $columns, int $rows) : ImagickPixelIterator {}
    function getPointSize() : float {}
    function getQuantum() : int {}
    function getQuantumDepth() : array {}
    function getQuantumRange() : array {}
    function getRegistry(string $key) : string {}
    function getReleaseDate() : string {}
    function getResource(int $type) : int {}
    function getResourceLimit(int $type) : int {}
    function getSamplingFactors() : array {}
    function getSize() : array {}
    function getSizeOffset() : int {}
    function getVersion() : array {}
    function haldClutImage(Imagick $clut, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function hasNextImage() : bool {}
    function hasPreviousImage() : bool {}
    function identifyFormat(string $embedText) {}
    function identifyImage(bool $appendRawOutput = false) : array {}
    function implodeImage(float $radius) : bool {}
    function importImagePixels(int $x, int $y, int $width, int $height, string $map, int $storage, array $pixels) : bool {}
    function inverseFourierTransformImage(string $complement, string $magnitude) : void {}
    function labelImage(string $label) : bool {}
    function levelImage(float $blackPoint, float $gamma, float $whitePoint, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function linearStretchImage(float $blackPoint, float $whitePoint) : bool {}
    function liquidRescaleImage(int $width, int $height, float $delta_x, float $rigidity) : bool {}
    function listRegistry() : array {}
    function magnifyImage() : bool {}
    function mapImage(Imagick $map, bool $dither) : bool {}
    function matteFloodfillImage(float $alpha, float $fuzz, $bordercolor, int $x, int $y) : bool {}
    function medianFilterImage(float $radius) : bool {}
    function mergeImageLayers(int $layer_method) : Imagick {}
    function minifyImage() : bool {}
    function modulateImage(float $brightness, float $saturation, float $hue) : bool {}
    function montageImage(ImagickDraw $draw, string $tile_geometry, string $thumbnail_geometry, int $mode, string $frame) : Imagick {}
    function morphImages(int $number_frames) : Imagick {}
    function morphology(int $morphologyMethod, int $iterations, ImagickKernel $ImagickKernel, string $CHANNEL = '') : void {}
    function mosaicImages() : Imagick {}
    function motionBlurImage(float $radius, float $sigma, float $angle, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function negateImage(bool $gray, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function newImage(int $cols, int $rows, $background, string $format = '') : bool {}
    function newPseudoImage(int $columns, int $rows, string $pseudoString) : bool {}
    function nextImage() : bool {}
    function normalizeImage(int $channel = Imagick::CHANNEL_ALL) : bool {}
    function oilPaintImage(float $radius) : bool {}
    function opaquePaintImage($target, $fill, float $fuzz, bool $invert, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function optimizeImageLayers() : bool {}
    function orderedPosterizeImage(string $threshold_map, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function paintFloodfillImage($fill, float $fuzz, $bordercolor, int $x, int $y, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function paintOpaqueImage($target, $fill, float $fuzz, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function paintTransparentImage($target, float $alpha, float $fuzz) : bool {}
    function pingImage(string $filename) : bool {}
    function pingImageBlob(string $image) : bool {}
    function pingImageFile(resource $filehandle, string $fileName = '') : bool {}
    function polaroidImage(ImagickDraw $properties, float $angle) : bool {}
    function posterizeImage(int $levels, bool $dither) : bool {}
    function previewImages(int $preview) : bool {}
    function previousImage() : bool {}
    function profileImage(string $name, string $profile) : bool {}
    function quantizeImage(int $numberColors, int $colorspace, int $treedepth, bool $dither, bool $measureError) : bool {}
    function quantizeImages(int $numberColors, int $colorspace, int $treedepth, bool $dither, bool $measureError) : bool {}
    function queryFontMetrics(ImagickDraw $properties, string $text, bool $multiline = false) : array {}
    function queryFonts(string $pattern = "*") : array {}
    function queryFormats(string $pattern = "*") : array {}
    function radialBlurImage(float $angle, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function raiseImage(int $width, int $height, int $x, int $y, bool $raise) : bool {}
    function randomThresholdImage(float $low, float $high, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function readImage(string $filename) : bool {}
    function readImageBlob(string $image, string $filename = '') : bool {}
    function readImageFile(resource $filehandle, string $fileName = null) : bool {}
    function readImages(string $filenames) : Imagick {}
    function recolorImage(array $matrix) : bool {}
    function reduceNoiseImage(float $radius) : bool {}
    function remapImage(Imagick $replacement, int $DITHER) : bool {}
    function removeImage() : bool {}
    function removeImageProfile(string $name) : string {}
    function render() : bool {}
    function resampleImage(float $x_resolution, float $y_resolution, int $filter, float $blur) : bool {}
    function resetImagePage(string $page) : bool {}
    function resizeImage(int $columns, int $rows, int $filter, float $blur, bool $bestfit = false) : bool {}
    function rollImage(int $x, int $y) : bool {}
    function rotateImage($background, float $degrees) : bool {}
    function rotationalBlurImage(string $angle, string $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function roundCorners(float $x_rounding, float $y_rounding, float $stroke_width = 10, float $displace = 5, float $size_correction = -6) : bool {}
    function sampleImage(int $columns, int $rows) : bool {}
    function scaleImage(int $cols, int $rows, bool $bestfit = false) : bool {}
    function segmentImage(int $COLORSPACE, float $cluster_threshold, float $smooth_threshold, bool $verbose = false) : bool {}
    function selectiveBlurImage(float $radius, float $sigma, float $threshold, int $CHANNEL) : void {}
    function separateImageChannel(int $channel) : bool {}
    function sepiaToneImage(float $threshold) : bool {}
    function setBackgroundColor($background) : bool {}
    function setColorspace(int $COLORSPACE) : bool {}
    function setCompression(int $compression) : bool {}
    function setCompressionQuality(int $quality) : bool {}
    function setFilename(string $filename) : bool {}
    function setFirstIterator() : bool {}
    function setFont(string $font) : bool {}
    function setFormat(string $format) : bool {}
    function setGravity(int $gravity) : bool {}
    function setImage(Imagick $replace) : bool {}
    function setImageAlphaChannel(int $mode) : bool {}
    function setImageArtifact(string $artifact, string $value) : bool {}
    function setImageAttribute(string $key, string $value) : void {}
    function setImageBackgroundColor($background) : bool {}
    function setImageBias(float $bias) : bool {}
    function setImageBiasQuantum(string $bias) : void {}
    function setImageBluePrimary(float $x, float $y) : bool {}
    function setImageBorderColor($border) : bool {}
    function setImageChannelDepth(int $channel, int $depth) : bool {}
    function setImageClipMask(Imagick $clip_mask) : bool {}
    function setImageColormapColor(int $index, ImagickPixel $color) : bool {}
    function setImageColorspace(int $colorspace) : bool {}
    function setImageCompose(int $compose) : bool {}
    function setImageCompression(int $compression) : bool {}
    function setImageCompressionQuality(int $quality) : bool {}
    function setImageDelay(int $delay) : bool {}
    function setImageDepth(int $depth) : bool {}
    function setImageDispose(int $dispose) : bool {}
    function setImageExtent(int $columns, int $rows) : bool {}
    function setImageFilename(string $filename) : bool {}
    function setImageFormat(string $format) : bool {}
    function setImageGamma(float $gamma) : bool {}
    function setImageGravity(int $gravity) : bool {}
    function setImageGreenPrimary(float $x, float $y) : bool {}
    function setImageIndex(int $index) : bool {}
    function setImageInterlaceScheme(int $interlace_scheme) : bool {}
    function setImageInterpolateMethod(int $method) : bool {}
    function setImageIterations(int $iterations) : bool {}
    function setImageMatte(bool $matte) : bool {}
    function setImageMatteColor($matte) : bool {}
    function setImageOpacity(float $opacity) : bool {}
    function setImageOrientation(int $orientation) : bool {}
    function setImagePage(int $width, int $height, int $x, int $y) : bool {}
    function setImageProfile(string $name, string $profile) : bool {}
    function setImageProperty(string $name, string $value) : bool {}
    function setImageRedPrimary(float $x, float $y) : bool {}
    function setImageRenderingIntent(int $rendering_intent) : bool {}
    function setImageResolution(float $x_resolution, float $y_resolution) : bool {}
    function setImageScene(int $scene) : bool {}
    function setImageTicksPerSecond(int $ticks_per_second) : bool {}
    function setImageType(int $image_type) : bool {}
    function setImageUnits(int $units) : bool {}
    function setImageVirtualPixelMethod(int $method) : bool {}
    function setImageWhitePoint(float $x, float $y) : bool {}
    function setInterlaceScheme(int $interlace_scheme) : bool {}
    function setIteratorIndex(int $index) : bool {}
    function setLastIterator() : bool {}
    function setOption(string $key, string $value) : bool {}
    function setPage(int $width, int $height, int $x, int $y) : bool {}
    function setPointSize(float $point_size) : bool {}
    function setProgressMonitor(callable $callback) : void {}
    function setRegistry(string $key, string $value) : void {}
    function setResolution(float $x_resolution, float $y_resolution) : bool {}
    function setResourceLimit(int $type, int $limit) : bool {}
    function setSamplingFactors(array $factors) : bool {}
    function setSize(int $columns, int $rows) : bool {}
    function setSizeOffset(int $columns, int $rows, int $offset) : bool {}
    function setType(int $image_type) : bool {}
    function shadeImage(bool $gray, float $azimuth, float $elevation) : bool {}
    function shadowImage(float $opacity, float $sigma, int $x, int $y) : bool {}
    function sharpenImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function shaveImage(int $columns, int $rows) : bool {}
    function shearImage($background, float $x_shear, float $y_shear) : bool {}
    function sigmoidalContrastImage(bool $sharpen, float $alpha, float $beta, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function sketchImage(float $radius, float $sigma, float $angle) : bool {}
    function smushImages(string $stack, string $offset) : Imagick {}
    function solarizeImage(int $threshold) : bool {}
    function sparseColorImage(int $SPARSE_METHOD, array $arguments, int $channel = Imagick::CHANNEL_DEFAULT) : bool {}
    function spliceImage(int $width, int $height, int $x, int $y) : bool {}
    function spreadImage(float $radius) : bool {}
    function statisticImage(int $type, int $width, int $height, string $CHANNEL = Imagick::CHANNEL_DEFAULT) : void {}
    function steganoImage(Imagick $watermark_wand, int $offset) : Imagick {}
    function stereoImage(Imagick $offset_wand) : bool {}
    function stripImage() : bool {}
    function subImageMatch(Imagick $Imagick, array &$offset = null, float &$similarity = null) : Imagick {}
    function swirlImage(float $degrees) : bool {}
    function textureImage(Imagick $texture_wand) : Imagick {}
    function thresholdImage(float $threshold, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function thumbnailImage(int $columns, int $rows, bool $bestfit = false, bool $fill = false) : bool {}
    function tintImage($tint, $opacity) : bool {}
    function __toString() : string {}
    function transformImage(string $crop, string $geometry) : Imagick {}
    function transformImageColorspace(int $colorspace) : bool {}
    function transparentPaintImage($target, float $alpha, float $fuzz, bool $invert) : bool {}
    function transposeImage() : bool {}
    function transverseImage() : bool {}
    function trimImage(float $fuzz) : bool {}
    function uniqueImageColors() : bool {}
    function unsharpMaskImage(float $radius, float $sigma, float $amount, float $threshold, int $channel = Imagick::CHANNEL_ALL) : bool {}
    function valid() : bool {}
    function vignetteImage(float $blackPoint, float $whitePoint, int $x, int $y) : bool {}
    function waveImage(float $amplitude, float $length) : bool {}
    function whiteThresholdImage($threshold) : bool {}
    function writeImage(string $filename = null) : bool {}
    function writeImageFile(resource $filehandle) : bool {}
    function writeImages(string $filename, bool $adjoin) : bool {}
    function writeImagesFile(resource $filehandle) : bool {}
}

class ImagickDraw
{
    function affine(array $affine) : bool {}
    function annotation(float $x, float $y, string $text) : bool {}
    function arc(float $sx, float $sy, float $ex, float $ey, float $sd, float $ed) : bool {}
    function bezier(array $coordinates) : bool {}
    function circle(float $ox, float $oy, float $px, float $py) : bool {}
    function clear() : bool {}
    function clone() : ImagickDraw {}
    function color(float $x, float $y, int $paintMethod) : bool {}
    function comment(string $comment) : bool {}
    function composite(int $compose, float $x, float $y, float $width, float $height, Imagick $compositeWand) : bool {}
    function __construct() {}
    function destroy() : bool {}
    function ellipse(float $ox, float $oy, float $rx, float $ry, float $start, float $end) : bool {}
    function getClipPath() : string {}
    function getClipRule() : int {}
    function getClipUnits() : int {}
    function getFillColor() : ImagickPixel {}
    function getFillOpacity() : float {}
    function getFillRule() : int {}
    function getFont() : string {}
    function getFontFamily() : string {}
    function getFontSize() : float {}
    function getFontStretch() : int {}
    function getFontStyle() : int {}
    function getFontWeight() : int {}
    function getGravity() : int {}
    function getStrokeAntialias() : bool {}
    function getStrokeColor() : ImagickPixel {}
    function getStrokeDashArray() : array {}
    function getStrokeDashOffset() : float {}
    function getStrokeLineCap() : int {}
    function getStrokeLineJoin() : int {}
    function getStrokeMiterLimit() : int {}
    function getStrokeOpacity() : float {}
    function getStrokeWidth() : float {}
    function getTextAlignment() : int {}
    function getTextAntialias() : bool {}
    function getTextDecoration() : int {}
    function getTextEncoding() : string {}
    function getTextInterlineSpacing() : float {}
    function getTextInterwordSpacing() : float {}
    function getTextKerning() : float {}
    function getTextUnderColor() : ImagickPixel {}
    function getVectorGraphics() : string {}
    function line(float $sx, float $sy, float $ex, float $ey) : bool {}
    function matte(float $x, float $y, int $paintMethod) : bool {}
    function pathClose() : bool {}
    function pathCurveToAbsolute(float $x1, float $y1, float $x2, float $y2, float $x, float $y) : bool {}
    function pathCurveToQuadraticBezierAbsolute(float $x1, float $y1, float $x, float $y) : bool {}
    function pathCurveToQuadraticBezierRelative(float $x1, float $y1, float $x, float $y) : bool {}
    function pathCurveToQuadraticBezierSmoothAbsolute(float $x, float $y) : bool {}
    function pathCurveToQuadraticBezierSmoothRelative(float $x, float $y) : bool {}
    function pathCurveToRelative(float $x1, float $y1, float $x2, float $y2, float $x, float $y) : bool {}
    function pathCurveToSmoothAbsolute(float $x2, float $y2, float $x, float $y) : bool {}
    function pathCurveToSmoothRelative(float $x2, float $y2, float $x, float $y) : bool {}
    function pathEllipticArcAbsolute(float $rx, float $ry, float $x_axis_rotation, bool $large_arc_flag, bool $sweep_flag, float $x, float $y) : bool {}
    function pathEllipticArcRelative(float $rx, float $ry, float $x_axis_rotation, bool $large_arc_flag, bool $sweep_flag, float $x, float $y) : bool {}
    function pathFinish() : bool {}
    function pathLineToAbsolute(float $x, float $y) : bool {}
    function pathLineToHorizontalAbsolute(float $x) : bool {}
    function pathLineToHorizontalRelative(float $x) : bool {}
    function pathLineToRelative(float $x, float $y) : bool {}
    function pathLineToVerticalAbsolute(float $y) : bool {}
    function pathLineToVerticalRelative(float $y) : bool {}
    function pathMoveToAbsolute(float $x, float $y) : bool {}
    function pathMoveToRelative(float $x, float $y) : bool {}
    function pathStart() : bool {}
    function point(float $x, float $y) : bool {}
    function polygon(array $coordinates) : bool {}
    function polyline(array $coordinates) : bool {}
    function pop() : bool {}
    function popClipPath() : bool {}
    function popDefs() : bool {}
    function popPattern() : bool {}
    function push() : bool {}
    function pushClipPath(string $clip_mask_id) : bool {}
    function pushDefs() : bool {}
    function pushPattern(string $pattern_id, float $x, float $y, float $width, float $height) : bool {}
    function rectangle(float $x1, float $y1, float $x2, float $y2) : bool {}
    function render() : bool {}
    function resetVectorGraphics() : void {}
    function rotate(float $degrees) : bool {}
    function roundRectangle(float $x1, float $y1, float $x2, float $y2, float $rx, float $ry) : bool {}
    function scale(float $x, float $y) : bool {}
    function setClipPath(string $clip_mask) : bool {}
    function setClipRule(int $fill_rule) : bool {}
    function setClipUnits(int $clip_units) : bool {}
    function setFillAlpha(float $opacity) : bool {}
    function setFillColor(ImagickPixel $fill_pixel) : bool {}
    function setFillOpacity(float $fillOpacity) : bool {}
    function setFillPatternURL(string $fill_url) : bool {}
    function setFillRule(int $fill_rule) : bool {}
    function setFont(string $font_name) : bool {}
    function setFontFamily(string $font_family) : bool {}
    function setFontSize(float $pointsize) : bool {}
    function setFontStretch(int $fontStretch) : bool {}
    function setFontStyle(int $style) : bool {}
    function setFontWeight(int $font_weight) : bool {}
    function setGravity(int $gravity) : bool {}
    function setResolution(string $x_resolution, string $y_resolution) : void {}
    function setStrokeAlpha(float $opacity) : bool {}
    function setStrokeAntialias(bool $stroke_antialias) : bool {}
    function setStrokeColor(ImagickPixel $stroke_pixel) : bool {}
    function setStrokeDashArray(array $dashArray) : bool {}
    function setStrokeDashOffset(float $dash_offset) : bool {}
    function setStrokeLineCap(int $linecap) : bool {}
    function setStrokeLineJoin(int $linejoin) : bool {}
    function setStrokeMiterLimit(int $miterlimit) : bool {}
    function setStrokeOpacity(float $stroke_opacity) : bool {}
    function setStrokePatternURL(string $stroke_url) : bool {}
    function setStrokeWidth(float $stroke_width) : bool {}
    function setTextAlignment(int $alignment) : bool {}
    function setTextAntialias(bool $antiAlias) : bool {}
    function setTextDecoration(int $decoration) : bool {}
    function setTextEncoding(string $encoding) : bool {}
    function setTextInterlineSpacing(float $spacing) : void {}
    function setTextInterwordSpacing(float $spacing) : void {}
    function setTextKerning(float $kerning) : void {}
    function setTextUnderColor(ImagickPixel $under_color) : bool {}
    function setVectorGraphics(string $xml) : bool {}
    function setViewbox(int $x1, int $y1, int $x2, int $y2) : bool {}
    function skewX(float $degrees) : bool {}
    function skewY(float $degrees) : bool {}
    function translate(float $x, float $y) : bool {}
}

class ImagickDrawException
{
}

class ImagickException
{
}

class ImagickKernel
{
    function addKernel(ImagickKernel $ImagickKernel) : void {}
    function addUnityKernel() : void {}
    function fromBuiltin(string $kernelType, string $kernelString) : ImagickKernel {}
    function fromMatrix(array $matrix, array $origin = null) : ImagickKernel {}
    function getMatrix() : array {}
    function scale() : void {}
    function separate() : array {}
}

class ImagickKernelException
{
}

class ImagickPixel
{
    function clear() : bool {}
    function __construct(string $color = '') {}
    function destroy() : bool {}
    function getColor(bool $normalized = false) : array {}
    function getColorAsString() : string {}
    function getColorCount() : int {}
    function getColorQuantum() {}
    function getColorValue(int $color) : float {}
    function getColorValueQuantum() {}
    function getHSL() : array {}
    function getIndex() : int {}
    function isPixelSimilar(ImagickPixel $color, float $fuzz) : bool {}
    function isPixelSimilarQuantum(string $color, string $fuzz = '') : bool {}
    function isSimilar(ImagickPixel $color, float $fuzz) : bool {}
    function setColor(string $color) : bool {}
    function setcolorcount(string $colorCount) : void {}
    function setColorValue(int $color, float $value) : bool {}
    function setColorValueQuantum(int $color, $value) : void {}
    function setHSL(float $hue, float $saturation, float $luminosity) : bool {}
    function setIndex(int $index) : void {}
}

class ImagickPixelException
{
}

class ImagickPixelIterator
{
    function clear() : bool {}
    function __construct(Imagick $wand) {}
    function destroy() : bool {}
    function getCurrentIteratorRow() : array {}
    function getIteratorRow() : int {}
    function getNextIteratorRow() : array {}
    function getPreviousIteratorRow() : array {}
    function newPixelIterator(Imagick $wand) : bool {}
    function newPixelRegionIterator(Imagick $wand, int $x, int $y, int $columns, int $rows) : bool {}
    function resetIterator() : bool {}
    function setIteratorFirstRow() : bool {}
    function setIteratorLastRow() : bool {}
    function setIteratorRow(int $row) : bool {}
    function syncIterator() : bool {}
}

class ImagickPixelIteratorException
{
}
