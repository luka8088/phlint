<?php

function apc_add(array $values, $unused = null, int $ttl = 0) : array {}
function apc_bin_dump(array $files = null, array $user_vars = null) : string {}
function apc_bin_dumpfile(array $files, array $user_vars, string $filename, int $flags = 0, resource $context = null) : int {}
function apc_bin_load(string $data, int $flags = 0) : bool {}
function apc_bin_loadfile(string $filename, resource $context = null, int $flags = 0) : bool {}
function apc_cache_info(string $cache_type = "", bool $limited = false) : array {}
function apc_cas(string $key, int $old, int $new) : bool {}
function apc_clear_cache(string $cache_type = "") : bool {}
function apc_compile_file(string $filename, bool $atomic = true) {}
function apc_dec(string $key, int $step = 1, bool &$success = false) : int {}
function apc_define_constants(string $key, array $constants, bool $case_sensitive = true) : bool {}
function apc_delete(string $key) {}
function apc_delete_file($keys) {}
function apc_exists($keys) {}
function apc_fetch($key, bool &$success = false) {}
function apc_inc(string $key, int $step = 1, bool &$success = false) : int {}
function apc_load_constants(string $key, bool $case_sensitive = true) : bool {}
function apc_sma_info(bool $limited = false) : array {}
function apc_store(array $values, $unused = null, int $ttl = 0) : array {}

class APCIterator implements Iterator
{
    function __construct(string $cache, $search = null, int $format = APC_ITER_ALL, int $chunk_size = 100, int $list = APC_LIST_ACTIVE) {}
    function current() {}
    function getTotalCount() : int {}
    function getTotalHits() : int {}
    function getTotalSize() : int {}
    function key() : string {}
    function next() : void {}
    function rewind() : void {}
    function valid() : void {}
}
