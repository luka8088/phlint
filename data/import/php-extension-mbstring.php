<?php

const MB_CASE_LOWER = 1;
const MB_CASE_TITLE = 2;
const MB_CASE_UPPER = 0;
const MB_OVERLOAD_MAIL = 1;
const MB_OVERLOAD_REGEX = 4;
const MB_OVERLOAD_STRING = 2;

function mbereg($pattern, $string, $registers) {}
function mbereg_match($pattern, $string, $option) {}
function mbereg_replace($pattern, $replacement, $string, $option) {}
function mbereg_search($pattern, $option) {}
function mbereg_search_getpos() {}
function mbereg_search_getregs() {}
function mbereg_search_init($string, $pattern, $option) {}
function mbereg_search_pos($pattern, $option) {}
function mbereg_search_regs($pattern, $option) {}
function mbereg_search_setpos($position) {}
function mberegi($pattern, $string, $registers) {}
function mberegi_replace($pattern, $replacement, $string) {}
function mbregex_encoding($encoding) {}
function mbsplit($pattern, $string, $limit) {}
