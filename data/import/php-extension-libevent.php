<?php

function event_add(resource $event, int $timeout = -1) : bool {}
function event_base_free(resource $event_base) {}
function event_base_loop(resource $event_base, int $flags = 0) : int {}
function event_base_loopbreak(resource $event_base) : bool {}
function event_base_loopexit(resource $event_base, int $timeout = -1) : bool {}
function event_base_new() : resource {}
function event_base_priority_init(resource $event_base, int $npriorities) : bool {}
function event_base_reinit(resource $event_base) : bool {}
function event_base_set(resource $event, resource $event_base) : bool {}
function event_buffer_base_set(resource $bevent, resource $event_base) : bool {}
function event_buffer_disable(resource $bevent, int $events) : bool {}
function event_buffer_enable(resource $bevent, int $events) : bool {}
function event_buffer_fd_set(resource $bevent, resource $fd) {}
function event_buffer_free(resource $bevent) {}
function event_buffer_new(resource $stream, $readcb, $writecb, $errorcb, $arg = null) : resource {}
function event_buffer_priority_set(resource $bevent, int $priority) : bool {}
function event_buffer_read(resource $bevent, int $data_size) : string {}
function event_buffer_set_callback(resource $event, $readcb, $writecb, $errorcb, $arg = null) : bool {}
function event_buffer_timeout_set(resource $bevent, int $read_timeout, int $write_timeout) {}
function event_buffer_watermark_set(resource $bevent, int $events, int $lowmark, int $highmark) {}
function event_buffer_write(resource $bevent, string $data, int $data_size = -1) : bool {}
function event_del(resource $event) : bool {}
function event_free(resource $event) {}
function event_new() : resource {}
function event_priority_set(resource $event, int $priority) : bool {}
function event_set(resource $event, $fd, int $events, $callback, $arg = null) : bool {}
function event_timer_set(resource $event, callable $callback, $arg = null) : bool {}
