<?php

namespace luka8088\phlint;

use \phlint\Issue;

class Result {

  protected $issues = [];

  public $libraryIssues = [];

  function addIssue ($node, $message, $causes = []) {

    $originalMessage = $message;

    if ($node && $node->getAttribute('isTrusted', false))
      return;

    if ($node && !$node->getAttribute('isReachable', true))
      return;

    $message = Result::expandMessageMeta($message, $node);

    foreach ($causes as $offset => $causeMessage)
      $message .= "\n  " . 'Cause #' . ($offset + 1) . ': ' .
        str_replace("\n", "\n  ", self::printMessage($causeMessage));

    #if ($node)
    #  var_dump($node->getAttribute('isSpecialization', false));

    if (in_array($message, $this->issues))
      return;

    if (in_array($message, $this->libraryIssues))
      return;

    if ($node) {
      $traces = $this->generateTraces($node);
      #var_dump($traces);

      if (count($traces) > 0) {
        $isNew = false;
        foreach ($traces as $trace) {
          $msg = $message;
          foreach (array_reverse($trace) as $offset => $traceMessage)
            $msg .= "\n  " . 'Trace #' . ($offset + 1) . ': ' . Result::expandMessageMeta($traceMessage['message'], $traceMessage['node']);

          if (in_array($msg, $this->issues))
            return;
          $isNew = true;
          $this->issues[] = $msg;
          context('output')->__invoke($msg . "\n");
        }
        if ($isNew)
          $this->extensionInterface->analysisIssueFound(new Issue($originalMessage, $node, $traces, $causes));
        return;
      }
    }

    $this->issues[] = $message;
    $this->extensionInterface->analysisIssueFound(new Issue($originalMessage, $node, [], $causes));
    context('output')->__invoke($message . "\n");
  }

  function getIssues () {
    return $this->issues;
  }

  public $traceStack = [];

  function pushTrace ($node, $message) {
    $this->traceStack[] = Result::expandMessageMeta($message, $node);
  }

  function popTrace () {
    array_pop($this->traceStack);
  }

  function toString () {
    return implode("\n", array_map(function ($entry) {
          return $entry;
      }, array_slice(array_unique($this->getIssues()), 0, 100))) .
      "\n" . (count(array_unique($this->getIssues())) > 100 ? '...' : '') .
      "\n(" . count(array_unique($this->getIssues())) . ' issue(s) found)';
  }

  static function mergeMessages ($messages) {
    $map = [];
    foreach ($messages as $message) {
      if (!isset($map[$message['message']]))
        $map[$message['message']] = $message;
      if (isset($message['causes']))
        $map[$message['message']]['causes'] = self::mergeMessages(array_merge(
          isset($map[$message['message']]['cause']) ? $map[$message['message']]['cause'] : [],
          $message['causes']
        ));
    }
    return array_values($map);
  }

  static function printMessage ($message) {
    if (is_string($message))
      return $message;
    $printed = $message['message'];
    foreach ($message['causes'] as $offset => $causeMessage)
      $printed .= "\n  " . 'Cause #' . ($offset + 1) . ': ' .
        str_replace("\n", "\n  ", self::printMessage($causeMessage));
    return $printed;
  }

  static function expandMessageMeta ($message, $node) {
    if (!is_object($node))
      return $message;

    if (isset($node->getAttributes()['path'])) {
      $path = $node->getAttributes()['path'];
      if (realpath($path))
        $path = realpath($path);
      return rtrim($message, '.') . ' in *' . $path . ($node->getLine() > 0 ? ':' . $node->getLine() : '') . '*.';
    }

    if ($node->getLine() > 0)
      return rtrim($message, '.') . ' on line ' . $node->getLine() . '.';

    return $message;
  }

  function generateTraces ($node, $participatingInvocationNodes = []) {

    #var_dump(NodeConcept::sourcePrint($node));

    #var_dump($node);

    $traces = [];

    $definitionNodes = [];
    $contextSymbol = inference\Scope::contextScope($node->getAttribute('scope', []));
    $contextSymbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $contextSymbol);

    #var_dump($contextSymbol);
    #var_dump(count(context('code')->symbols[$contextSymbol]['definitionNodes']));

    if (isset(context('code')->symbols[$contextSymbol])) {
      foreach (context('code')->symbols[$contextSymbol]['definitionNodes'] as $potentialDefinitionNode) {
        #var_dump($potentialDefinitionNode->getAttribute('types', []));
        NodeTraverser::traverse([$potentialDefinitionNode], [function ($existingNode) use (&$definitionNodes, $node, $potentialDefinitionNode) {
          if ($existingNode === $node)
            $definitionNodes[] = $potentialDefinitionNode;
        }]);
      }
    }

    #if (count($definitionNodes) == 0)
    #  return [];

    #var_dump(count($definitionNodes));

    foreach ($definitionNodes as $definitionNode) {

    #var_dump('................', $definitionNode->getAttribute('types', []));

    $isRecursion = false;
    foreach ($participatingInvocationNodes as $participatingDefinitionNode)
      if ($participatingDefinitionNode === $definitionNode)
        $isRecursion = true;


    if (!$isRecursion) {

      #$definitionNodeTraces = $this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode]));

      #var_dump($definitionNodeTraces);

      #var_dump($this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode])));

      #var_dump($definitionNode->getAttribute('types', []));

      #foreach ($definitionNode->getAttribute('types', []) as $definitionType) {
      #  $definitionContextSymbol = inference\Scope::contextScope(inference\Scope::parent($definitionType));
      #  $definitionContextSymbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $definitionContextSymbol);
      #
      #  var_dump($definitionContextSymbol);
      #  var_dump(isset(context('code')->symbols[$definitionContextSymbol]));
      #  if (isset(context('code')->symbols[$definitionContextSymbol]['definitionNodes']))
      #    foreach (context('code')->symbols[$definitionContextSymbol]['definitionNodes'] as $definitionContextNode) {
      #      var_dump($this->generateTraces($definitionContextNode, array_merge($participatingInvocationNodes, [$definitionNode])));
      #    }
      #
      #  #var_dump($definitionContextSymbol);
      #
      #}

      #var_dump($definitionNode->getAttribute('types', []));
      #var_dump($contextSymbol);
      #var_dump($definitionNode);

      foreach ($definitionNode->getAttribute('symbols', []) as $symbol) {

        $symbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $symbol);

        #var_dump($symbol);
        #var_dump(count(context('code')->symbols[$symbol]['linkedNodes']));

        #var_dump($definitionNode);

        #var_dump($symbol);

        #var_dump($definitionNode->getAttribute('types', []));

        #if (false) {

        $definitionContextTraces = [];

        $definitionContextSymbol = inference\Scope::contextScope(inference\Scope::parent($symbol));
        $definitionContextSymbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $definitionContextSymbol);

        #var_dump($symbol);
        #var_dump(count(context('code')->symbols[$symbol]['definitionNodes']));

        #foreach (context('code')->symbols[$symbol]['definitionNodes'] as $x)
        #  var_dump($x);

        #var_dump('------------------------------------------------');
        #var_dump('------------------------------------------------');
        #var_dump('------------------------------------------------');

        #$message = '';

        $contextes = [];


        if (false)
        if (isset(context('code')->symbols[$definitionContextSymbol]))
          foreach (context('code')->symbols[$definitionContextSymbol]['definitionNodes'] as $definitionContextNode) {

            #var_dump("A");

            #var_dump(array_map(function ($type) { return $type; }, $definitionContextNode->getAttribute('types', [])));
            #var_dump($definitionNode->getAttribute('types', []));


            // @todo: Rewrite
            if (array_map(function ($type) { return inference\Scope::parent($type); }, $definitionNode->getAttribute('types', [])) != $definitionContextNode->getAttribute('types', []))
              continue;

            #var_dump($definitionContextNode->getAttribute('types', []));
            #var_dump($definitionContextNode->params[0]->getAttribute('types', []));
            $contextes[] = $definitionContextNode;

            #$contextes[] = $definitionContextNode;

            if (isset(context('code')->symbols[$definitionContextSymbol]['linkedNodes']))
              foreach (context('code')->symbols[$definitionContextSymbol]['linkedNodes'] as $linkedNode) {

                // @todo: Rewrite
                if ($linkedNode->getAttribute('invocationTypes', []) != $definitionContextNode->getAttribute('types', []))
                  continue;

                $contextes[] = $definitionContextNode;

                #var_dump(ucfirst(NodeConcept::referencePrint($definitionContextNode)) . ' specialized for the ' . NodeConcept::referencePrint($linkedNode));

                #var_dump($this->generateTraces($linkedNode, array_merge($participatingInvocationNodes, [$definitionContextNode])));

                #$message = $message . "\n" . ucfirst(NodeConcept::referencePrint($definitionNode)) .
                #  ' specialized for the ' . NodeConcept::referencePrint($linkedNode) .
                #  'in the context of ' . NodeConcept::referencePrint($definitionContextNode) . '.';

                #$message = ucfirst(NodeConcept::referencePrint($definitionNode)) .
                #  ' specialized in the context of ' . NodeConcept::referencePrint($definitionContextNode);

                #var_dump("A");

              }


            #echo $message . "\n";

            #var_dump($this->generateTraces($definitionContextNode, array_merge($participatingInvocationNodes, [$definitionContextNode])));


              #foreach ($definitionContextNode->getAttribute('symbols', []) as $contextSymbol2)
              #if (isset(context('code')->symbols[$contextSymbol2]['linkedNodes']))
              #foreach (context('code')->symbols[$contextSymbol2]['linkedNodes'] as $linkedNode) {
              #  #var_dump($contextSymbol2);
              #  #var_dump(NodeConcept::sourcePrint($linkedNode));
              #  $message .= "\n" . ucfirst(NodeConcept::referencePrint($definitionContextNode)) .
              #' specialized for the ' . NodeConcept::referencePrint($linkedNode) . '.' . ($isRecursion ? 'recursion detected!' : '');
              #
              #  #var_dump($this->generateTraces($linkedNode, array_merge($participatingInvocationNodes, [$definitionContextNode])));
              #}


            #var_dump($definitionNode->getAttribute('types', []));
            #var_dump($definitionContextNode->getAttribute('types', []));

            #$contextTraces = $this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode]));

            #if (count($contextTraces) > 0)
            #  var_dump($contextSymbol, $definitionNode->getAttribute('types', []));


            // @todo: Rewrite
            #if ($linkedNode->getAttribute('invocationTypes', []) != $definitionNode->getAttribute('types', []))
            #  continue;

            #var_dump("A");
          }

        #}

        #var_dump($contextTraces);

        #$contextes = [];


        if (isset(context('code')->symbols[$symbol]['linkedNodes']))
          foreach (count($contextes) > 0 ? $contextes : [null] as $definitionContextNode) {

          $hasLinks = false;
          foreach (context('code')->symbols[$symbol]['linkedNodes'] as $linkedNode) {

            #var_dump($this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode])));

            #var_dump($linkedNode->getAttribute('invocationTypes', []));
            #var_dump($definitionNode->getAttribute('types', []));

            // @todo: Rewrite
            if ($linkedNode->getAttribute('invocationTypes', []) != $definitionNode->getAttribute('types', []))
              continue;

            $hasLinks = true;

            #var_dump($this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode])));

            $linkedNodeTraces = $this->generateTraces($linkedNode, array_merge($participatingInvocationNodes, [$definitionNode]));

            #var_dump($message);

            $message = ucfirst(NodeConcept::referencePrint($definitionNode)) .
              ' specialized for the ' . NodeConcept::referencePrint($linkedNode) . ($definitionContextNode ? ' in the context of ' . NodeConcept::referencePrint($definitionContextNode) : '') . '.' . ($isRecursion ? 'recursion detected!' : '');

            foreach (count($linkedNodeTraces) > 0 ? $linkedNodeTraces : [0 => ''] as $traceIndex => $_) {
              if (!isset($linkedNodeTraces[$traceIndex]))
                $linkedNodeTraces[$traceIndex] = [];
              #foreach ($definitionNodeTraces as $definitionNodeTrace)
              #  foreach ($definitionNodeTrace as $definitionNodeTraceEntry)
              #    $linkedNodeTraces[$traceIndex][] = $definitionNodeTraceEntry;
              #var_dump($contextTraces);
              #foreach ($contextTraces as $contextTrace)
              #  foreach ($contextTrace as $contextTraceEntry)
              #    $linkedNodeTraces[$traceIndex][] = $contextTraceEntry;
              $linkedNodeTraces[$traceIndex][] = ['message' => $message, 'node' => $linkedNode];
            }

            #var_dump($contextTraces);

            #foreach (count($definitionNodeTraces) > 0 ? $definitionNodeTraces : [0 => ''] as $traceIndex => $_) {
            #  if (!isset($definitionNodeTraces[$traceIndex]))
            #    $definitionNodeTraces[$traceIndex] = [];
            #  foreach ($linkedNodeTraces as $linkedNodeTrace)
            #    foreach ($linkedNodeTrace as $linkedNodeTraceEntry)
            #      $definitionNodeTraces[$traceIndex][] = $linkedNodeTraceEntry;
            #}

            #foreach (count($contextTraces) > 0 ? $contextTraces : [0 => ''] as $traceIndex => $_) {
            #  if (!isset($contextTraces[$traceIndex]))
            #    $contextTraces[$traceIndex] = [];
            #  foreach ($linkedNodeTraces as $linkedNodeTrace)
            #    foreach ($linkedNodeTrace as $linkedNodeTraceEntry)
            #      $contextTraces[$traceIndex][] = $linkedNodeTraceEntry;
            #}

            foreach ($linkedNodeTraces as $linkedNodeTrace)
              $traces[] = $linkedNodeTrace;

          }


          if (!$hasLinks && $definitionContextNode) {

            #var_dump($definitionContextNode);

            $message = ucfirst(NodeConcept::referencePrint($definitionNode)) .
              ' specialized in the context of ' . NodeConcept::referencePrint($definitionContextNode);

            foreach (count($traces) > 0 ? $traces : [0 => ''] as $traceIndex => $_) {
              if (!isset($traces[$traceIndex]))
                $traces[$traceIndex] = [];
              $traces[$traceIndex][] = ['message' => $message, 'node' => $definitionNode];
            }

              var_dump($message);

            var_dump("AAA");
          }


          }


      }

      #foreach ($definitionNodeTraces as $definitionNodeTrace)
      #  $traces[] = $definitionNodeTrace;

      #$definitionNodeTraces = $this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode]));

      #var_dump($definitionNodeTraces);

      #if (count($definitionNodeTraces) > 0)
      #  return $definitionNodeTraces;

      #var_dump(count($definitionNodeTraces));
      #var_dump(count($traces));
      #var_dump('----------------------');

      #if (count($definitionNodeTraces) > 0)
      #  var_dump(count($traces));
      #  var_dump($definitionNodeTraces);

      #foreach ($definitionNodeTraces as $definitionNodeTrace)
      #  foreach (count($traces) > 0 ? $traces : [0 => ''] as $traceIndex => $_)
      #    foreach ($definitionNodeTrace as $a)
      #      $traces[$traceIndex][] = $a;



            #$linkedNodeTraces = $this->generateTraces($definitionNode, array_merge($participatingInvocationNodes, [$definitionNode]));

            #foreach ($linkedNodeTraces as $linkedNodeTrace)
            #  $traces[] = $linkedNodeTrace;

            #var_dump($linkedNodeTraces);

            #$message = ucfirst(NodeConcept::referencePrint($definitionNode)) .
            #  ' specialized for the ' . NodeConcept::referencePrint($definitionNode) . '.' . ($isRecursion ? 'recursion detected!' : '');

            #foreach (count($linkedNodeTraces) > 0 ? $linkedNodeTraces : [0 => ''] as $traceIndex => $_) {
            #  if (!isset($linkedNodeTraces[$traceIndex]))
            #    $linkedNodeTraces[$traceIndex] = [];
            #  $linkedNodeTraces[$traceIndex][] = ['message' => $message, 'node' => $definitionNode];
            #}

            #foreach ($linkedNodeTraces as $linkedNodeTrace)
            #  $traces[] = $linkedNodeTrace;



    }

    }

    #var_dump($traces);
    #exit;

    #if (count($traces) > 0 && count($traces[0]) > 1) {
    #  var_dump($traces);
    #  exit;
    #}

    // @todo: Remove or rewrite.
    $traces = array_slice($traces, 0, 5);

    return $traces;

  }

}
