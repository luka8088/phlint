<?php

namespace luka8088\phlint\rule;

use \luka8088\Phlint;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\Result;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;
use \PhpParser\NodeVisitorAbstract;

/**
 * Require that all symbol interactions are done through a compatible concept.
 */
class RequireCompatibleConcept extends NodeVisitorAbstract {

  function getIdentifier () {
    return 'requireCompatibleConcept';
  }

  function getCategories () {
    return [
      'default',
    ];
  }

  function getInferences () {
    return [
      'symbol',
      'templateSpecialization',
      'type',
      'value',
    ];
  }

  function visitNode ($node) {

    #if ($node->getAttribute('isCheckedConcept', false))
    #  return;

    #$node->setAttribute('isCheckedConcept', true);

    #if (count(debug_backtrace()) > 300) {
    #  echo new \Error('e');
    #  exit;
    #}

    if (NodeConcept::isInvocationNode($node)) {

      if ($node instanceof Node\Expr\FuncCall || $node instanceof Node\Expr\StaticCall) {

        // Todo: Rewrite
        if (strpos(NodeConcept::sourcePrint($node) . '::', 'parent::') !== false) {
          return;
        }
        // Todo: Rewrite
        if (strpos(NodeConcept::sourcePrint($node) . '::', 'self::') !== false) {
          return;
        }
        // Todo: Rewrite
        if (strpos(NodeConcept::sourcePrint($node) . '::', 'static::') !== false) {
          return;
        }

        // Todo: Rewrite
        if (!($node instanceof Node\Expr\StaticCall))
        if (strpos(NodeConcept::sourcePrint($node), '$') === 0)
          return;

      }

      foreach (NodeFacade::getSymbols($node) as $symbol)
        if (!isset(context('code')->symbols[$symbol]) || count(context('code')->symbols[$symbol]['definitionNodes']) == 0) {
          $symbolDisplay = isset(context('code')->symbols[$symbol]) ? context('code')->symbols[$symbol]['phpId'] : $symbol;
          $symbolDisplay = strpos($symbolDisplay, 't_') === 0 ? substr($symbolDisplay, 2) : $symbolDisplay;
          $expressionDisplay = NodeConcept::displayPrint($node);
          if ($symbolDisplay != $expressionDisplay && $symbolDisplay . '()' != ltrim($expressionDisplay, '\\'))
            context('result')->addIssue(
              $node,
              'Unable to invoke undefined *' . $symbolDisplay . '*' .
                ' for the expression *' . $expressionDisplay . '*.'
            );
          else
            context('result')->addIssue(
              $node,
              'Unable to invoke undefined *' . $expressionDisplay . '*.'
            );
        }

    }

    if ($node instanceof Node\Stmt\Foreach_) {
      foreach (array_unique(array_merge(NodeFacade::getTypes($node->expr), array_map(function ($value) { return $value['type']; }, NodeFacade::getValues($node->expr)))) as $type)
        if ($type && strpos($type, 'c_') !== 0 && $type != 't_mixed' && $type != 'array' && $type != 't_array' && substr($type, -2) != '[]')
          context('result')->addIssue($node,
            'Provided symbol *' . NodeConcept::displayPrint($node->expr) . '* ' .
            'of type *' . (isset(context('code')->symbols[$type]) ? context('code')->symbols[$type]['phpId'] : $type) . '* ' .
            'is not compatible in the expression *' . NodeConcept::displayPrint($node) . '*.'
          );

    }

  }

  function enterNode(Node $node) {

    if ($node->getAttribute('isCheckedConcept', false))
      return;

    if (($node instanceof Node\Expr\BinaryOp\Plus) || ($node instanceof Node\Expr\BinaryOp\Minus)) {

      foreach (array_unique(array_merge(NodeFacade::getTypes($node->left), array_map(function ($value) { return $value['type']; }, NodeFacade::getValues($node->left)))) as $type)
        if (!in_array($type, ['t_undefined', 't_mixed', 't_integer', 'int', 't_int', 'autoInteger', 't_autoInteger', 'autoFloat', 't_autoFloat']))
          context('result')->addIssue($node,
            'Provided ' . RequireCompatibleConcept::constructTypeName($node->left) . ' *' . NodeConcept::displayPrint($node->left) . '* ' .
            'of type *' . (isset(context('code')->symbols[$type]) ? context('code')->symbols[$type]['phpId'] : $type) . '* ' .
            'is not compatible in the expression *' . NodeConcept::displayPrint($node) . '*.'
          );

      foreach (array_unique(array_merge(NodeFacade::getTypes($node->right), array_map(function ($value) { return $value['type']; }, NodeFacade::getValues($node->right)))) as $type)
        if (!in_array($type, ['t_undefined', 't_mixed', 't_integer', 'int', 't_int', 'autoInteger', 't_autoInteger', 'autoFloat', 't_autoFloat']))
          context('result')->addIssue($node,
            'Provided ' . RequireCompatibleConcept::constructTypeName($node->right) . ' *' . NodeConcept::displayPrint($node->right) . '* ' .
            'of type *' . (isset(context('code')->symbols[$type]) ? context('code')->symbols[$type]['phpId'] : $type) . '* ' .
            'is not compatible in the expression *' . NodeConcept::displayPrint($node) . '*.'
          );

    }

    if (($node instanceof Node\Expr\AssignOp\Plus) || ($node instanceof Node\Expr\AssignOp\Minus)) {

      foreach (array_unique(array_merge(NodeFacade::getTypes($node->var), array_map(function ($value) { return $value['type']; }, NodeFacade::getValues($node->var)))) as $type)
        if (!in_array($type, ['t_undefined', 't_mixed', 't_integer', 'int', 't_int', 'autoInteger', 't_autoInteger', 'autoFloat', 't_autoFloat']))
          context('result')->addIssue($node,
            'Provided ' . self::constructTypeName($node->var) . ' *' . NodeConcept::displayPrint($node->var) . '* ' .
            'of type *' . (isset(context('code')->symbols[$type]) ? context('code')->symbols[$type]['phpId'] : $type) . '* ' .
            'is not compatible in the expression *' . NodeConcept::displayPrint($node) . '*.'
          );

      foreach (array_unique(array_merge(NodeFacade::getTypes($node->expr), array_map(function ($value) { return $value['type']; }, NodeFacade::getValues($node->expr)))) as $type)
        if (!in_array($type, ['t_undefined', 't_mixed', 't_integer', 'int', 't_int', 'autoInteger', 't_autoInteger', 'autoFloat', 't_autoFloat']))
          context('result')->addIssue($node,
            'Provided ' . self::constructTypeName($node->var) . ' *' . NodeConcept::displayPrint($node->expr) . '* ' .
            'of type *' . (isset(context('code')->symbols[$type]) ? context('code')->symbols[$type]['phpId'] : $type) . '* ' .
            'is not compatible in the expression *' . NodeConcept::displayPrint($node) . '*.'
          );

      #var_dump($node);

    }

    // PhpParser\Node\Expr\Variable
    // PhpParser\Node\Expr\BinaryOp\Plus

  }

  /** Some concepts implicitly support other concepts. */
  static $implicitMap = [
    'numeric' => 'opPlus',
    'int' => 'opPlus',
  ];

  static function constructTypeName ($node) {

    if ($node instanceof Node\Expr\FuncCall)
      return 'expression';

    if ($node instanceof Node\Expr\Variable)
      return 'variable';

    if ($node instanceof Node\Stmt\Function_)
      return 'function';

    #var_dump($node);
    #return get_class($node);

    return 'symbol';
  }

  /** @test @internal */
  static function unittest_test () {

    PhlintTest::assertIssues('
      $x = function_that_does_not_exist();
    ', [
      'Unable to invoke undefined *function_that_does_not_exist()* on line 1.',
    ]);

    PhlintTest::assertNoIssues('
      $x = chr(64);
    ');

    PhlintTest::assertIssues('
      $x = \class_that_does_not_exist::method_that_does_not_exist();
    ', [
      'Unable to invoke undefined *\class_that_does_not_exist::method_that_does_not_exist()* on line 1.',
    ]);

    PhlintTest::assertNoIssues('
      $x = \DateTime::getLastErrors();
    ');

    PhlintTest::assertNoIssues('
      $f = function () {
        $x = \DateTime::getLastErrors();
      };
    ');

    PhlintTest::assertIssues('
      class A { static function x () {} }
      class B extends A {}
      B::y();
    ', [
      'Unable to invoke undefined *B::y()* on line 3.',
    ]);

    PhlintTest::assertNoIssues('
      class A { static function x () {} }
      class B extends A {}
      B::x();
    ');

    PhlintTest::assertIssues('
      namespace ns1;
      class A { static function x () {} }
      class B extends A {}
      B::y();
    ', [
      'Unable to invoke undefined *ns1\B::y* for the expression *B::y()* on line 4.',
    ]);

    PhlintTest::assertNoIssues('
      namespace ns1;
      class a { static function x () {} }
      class b extends a {}
      b::x();
    ');

    PhlintTest::assertIssues('
      class a {}
      $x = new A();
      $x = new B();
    ', [
      'Expression *new A()* is not using the same letter casing as class *a* on line 2.',
      'Unable to invoke undefined *B* for the expression *new B()* on line 3.',
    ]);

    PhlintTest::assertNoIssues('
      class a {}
      $x = new a();
    ');

    PhlintTest::assertIssues('
      class a {}
      try {} catch (A $e) {}
      try {} catch (B $e) {}
    ', [
      'Catch *A $e* is not using the same letter casing as class *a* on line 2.',
      'Unable to invoke undefined *B* for the catch *B $e* on line 3.',
    ]);

    PhlintTest::assertNoIssues('
      class a {}
      try {} catch (a $e) {}
    ');

    PhlintTest::assertNoIssues('
      $x = \DateTime::createFromFormat("Y-m-d", "2000-01-01");
    ');

  }

  /**
   * Test template specialization.
   *
   * @test @internal
   */
  static function unittest_templateSpecialization () {

    PhlintTest::assertNoIssues('
      function f () {
        return "2";
      }
      $x = f() + 1;
    ');

    PhlintTest::assertIssues('
      function f () {
        return "a";
      }
      $x = f() + 1;
    ', [
      'Provided expression *f()* of type *string* is not compatible in the expression *f() + 1* on line 4.',
    ]);

    PhlintTest::assertNoIssues('
      function sum ($a, $b) {
        return $a + $b;
      }
      $x = sum(2, 3);
      $x = sum("2", 3);
      $x = sum(2, "3");
    ');

    PhlintTest::assertIssues('
      function sum ($a, $b) {
        return $a + $b;
      }
      $x = sum("a", 3);
    ', [
      '
        Provided variable *$a* of type *string* is not compatible in the expression *$a + $b* on line 2.
          Trace #1: Function *sum("a", 3)* specialized for the expression *sum("a", 3)* on line 4.
      ',
    ]);

    PhlintTest::assertIssues('
      function sum ($a, $b) {
        return $a + $b;
      }
      function doSum ($c, $d) {
        return sum($c, $d);
      }
      $x = doSum("a", 3);
    ', [
      '
        Provided variable *$a* of type *string* is not compatible in the expression *$a + $b* on line 2.
          Trace #1: Function *sum("a", 3)* specialized for the expression *sum($c, $d)* on line 5.
          Trace #2: Function *doSum("a", 3)* specialized for the expression *doSum("a", 3)* on line 7.
      ',
    ]);

    PhlintTest::assertNoIssues('
      function sum ($a, $b) {
        $c = $a;
        return $c + $b;
      }

      function doSum ($d, $e) {
        $f = $d;
        return sum($f, $e);
      }

      function getExternalInput () : int {
        return 2;
      }

      $x = doSum(getExternalInput(), 3);
    ');

    PhlintTest::assertIssues('
      function sum ($a, $b) {
        $c = $a;
        return $c + $b;
      }

      function doSum ($d, $e) {
        $f = $d;
        return sum($f, $e);
      }

      function getExternalInput () : string {
        return "a";
      }

      $x = doSum(getExternalInput(), 3);
    ', [
      '
        Provided variable *$c* of type *string* is not compatible in the expression *$c + $b* on line 3.
          Trace #1: Function *sum("a", 3)* specialized for the expression *sum($f, $e)* on line 8.
          Trace #2: Function *doSum("a", 3)* specialized for the expression *doSum(getExternalInput(), 3)* on line 15.
      ',
    ]);

    PhlintTest::assertIssues('
      function foo () {
        $x = true ? rand(0, 1) : false;
        return $x;
      }
      function bar () {
        $x = foo() + 1;
      }
    ', [
      'Provided expression *foo()* of type *bool* is not compatible in the expression *foo() + 1* on line 6.',
    ]);

  }

  /**
   * Test that the analyzer does not go into infinite recursion while analyzing.
   * @test @internal
   */
  static function unittest_infiniteRecursion () {
    PhlintTest::assertNoIssues('
      function f () {
        f();
      }
      f();
    ');
  }

  /**
   * Regression test for undefined function not being reported due to
   * a closure being registered as empty symbol name.
   * @test @internal
   */
  static function unittest_closureSymbol () {
    PhlintTest::assertIssues('
      function f1 () {
        fx();
      }
      function f2 () {
        $f3 = function () {};
        $x = $f3();
      }
    ', [
      'Unable to invoke undefined *fx()* on line 2.',
    ]);
  }

  /**
   * Test execution branching.
   *
   * @test @internal
   */
  static function unittest_executionBranching () {

    PhlintTest::assertNoIssues('
      function f () {
        if (rand(0, 1))
          $x = 0;
        else
          $x = 1;
        return $x;
      }
      $x = f() + 1;
    ');

    PhlintTest::assertIssues('
      function f () {
        if (rand(0, 1))
          $x = 0;
        else
          $x = "a";
        return $x;
      }
      $x = f() + 1;
    ', [
      'Provided expression *f()* of type *string* is not compatible in the expression *f() + 1* on line 8.',
    ]);

    PhlintTest::assertIssues('
      function sum ($a, $b) {
        $c = $a;
        return $c + $b;
      }

      function doSum ($d, $e) {
        if (rand(0, 1))
          $f = $d;
        else
          $f = "a";
        return sum($f, $e);
      }

      function getExternalInput () : int {
        return 2;
      }

      $x = doSum(getExternalInput(), 3);
    ', [
      '
        Provided variable *$c* of type *string* is not compatible in the expression *$c + $b* on line 3.
          Trace #1: Function *sum("a", $b)* specialized for the expression *sum($f, $e)* on line 11.
      ',
      '
        Provided variable *$c* of type *string* is not compatible in the expression *$c + $b* on line 3.
          Trace #1: Function *sum(2|"a", 3)* specialized for the expression *sum($f, $e)* on line 11.
          Trace #2: Function *doSum(2, 3)* specialized for the expression *doSum(getExternalInput(), 3)* on line 18.
      ',
    ]);

  }

  /**
   * Test class concept.
   * @test @internal
   */
  static function unittest_classConcept () {

    PhlintTest::assertNoIssues('

      class A {
        function bar () { return 1; }
      }

      class B {
        function bar () { return 2; }
      }

      class C {
        function foo () {}
      }

      function foo ($o, $i) {
        return $o->bar() + $i;
      }

      function baz ($newObj) {
        if (rand(0, 1))
          $obj = new A();
        else
          $obj = $newObj;
        return foo($obj, 2);
      }

      $x = foo(new B(), baz(new B()));

    ');

    PhlintTest::assertIssues('

      class A {
        function bar () { return 1; }
      }

      class B {
        function bar () { return 2; }
      }

      class C {
        function foo () {}
      }

      function foo ($o, $i) {
        return $o->bar() + $i;
      }

      function baz ($newObj) {
        if (rand(0, 1))
          $obj = new A();
        else
          $obj = $newObj;
        return foo($obj, 2);
      }

      $x = foo(new B(), baz(new C()));

    ', [
      '
        Unable to invoke undefined *C::bar* for the expression *$o->bar()* on line 15.
          Trace #1: Function *foo(A|C $o, 2)* specialized for the expression *foo($obj, 2)* on line 23.
          Trace #2: Function *baz(C $newObj)* specialized for the expression *baz(new C())* on line 26.
      ',
    ]);

  }

  /**
   * Test isTraversable.
   * @test @internal
   */
  static function unittest_isTraversable () {

    PhlintTest::assertIssues('
      function myRangeSum ($values) {
        $result = 0;
        foreach ($values as $value) {
          $result += $value;
        }
        return $result;
      }

      myRangeSum([1, 2.2, "2", 3]);
      myRangeSum([1, 2.2, "2", 3, "a"]);
      myRangeSum(2);
    ', [
      '
        Provided symbol *$values* of type *int* is not compatible in the expression *foreach ($values as $value)* on line 3.
          Trace #1: Function *myRangeSum(2)* specialized for the expression *myRangeSum(2)* on line 11.
      ',
      '
        Provided variable *$value* of type *autoString* is not compatible in the expression *$result += $value* on line 4.
          Trace #1: Function *myRangeSum(autoString[] $values)* specialized for the expression *myRangeSum([1, 2.2, "2", 3, "a"])* on line 10.
      ',
    ]);

  }

}
