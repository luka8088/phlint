<?php

namespace luka8088\phlint\rule;

use \luka8088\Phlint;
use \luka8088\phlint\Test as PhlintTest;

/**
 * Makes sure that @isolated code is indeed isolated.
 *
 * @see /documentation/attribute/isolated.md
 */
class EnforceIsolatedAttribute {

  function getIdentifier () {
    return 'enforceIsolatedAttribute';
  }

  function getCategories () {
    return [
      'attribute',
      'default',
    ];
  }

  function getInferences () {
    return [
      'attribute',
      'isolation',
    ];
  }

  function visitNode ($node) {
    if (in_array('isolated', $node->getAttribute('attributes', []))
        && count($node->getAttribute('isolationBreaches', [])) > 0)
      context('result')->addIssue(
        $node,
        'Function *' . $node->name . '* not isolated.',
        $node->getAttribute('isolationBreaches', [])
      );
  }

  /** @test @internal */
  static function unittest_test () {

    PhlintTest::assertNoIssues('
      function foo () {
        $x = $GLOBALS["x"];
      }
    ');

    PhlintTest::assertIssues('
      /** @isolated */
      function foo () {
        $x = $GLOBALS["x"];
      }
    ', [
      '
        Function *foo* not isolated on line 2.
          Cause #1: Accessing superglobal *$GLOBALS* on line 3.
      ',
    ]);

  }

}
