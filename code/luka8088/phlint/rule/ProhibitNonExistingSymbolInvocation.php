<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\Result;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * @see /documentation/rule/prohibitNonExistingSymbolInvocation.md
 */
class ProhibitNonExistingSymbolInvocation {

  function getIdentifier () {
    return 'prohibitNonExistingSymbolInvocation';
  }

  function getCategories () {
    return [
      'default',
      'fatal',
    ];
  }

  function getInferences () {
    return [
      'symbol',
      'templateSpecialization',
      'type',
      'value',
    ];
  }

  function visitNode ($node) {

    if ($node instanceof Node\Expr\FuncCall)
      $this->enforceRule($node, $node);

    if ($node instanceof Node\Expr\MethodCall)
      $this->enforceRule($node, $node);

    if ($node instanceof Node\Expr\New_)
      $this->enforceRule($node, $node->class);

    if ($node instanceof Node\Expr\StaticCall)
      $this->enforceRule($node, $node);

    if ($node instanceof Node\Stmt\Catch_)
      $this->enforceRule($node, $node->type);

  }

  function enforceRule ($expressionNode, $symbolNode) {

    $symbols = NodeFacade::getSymbols($symbolNode);

    foreach ($symbols as $symbol) {

      if (isset(context('code')->symbols[$symbol])) {

        if (count(context('code')->symbols[$symbol]['definitionNodes']) == 0) {

          $symbolDisplay = isset(context('code')->symbols[$symbol]) ? context('code')->symbols[$symbol]['phpId'] : $symbol;
          $symbolDisplay = strpos($symbolDisplay, 't_') === 0 ? substr($symbolDisplay, 2) : $symbolDisplay;
          $expressionDisplay = NodeConcept::displayPrint($expressionNode);

          // Todo: Rewrite
          if (strpos($symbolDisplay . '::', 'parent::') !== false)
            continue;
          if (strpos($symbolDisplay . '::', 'self::') !== false)
            continue;
          if (strpos($symbolDisplay . '::', 'static::') !== false)
            continue;

          if ($symbolDisplay != $expressionDisplay && $symbolDisplay . '()' != ltrim($expressionDisplay, '\\'))
            $message = 'Unable to invoke undefined *' . $symbolDisplay . '*' .
              ' for the ' . NodeConcept::referencePrint($expressionNode) . '.';
          else
            $message = 'Unable to invoke undefined *' . $expressionDisplay . '*.';

          context('result')->addIssue($expressionNode, $message);

        }

      }

    }

  }

}
