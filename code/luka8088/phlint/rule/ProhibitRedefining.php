<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * @see /documentation/rule/prohibitRedefining.md
 */
class ProhibitRedefining {

  function getIdentifier () {
    return 'prohibitRedefining';
  }

  function getCategories () {
    return [
      'default',
      'phpFatal',
    ];
  }

  function getInferences () {
    return [
      'symbol',
    ];
  }

  function visitNode ($node) {

    if (NodeConcept::isDefinitionNode($node) && !NodeConcept::isNamespaceNode($node)) {

      $definitionNodes = [];

      foreach (NodeFacade::getSymbols($node) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode)
            if (!$definitionNode->getAttribute('isSpecializationTemp', false))
              $definitionNodes[] = $definitionNode;

      if (!($node instanceof Node\Expr\Closure) && count($definitionNodes) > 1)
        context('result')->addIssue(
          $node,
          'Having multiple definitions for *' . NodeConcept::sourcePrint($node->name) . '* is prohibited.'
        );
    }

  }

  /**
   * Test redefining.
   *
   * @test @internal
   */
  static function unittest_redefining () {

    $linter = PhlintTest::create();

    $linter->addSource('
      function x () {}
    ');

    $linter->addSource('
      function x () {}
    ');

    PhlintTest::assertIssues($linter->analyze('
      x();
    '), [
      'Having multiple definitions for *x* is prohibited on line 1.',
    ]);


    PhlintTest::assertNoIssues('

      class C {
        use T;
        function f () {}
      }

      class C2 {
        use T2;
        function f () {}
      }

      trait T {
        function f () {}
      }

      trait T2 {
        function f () {}
      }

      function f () {}

    ');

  }

  /**
   * Test redefining class constants.
   *
   * @test @internal
   */
  static function unittest_redefiningClassConstants () {
    PhlintTest::assertIssues('

      class A {
        const C1 = 1;
        const C2 = 2;
      }

      class B {
        const C2 = 3;
        const C2 = 4;
      }

    ', [
      'Having multiple definitions for *C2* is prohibited on line 8.',
      'Having multiple definitions for *C2* is prohibited on line 9.',
    ]);
  }

}
