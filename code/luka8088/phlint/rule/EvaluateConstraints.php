<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\phpLanguage\Evaluation;

/**
 * @see /documentation/rule/evaluateConstraints.md
 */
class EvaluateConstraints {

  function getIdentifier () {
    return 'evaluateConstraints';
  }

  function getCategories () {
    return [
      'default',
      'strict',
    ];
  }

  function getInferences () {
    return [
      'type',
      'value',
      'constraint',
    ];
  }

  function visitNode ($node) {

    if (NodeConcept::isExecutionContextNode($node))
      foreach ($node->stmts as $statement)
        if ($statement->getAttribute('isConstraint', false))
          foreach (Evaluation::evaluate($statement->args[0]) as $value)
            if ($value['type'] == 't_bool' && !$value['value'])
              context('result')->addIssue(
                $statement,
                ucfirst(NodeConcept::referencePrint($statement)) . ' failed for the ' .
                NodeConcept::referencePrint($node) . '.'
              );

  }

}
