<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\inference\Type;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\Result;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * @see /documentation/rule/prohibitIncompatibleInvocationArguments.md
 */
class ProhibitIncompatibleInvocationArguments {

  function getIdentifier () {
    return 'prohibitIncompatibleInvocationArguments';
  }

  function getCategories () {
    return [
      'default',
    ];
  }

  function getInferences () {
    return [
      'symbol',
      'templateSpecialization',
      'type',
      'value',
    ];
  }

  function visitNode ($node) {

    if (NodeConcept::isInvocationNode($node)) {

      #var_dump(get_class($node));
      #var_dump(NodeFacade::getSymbols($node));

      foreach (NodeFacade::getSymbols($node) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {

            // @todo: Rewrite
            #if ($node->getAttribute('invocationTypes', []) != $definitionNode->getAttribute('types', []))
            #  continue;

            // @todo: Rewrite
            if ($definitionNode->getAttribute('isSpecializationTemp', false))
              continue;

            $variadicIndex = 0;
            foreach ($node->args as $argumentIndex => $argument) {
              $index = $variadicIndex ? $variadicIndex : $argumentIndex;
              #var_dump($index);
              #var_dump($variadicIndex);
              if (isset($definitionNode->params[$index])) {
                if ($definitionNode->params[$index]->variadic)
                  $variadicIndex = $index;
                #var_dump($definitionNode->params[$index]->variadic, $index);
                foreach (NodeFacade::getTypes($argument) as $argumentType)
                  foreach (NodeFacade::getTypes($definitionNode->params[$index]) as $parameterType) {
                    if ($definitionNode->params[$index]->variadic)
                      $parameterType = substr($parameterType, -2) == '[]' ? substr($parameterType, 0, -2) : '';

                    #if (!Type::isImplicitlyConvertible($argumentType, $parameterType))
                    #  var_dump($argumentType . ' to ' .  $parameterType);

                    if (context('code')->isImplicitlyConvertible($argumentType, $parameterType))
                      continue;

                    context('result')->addIssue($node,
                      'Provided ' . NodeConcept::referencePrint($argument) . ' ' .
                      'of type *' . (isset(context('code')->symbols[$argumentType]) ? context('code')->symbols[$argumentType]['phpId'] : substr($argumentType, 2)) . '* ' .
                      'is not compatible in the ' . NodeConcept::referencePrint($node) . '.'
                    );

                  }
              }

              #var_dump(NodeFacade::getTypes($argument));
            }
          }



    }

    // prohibitIncompatibleInvocationArguments

  }

  /**
   * Test argument types.
   * @test @internal
   */
  static function unittest_argumentTypes () {
    PhlintTest::assertIssues('

      function foo () {
        return rand(0, 1) ? 2 : ["a" => 1];
      }

      function bar () {
        return rand(0, 1) ? 2.0 : ["a" => 1.0];
      }

      $baz = sprintf("%0.2f and %0.2f", foo(), bar());

    ', [
      '
        Provided argument *foo()* of type *int[]* is not compatible in
          the expression *sprintf("%0.2f and %0.2f", foo(), bar())* on line 10.',
      '
        Provided argument *bar()* of type *float[]* is not compatible in
          the expression *sprintf("%0.2f and %0.2f", foo(), bar())* on line 10.
      ',
    ]);
  }

}
