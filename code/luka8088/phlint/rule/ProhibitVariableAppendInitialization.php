<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * Prohibit initializing variables by using the append operator.
 *
 * For example:
 *   $x = ['a' => 'b']; // Ok.
 *   $x[] = 'b'; // Prohibited.
 *
 */
class ProhibitVariableAppendInitialization {

  function getIdentifier () {
    return 'prohibitVariableAppendInitialization';
  }

  function getInferences () {
    return [
      'symbol',
      'symbolInitialization',
    ];
  }

  function visitNode($node) {

    if (($node instanceof Node\Expr\Assign) || ($node instanceof Node\Expr\AssignRef))
      if ($node->var instanceof Node\Expr\ArrayDimFetch)
        if (NodeConcept::isVariableNode($node->var->var)) {
          foreach (NodeFacade::getSymbols($node->var->var) as $symbol) {
            if (in_array($symbol, $node->var->var->getAttribute('isInitialization', [])))
              context('result')->addIssue(
                $node,
                ucfirst(NodeConcept::referencePrint($node->var->var)) . ' initialized using append operator.'
              );
          }
        }

  }

  /**
   * General rule test.
   *
   * @test @internal
   */
  static function unittest_test () {

    PhlintTest::assertNoIssues('
      function f () {
        $a = [];
        $a[] = 2;
      }
    ');

    PhlintTest::assertNoIssues('
      function f ($a) {
        $a[] = 2;
      }
    ');

    PhlintTest::assertIssues('
      function f () {
        $a[] = 2;
        $a = [];
      }
    ', [
      'Variable *$a* initialized using append operator on line 2.',
    ]);

    PhlintTest::assertIssues('
      function f () {
        if (rand(0, 1))
          $a[] = 2;
        $a = [];
      }
    ', [
      'Variable *$a* initialized using append operator on line 3.',
    ]);

  }

}
