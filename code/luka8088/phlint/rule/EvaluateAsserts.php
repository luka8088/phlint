<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\phpLanguage\Evaluation;

/**
 * @see /documentation/rule/evaluateAsserts.md
 */
class EvaluateAsserts {

  function getIdentifier () {
    return 'evaluateAsserts';
  }

  function getCategories () {
    return [
      'default',
      'strict',
    ];
  }

  function getInferences () {
    return [
      'type',
      'value',
    ];
  }

  function visitNode ($node) {

    if ($node->getAttribute('isGenerated', false))
      return;

    if (NodeConcept::isInvocationNode($node) && NodeFacade::getSymbols($node) == ['f_assert'])
      if (count($node->args) > 0)
        foreach (Evaluation::evaluate($node->args[0]) as $value)
          if ($value['type'] == 't_bool' && !$value['value'])
            context('result')->addIssue(
              $node,
              'Assertion failure for the ' . NodeConcept::referencePrint($node) . '.'
            );

  }

}
