<?php

namespace luka8088\phlint;

use \PhpParser\Node;

class NodeFacade
{

  /**
   * Get node analysis-time known symbols.
   *
   * @param Node|string $node Symbols holding `Node` or a literal symbol.
   * @return string[]
   */
  static function getSymbols ($node) {

    $symbols = [];

    if (is_string($node))
      $symbols[] = $node;

    if ($node instanceof Node)
      foreach ($node->getAttribute('symbols', []) as $symbol)
        $symbols[] = strpos($symbol, 'n_.') === 0 ? substr($symbol, 3) : $symbol;

    if ($node instanceof Node\Arg)
      foreach (NodeFacade::getSymbols($node->value) as $symbol)
        $symbols[] = $symbol;

    if ($node instanceof Node\Expr\ConstFetch)
      foreach (NodeFacade::getSymbols($node->name) as $symbol)
        $symbols[] = $symbol;

    if ($node instanceof Node\Expr\New_)
      foreach (NodeFacade::getSymbols($node->class) as $symbol)
        $symbols[] = $symbol;

    return array_unique($symbols);

  }

  /**
   * Get node analysis-time known types.
   *
   * @param Node $node Types holding `Node`.
   * @return string[]
   */
  static function getTypes ($node) {

    $types = [];

    if ($node instanceof Node)
      foreach ($node->getAttribute('types', []) as $type)
        $types[] = $type;

    if ($node instanceof Node\Arg)
      foreach (NodeFacade::getTypes($node->value) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\ArrayDimFetch)
      foreach (NodeFacade::getTypes($node->var) as $arrayType)
        $types[] = substr($arrayType, -2) == '[]' ? substr($arrayType, 0, -2) : 't_mixed';

    if ($node instanceof Node\Expr\ArrayItem)
      foreach (NodeFacade::getTypes($node->value) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\BinaryOp\BooleanAnd)
      $types[] = 't_bool';

    if ($node instanceof Node\Expr\Closure)
      foreach (NodeFacade::getSymbols($node) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\New_)
      foreach (NodeFacade::getTypes($node->class) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\Ternary)
      foreach (array_merge(NodeFacade::getTypes($node->if), NodeFacade::getTypes($node->else)) as $type)
        $types[] = $type;

    if ($node instanceof Node\Name)
      foreach (NodeFacade::getSymbols($node) as $type)
        $types[] = $type;

    if (($node instanceof Node\Param) && ($node->type instanceof Node\Name))
      foreach (NodeFacade::getTypes($node->type) as $type)
        $types[] = $type;

    if ($node instanceof Node\Scalar\DNumber)
      $types[] = 't_float';

    if ($node instanceof Node\Scalar\LNumber)
      $types[] = 't_int';

    if ($node instanceof Node\Scalar\MagicConst)
      if ($node instanceof Node\Scalar\MagicConst\Line)
        $types[] = 't_int';
      else
        $types[] = 't_string';

    if ($node instanceof Node\Scalar\String_)
      if (((string) ((int) $node->value)) === (string) $node->value)
        $types[] = 't_autoInteger';
      else if (is_numeric($node->value))
        $types[] = 't_autoFloat';
      else
        $types[] =  't_string';

    return array_unique($types);

  }

  /**
   * Get node analysis-time known values.
   *
   * @param Node|string $node Values holding `Node` or a literal value.
   * @return mixed[]
   */
  static function getValues ($node) {

    $values = [];

    if (is_bool($node))
      $values[] = [
        'type' => 't_bool',
        'value' => $node,
      ];

    if (is_int($node))
      $values[] = [
        'type' => 't_int',
        'value' => $node,
      ];

    if (is_string($node))
      $values[] = [
        'type' => 't_string',
        'value' => $node,
      ];

    if ($node instanceof Node)
      foreach ($node->getAttribute('values', []) as $value)
        $values[] = $value;

    if ($node instanceof Node\Arg)
      foreach (NodeFacade::getValues($node->value) as $value)
        $values[] = $value;

    if ($node instanceof Node\Expr\ConstFetch) {
      if (strtolower($node->name->toString()) == 'null')
        $values[] = [
          'type' => '',
          'value' => null,
        ];
      if (strtolower($node->name->toString()) == 'true')
        $values[] = [
          'type' => 't_bool',
          'value' => true,
        ];
      if (strtolower($node->name->toString()) == 'false')
        $values[] = [
          'type' => 't_bool',
          'value' => false,
        ];
    }

    if ($node instanceof Node\Const_)
      foreach (NodeFacade::getValues($node->value) as $value)
        $values[] = $value;

    if ($node instanceof Node\Expr\Ternary) {
      if ($node->if)
        foreach (NodeFacade::getValues($node->if) as $value)
          $values[] = $value;
      foreach (NodeFacade::getValues($node->else) as $value)
        $values[] = $value;
    }

    if ($node instanceof Node\Param)
      if ((count($values) == 0) && $node->default instanceof Node)
        foreach (NodeFacade::getValues($node->default) as $value)
          $values[] = $value;

    if ($node instanceof Node\Scalar\DNumber)
      $values[] = [
        'type' => 't_float',
        'value' => $node->value,
      ];

    if ($node instanceof Node\Scalar\LNumber)
      $values[] = [
        'type' => 't_int',
        'value' => $node->value,
      ];

    if ($node instanceof Node\Scalar\String_) {
      $type = 't_string';
      if (is_numeric($node->value))
        $type = 't_autoFloat';
      if (((string) ((int) $node->value)) === (string) $node->value)
        $type = 't_autoInteger';
      $values[] = [
        'type' => $type,
        'value' => $node->value,
      ];
    }

    $uniqueValues = function ($values) {
      $valueMap = [];
      foreach ($values as $value)
        $valueMap[$value['type'] . '/' . json_encode($value['value'])] = $value;
      return array_values($valueMap);
    };

    return $uniqueValues($values);

  }

}
