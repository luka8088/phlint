<?php

namespace luka8088\phlint\autoload;

use \Exception;
use \luka8088\phlint\Code;
use \luka8088\phlint\Internal;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;
use \PhpParser\Node\Expr\BinaryOp\Concat;
use \PhpParser\Node\Expr\Include_;
use \PhpParser\Node\Scalar\MagicConst\Dir as MagicConstDir;
use \PhpParser\Node\Scalar\MagicConst\File as MagicConstFile;
use \PhpParser\Node\Scalar\String_;
use \PhpParser\NodeTraverser;

class Composer {

  protected $path = '';
  protected $loadedFileMap = [];

  function __construct ($path) {
    $this->path = $path;
  }

  protected $initialized = false;

  protected $map = [
    'classMap' => [],
    'namespaceMap' => [],
    'psr4Map' => [],
  ];

  function initialize () {

    if ($this->initialized)
      return;
    $this->initialized = true;

    if (is_file(dirname($this->path) . '/composer.lock')) {
      $composerLock = json_decode(file_get_contents(dirname($this->path) . '/composer.lock'), true);

      foreach ($composerLock['platform'] as $extensionSignature => $extensionVersion) {

        $id = '';

        if ($extensionSignature == 'php')
          $id = 'standard';
        else if (strpos($extensionSignature, 'ext-') === 0)
          $id = 'extension-' . substr($extensionSignature, 4);

        if ($id == 'standard')
          continue;

        context('code')->addAst(Internal::importDefinitions($id));

      }

    }

    if (is_file(dirname($this->path) . '/vendor/composer/autoload_classmap.php'))
      $this->map['classMap'] = require(dirname($this->path) . '/vendor/composer/autoload_classmap.php');

    if (is_file(dirname($this->path) . '/vendor/composer/autoload_namespaces.php'))
      $this->map['namespaceMap'] = require(dirname($this->path) . '/vendor/composer/autoload_namespaces.php');

    if (is_file(dirname($this->path) . '/vendor/composer/autoload_psr4.php'))
      $this->map['psr4Map'] = require(dirname($this->path) . '/vendor/composer/autoload_psr4.php');

    if (is_file(dirname($this->path) . '/vendor/composer/autoload_files.php'))
      foreach (require(dirname($this->path) . '/vendor/composer/autoload_files.php') as $path)
        $this->autoloadFile($path);

  }

  function __invoke ($symbol) {

    $this->initialize();

    foreach ($this->map['classMap'] as $className => $path)
      if ($symbol == $className)
        $this->autoloadFile($path);

    foreach ($this->map['namespaceMap'] as $namespace => $paths)
      if (self::inNamespace($symbol, $namespace))
        foreach ($paths as $path) {
          $this->autoloadFile($path . '/' . str_replace('_', '/', ltrim($symbol, '_')) . '.php');
          $this->autoloadFile($path . '/' . str_replace('\\', '/', ltrim($symbol, '\\')) . '.php');
        }

    foreach ($this->map['psr4Map'] as $namespace => $paths)
      if (self::inNamespace($symbol, $namespace))
        foreach ($paths as $path)
          $this->autoloadFile(
            $path . '/' .
            str_replace('\\', '/', substr(ltrim($symbol, '\\'), strlen(ltrim($namespace, '\\')))) . '.php'
          );

  }

  static function inNamespace ($symbol, $namespace) {
    if (trim($namespace, '_') == '' || strpos(ltrim($symbol, '_') . '_', trim($namespace, '_') . '_') === 0)
      return true;
    if (trim($namespace, '\\') == '' || strpos(ltrim($symbol, '\\') . '\\', trim($namespace, '\\') . '\\') === 0)
      return true;
    return false;
  }

  function autoloadFile ($path) {

    if (!is_file($path))
      return;

    if (isset(context('code')->loadedFileMap[$path]) || isset(context('code')->loadedFileMap[realpath($path)]))
      return;

    context('code')->loadedFileMap[$path] = true;
    context('code')->loadedFileMap[realpath($path)] = true;

    context('code')->load([[
      'path' => $path,
      'source' => '',
      'isLibrary' => true,
    ]]);


    $self = $this;

    $ast = Code::parse(file_get_contents($path), $path);

    $traverser = new NodeTraverser();
    $traverser->addVisitor(new _InlineNodeVisitor(function ($node) use ($self, $path) {
      if ($node instanceof Include_) {
        $node = clone $node;

        $subTraverser = new NodeTraverser();
        $subTraverser->addVisitor(new _InlineNodeVisitor(function ($node) use ($path) {
          if (($node instanceof Include_) || ($node instanceof String_) || ($node instanceof Concat)) {
            return $node;
          }
          if ($node instanceof \PhpParser\Node\Scalar\EncapsedStringPart)
            return $node;
          if ($node instanceof MagicConstDir) {
            return new String_(dirname($path));
            #$node = clone $node;
            #var_dump($node);
            #var_dump(NodeConcept::sourcePrint($node->expr));
            #exit;
          }
          if ($node instanceof MagicConstFile) {
            return new String_($path);
            #$node = clone $node;
            #var_dump($node);
            #var_dump(NodeConcept::sourcePrint($node->expr));
            #exit;
          }
          #var_dump($node);
          #exit;
          // @todo: Revisit.
          return new String_('Unrecognized node: ' . NodeConcept::sourcePrint($node) . '__sfk209uylrjkqhwiu3ydlrhrwa8dj');
          #exit;
        }));
        $subTraverser->traverse([$node]);

        #var_dump(NodeConcept::sourcePrint($node->expr));

        $includePath = eval('return call_user_func(function () { return ' . NodeConcept::sourcePrint($node->expr) . '});');
        #var_dump($x);
        #exit;

        // @todo: Revisit.
        if (!realpath($includePath)) {
          return;
        }

        if (realpath($includePath) === false) {
          throw new Exception('File *' . $includePath . '* not found.');
        }

        $self->autoloadFile($includePath);

      }
    }));
    $traverser->traverse($ast);

  }

  /**
   * Test forward reference autoloading.
   *
   * @test @internal
   */
  static function unittest_forwardReference () {

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R');

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor');

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer');

    file_put_contents(
      sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer/autoload_files.php',
      '<?php return ' . var_export([sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/A.php'], true) . ';'
    );

    file_put_contents(
      sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer/autoload_classmap.php',
      '<?php return ' . var_export(['C' => sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/C.php'], true) . ';'
    );

    file_put_contents(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/A.php', '<?php
      class A {
        function b () {
          return C::d();
        }
      }
    ');

    file_put_contents(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/C.php', '<?php
      class C {
        function d () {
          return 2;
        }
      }
    ');

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/composer.json'));

    PhlintTest::assertNoIssues($linter->analyze('
      $x = A::b();
    '));

  }

  /**
   * Test that arbitrary `include` does not break the analyzer.
   * @test @internal
   */
  static function unittest_arbitraryInclude () {

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R');

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor');

    if (!is_dir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer'))
      mkdir(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer');

    file_put_contents(
      sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/vendor/composer/autoload_files.php',
      '<?php return ' . var_export([sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/A.php'], true) . ';'
    );

    file_put_contents(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/A.php', '<?php
      class A {
        function b () {
          return 1;
        }
        function x ($path) {
          include $path;
        }
      }
    ');

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/gKBRfZR1ujwAATDRMFLrQU6R/composer.json'));

    PhlintTest::assertNoIssues($linter->analyze('
      $x = A::b();
    '));

  }

  /**
   * Test file include that depends on a class.
   * @test @internal
   */
  static function unittest_indirectFunctionClassDependecy () {

    if (!is_dir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG'))
      mkdir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG');

    if (!is_dir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor'))
      mkdir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor');

    if (!is_dir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor/composer'))
      mkdir(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor/composer');

    file_put_contents(
      sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor/composer/autoload_files.php',
      '<?php return ' . var_export([sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/functions.php'], true) . ';'
    );

    file_put_contents(
      sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/vendor/composer/autoload_classmap.php',
      '<?php return ' . var_export(['A\B\C' => sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/C.php'], true) . ';'
    );

    file_put_contents(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/functions.php', '<?php
      use A\B;
      use A\B\C;
      function foo($x) {
        B\C::bar($x);
        C::bar($x);
      }
    ');

    file_put_contents(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/C.php', '<?php
      namespace A\B {
        class C {
          function bar ($x) {}
        }
      }
    ');

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/hUY29ImLj8C9d4F5luMDfHSG/composer.json'));

    PhlintTest::assertNoIssues($linter->analyze('
      foo(rand(0, 1));
    '));

  }

  /**
   * Test file include that depends on a class.
   * @test @internal
   */
  static function unittest_psr4IndirectFunctionClassDependecy () {

    if (!is_dir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH'))
      mkdir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH');

    if (!is_dir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/vendor'))
      mkdir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/vendor');

    if (!is_dir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/vendor/composer'))
      mkdir(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/vendor/composer');

    file_put_contents(
      sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/vendor/composer/autoload_psr4.php',
      '<?php return ' . var_export(['A\B' => [sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH']], true) . ';'
    );

    file_put_contents(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/C.php', '<?php
      namespace A\B {
        class C {
          function bar ($x) {}
        }
      }
    ');

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/zWa1lRFC1Uwgunf7fyIAMBsH/composer.json'));

    PhlintTest::assertNoIssues($linter->analyze('
      namespace A\A;
      use \A\B\C;
      $x = new C();
    '));

  }

  /**
   * Test autoloading class extends.
   * @test @internal
   */
  static function unittest_autoloadExtends () {

    if (!is_dir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy'))
      mkdir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy');

    if (!is_dir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor'))
      mkdir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor');

    if (!is_dir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor/composer'))
      mkdir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor/composer');

    if (!is_dir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/A'))
      mkdir(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/A');

    file_put_contents(
      sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor/composer/autoload_psr4.php',
      '<?php return ' . var_export(['A' => [sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/A']], true) . ';'
    );

    file_put_contents(
      sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/vendor/composer/autoload_namespaces.php',
      '<?php return ' . var_export(['C' => [sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy']], true) . ';'
    );

    file_put_contents(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/A/B.php', '<?php
      namespace A;
      class B extends \C {}
    ');

    file_put_contents(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/C.php', '<?php
      class C {
        function foo () {}
      }
    ');

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/lDwvZhJ3j9hIAkBl3qwY7wYy/composer.json'));

    PhlintTest::assertIssues($linter->analyze('
      namespace D;
      use \A\B;
      class E {
        function baz (B $b) {
          $b->foo();
          $b->bar();
        }
      }
    '), [
      'Unable to invoke undefined *A\B::bar* for the expression *$b->bar()* on line 6.',
    ]);

  }

  /**
   * Test autoloading by fully qualified class name.
   * @test @internal
   */
  static function unittest_autoloadFullyQualified () {

    PhlintTest::mockFilesystem(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/', [
      '/vendor/composer/autoload_classmap.php' =>
        '<?php return ' . var_export([
          'A_B' => sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/vendor/foo/bar/A/B.php',
        ], true) . ';',
      '/vendor/foo/bar/A/B.php' => '<?php
        class A_B {
          function foo () {}
        }
      ',
    ]);

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/composer.json'));

    PhlintTest::assertIssues($linter->analyze('
      namespace C;
      class D {
        function bar () {
        $bar = new \A_B();
        $bar->foo();
        $bar->baz();
        }
      }
    '), [
      'Unable to invoke undefined *A_B::baz* for the expression *$bar->baz()* on line 6.',
    ]);

  }

  /**
   * Regression test for the issue:
   *   Unable to invoke undefined *Z\X\C\D* for the expression *new C\D()* on line 5.
   *
   * @test @internal
   */

  static function unittest_autoloadRelativeClassPath () {

    PhlintTest::mockFilesystem(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/', [
      '/vendor/composer/autoload_psr4.php' =>
        '<?php return ' . var_export([
          'A' => [sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/vendor/company/project/A'],
        ], true) . ';',
      '/vendor/company/project/A/B/D.php' => '<?php
        namespace A\B;
        class D {
          function foo () {}
        }
      ',
    ]);

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/composer.json'));

    PhlintTest::assertIssues($linter->analyze('
      namespace Z\X;
      use \A\B as C;
      class Y {
        function baz () {
          $obj = new C\D();
          $obj->foo();
          $obj->bar();
        }
      }
    '), [
      'Unable to invoke undefined *A\B\D::bar* for the expression *$obj->bar()* on line 7.',
    ]);

  }

}

// Temporary workaround until NodeTraverser::traverse
// is used instead of the one from PhpParser
// @todo: Remove
class _InlineNodeVisitor extends \PhpParser\NodeVisitorAbstract {

  protected $callback = null;

  function __construct ($callback) {
    $this->callback = $callback;
  }

  function leaveNode(\PhpParser\Node $node) {
    return call_user_func($this->callback, $node);
  }

}
