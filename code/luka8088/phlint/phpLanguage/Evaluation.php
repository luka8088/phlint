<?php

namespace luka8088\phlint\phpLanguage;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \PhpParser\Node;

/**
 * Holds some PHP specifics which would otherwise have to be
 * hard-coded in various places.
 * This class specifically deals with analysis-time code value evaluation.
 */
class Evaluation {

  /**
   * Analyzes the code and infers the values that it would yield if it would
   * be evaluated.
   */
  static function evaluate ($node) {

    $values = [];

    foreach (NodeFacade::getValues($node) as $value)
      $values[] = $value;

    if ($node instanceof Node\Arg)
      foreach (Evaluation::evaluate($node->value) as $value)
        $values[] = $value;

    /** @see http://www.php.net/manual/en/function.is-numeric.php */
    if (NodeConcept::isInvocationNode($node) && NodeFacade::getSymbols($node) == ['f_is_numeric'])
      if (count($node->args) > 0)
        foreach (NodeFacade::getValues($node->args[0]) as $value) {
          $evaluatedValue = [
            'type' => 't_bool',
            'value' => is_numeric($value['value']),
          ];
          $values[] = $evaluatedValue;
        }

    /** @see http://www.php.net/manual/en/function.is-object.php */
    if (NodeConcept::isInvocationNode($node) && NodeFacade::getSymbols($node) == ['f_is_object'])
      if (count($node->args) > 0)
        foreach (NodeFacade::getTypes($node->args[0]) as $type)
          $values[] = [
            'type' => 't_bool',
            'value' => strpos($type, 'c_') === 0,
          ];

    $uniqueValues = function ($values) {
      $valueMap = [];
      foreach ($values as $value)
        $valueMap[$value['type'] . '/' . json_encode($value['value'])] = $value;
      return array_values($valueMap);
    };

    return $uniqueValues($values);

  }

}
