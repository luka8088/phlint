<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\inference\Type;
use \luka8088\phlint\inference\Value;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\phpLanguage;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * Links invocation to invocable symbols.
 */
class SymbolLink {

  function getIdentifier () {
    return 'symbolLink';
  }

  protected $importMap = [];

  function getPass () {
    return [30, 40, 60];
  }

  function resetState () {
    $this->importMap = [];
  }

  function beforeTraverse ($nodes) {
      $this->resetState();
  }

  function visitNode ($node) {

    $this->link($node);

    $this->inferImplicitlyConvertable($node);

    $this->inferConditionalGuarantees($node);

    /**
     * Link the current class that has the `Node\Stmt\TraitUse` statement
     * to the referenced trait.
     */
    if ($node instanceof Node\Stmt\TraitUse)
      foreach (Scope::get($node) as $scope)
        foreach (context('code')->symbols[Scope::contextScope($scope)]['definitionNodes'] as $classNode) {
          assert(($classNode instanceof Node\Stmt\Class_) || ($classNode instanceof Node\Stmt\Trait_));
          $traits = $classNode->getAttribute('traits', []);
          foreach ($node->traits as $traitNameNode)
            foreach (NodeFacade::getSymbols($traitNameNode) as $traitSymbol)
              $traits[] = $traitSymbol;
          $classNode->setAttribute('traits', $traits);
        }

  }

  /**
   * Links a node to a symbol.
   * This method links nodes whose fully qualified symbol identifier is not
   * necessarily known without a lookup - and so it is done in a separate pass
   * from `Symbol` inference.
   *
   * @param mixed $node
   * @return void
   */
  function link ($node) {

    if ($node->getAttribute('isLibrary', false) && !$node->getAttribute('isSpecialization', false))
      return;

    if ($node instanceof Node\Expr\ClassConstFetch) {
      #$node->class->setAttribute('symbols', Symbol::lookup($node->class, 'class'));
      $node->setAttribute('symbols', Symbol::lookup($node, 'constant'));
      foreach (NodeFacade::getSymbols($node) as $symbol)
        context('code')->registerSymbolLink($symbol, $node);
    }

    if ($node instanceof Node\Expr\Closure)
      if ($node->returnType && ($node->returnType instanceof Node))
        $node->returnType->setAttribute('symbols', Symbol::lookup($node->returnType, 'class'));

    if ($node instanceof Node\Expr\ConstFetch) {
      $node->name->setAttribute('symbols', Symbol::lookup($node->name, 'constant'));
      foreach (NodeFacade::getSymbols($node->name) as $symbol)
        context('code')->registerSymbolLink($symbol, $node->name);
    }

    if ($node instanceof Node\Expr\FuncCall) {
      $node->setAttribute('symbols', Symbol::lookup($node, 'function'));
      foreach (NodeFacade::getSymbols($node) as $symbol)
        context('code')->registerSymbolLink($symbol, $node);
    }

    if (($node instanceof Node\Expr\FuncCall) && NodeFacade::getSymbols($node) == ['f_function_exists'] && count($node->args) > 0) {
      $node->args[0]->setAttribute('symbols', Symbol::lookup($node->args[0], 'function'));
      foreach (NodeFacade::getSymbols($node->args[0]) as $symbol)
        context('code')->registerSymbolLink($symbol, $node->args[0]);
    }

    if ($node instanceof Node\Expr\Instanceof_)
      $node->class->setAttribute('symbols', Symbol::lookup($node->class, 'class'));

    if ($node instanceof Node\Expr\MethodCall) {
      #$node->var->setAttribute('symbols', Symbol::lookup($node->var, 'class'));
      $node->setAttribute('symbols', Symbol::lookup($node));
      foreach (NodeFacade::getSymbols($node) as $symbol)
        context('code')->registerSymbolLink($symbol, $node);
    }

    if ($node instanceof Node\Expr\New_)
      $node->class->setAttribute('symbols', Symbol::lookup($node->class, 'class'));

    if ($node instanceof Node\Expr\StaticCall) {
      #$node->class->setAttribute('symbols', Symbol::lookup($node->class, 'class'));
      $node->setAttribute('symbols', Symbol::lookup($node, 'function'));
      foreach (NodeFacade::getSymbols($node) as $symbol)
        context('code')->registerSymbolLink($symbol, $node);
    }

    if ($node instanceof Node\Expr\StaticPropertyFetch) {
      #$node->class->setAttribute('symbols', Symbol::lookup($node->class, 'class'));
      $node->setAttribute('symbols', Symbol::lookup($node, 'variable'));
      foreach (NodeFacade::getSymbols($node) as $symbol)
        context('code')->registerSymbolLink($symbol, $node);
    }

    if ($node instanceof Node\Param)
      if ($node->type && ($node->type instanceof Node))
        $node->type->setAttribute('symbols', Symbol::lookup($node->type, 'class'));

    if ($node instanceof Node\Stmt\Catch_)
      $node->type->setAttribute('symbols', Symbol::lookup($node->type, 'class'));

    if ($node instanceof Node\Stmt\Class_) {
      if ($node->extends)
        $node->extends->setAttribute('symbols', Symbol::lookup($node->extends, 'class'));
      foreach ($node->implements as $interface_)
        $interface_->setAttribute('symbols', Symbol::lookup($interface_, 'class'));
    }

    if ($node instanceof Node\Stmt\ClassMethod)
      if ($node->returnType && ($node->returnType instanceof Node))
        $node->returnType->setAttribute('symbols', Symbol::lookup($node->returnType, 'class'));


    if ($node instanceof Node\Stmt\Function_)
      if ($node->returnType && ($node->returnType instanceof Node))
        $node->returnType->setAttribute('symbols', Symbol::lookup($node->returnType, 'class'));

    if ($node instanceof Node\Stmt\Interface_)
      foreach ($node->extends as $interface_)
        $interface_->setAttribute('symbols', Symbol::lookup($interface_, 'class'));

    if ($node instanceof Node\Stmt\TraitUse)
      foreach ($node->traits as $traitName)
        $traitName->setAttribute('symbols', Symbol::lookup($traitName, 'class'));

  }

  function inferImplicitlyConvertable ($node) {

    if ($node instanceof Node\Stmt\Class_)
      if ($node->extends)
        foreach (NodeFacade::getSymbols($node) as $symbol)
          foreach (NodeFacade::getSymbols($node->extends) as $implicitlyConvertableSymbol)
            context('code')->symbols[$symbol]['implicitlyConvertable'][] = $implicitlyConvertableSymbol;

    if ($node instanceof Node\Stmt\Class_)
      foreach ($node->implements as $interface_)
        foreach (NodeFacade::getSymbols($node) as $symbol)
          foreach (NodeFacade::getSymbols($interface_) as $implicitlyConvertableSymbol)
            context('code')->symbols[$symbol]['implicitlyConvertable'][] = $implicitlyConvertableSymbol;

    if ($node instanceof Node\Stmt\Interface_)
      foreach ($node->extends as $interface_)
        foreach (NodeFacade::getSymbols($node) as $symbol)
          foreach (NodeFacade::getSymbols($interface_) as $implicitlyConvertableSymbol)
            context('code')->symbols[$symbol]['implicitlyConvertable'][] = $implicitlyConvertableSymbol;

  }

  function inferConditionalGuarantees ($node) {

    if (($node instanceof Node\Stmt\If_) || ($node instanceof Node\Stmt\ElseIf_) || ($node instanceof Node\Expr\Ternary)) {

      $guarantees = phpLanguage\ConditionalGuarantee::evaluateCondition($node->cond);

      foreach ($guarantees as $guarantee) {

        foreach (NodeFacade::getSymbols($guarantee['node']) as $symbol) {

          $symbol = Symbol::concat($guarantee['node']->getAttribute('scope', ''), Symbol::unqualified($symbol));

          if (in_array('t_undefined', $guarantee['excludesTypes'])) {

            if (Symbol::symbolIdentifierGroup($symbol) == 'function') {
              if (!isset(context('code')->symbols[$symbol]))
                context('code')->symbols[$symbol] = [
                  'id' => $symbol,
                  'phpId' => '',
                  'aliasOf' => '',
                  'definitionNodes' => [],
                ];
              context('code')->symbols[$symbol]['definitionNodes'][] = new Node\Stmt\Function_('');
            }

          }

        }

      }

    }

  }

  /**
   * Test symbol autoloading.
   *
   * @test @internal
   */
  static function unittest_autoloader () {

    $linter = PhlintTest::create();

    $linter->addAutoloader(function ($symbol) {
      if ($symbol == 'class_that_needs_to_be_autoloaded') {
        context('code')->load([[
          'path' => '',
          'source' => '
            class class_that_needs_to_be_autoloaded {
              function x () {}
            }
          ',
          'isLibrary' => false,
        ]]);
      }
      if ($symbol == 'a\\class_that_needs_to_be_autoloaded') {
        context('code')->load([[
          'path' => '',
          'source' => '
            namespace a {
              class class_that_needs_to_be_autoloaded {
                function y () {}
              }
            }
          ',
          'isLibrary' => false,
        ]]);
      }
    });

    PhlintTest::assertNoIssues($linter->analyze('
      $x = \class_that_needs_to_be_autoloaded::x();
    '));

    PhlintTest::assertNoIssues($linter->analyze('
      namespace a {
        $x = class_that_needs_to_be_autoloaded::y();
      }
    '));

    PhlintTest::assertIssues($linter->analyze('
      $x = \class_that_will_not_be_autoloaded::x();
    '), [
      'Unable to invoke undefined *\class_that_will_not_be_autoloaded::x()* on line 1.',
    ]);

    PhlintTest::assertNoIssues($linter->analyze('
      $f = function () {
        $x = \example::x();
      };
      class example extends \class_that_needs_to_be_autoloaded {}
    '));

    PhlintTest::assertIssues($linter->analyze('
      $f = function () {
        $x = \example::x();
      };
      class example extends \class_that_will_not_be_autoloaded {}
    '), [
      'Unable to invoke undefined *\example::x()* on line 2.',
    ]);
  }

  /**
   * Test lookahead.
   *
   * @test @internal
   */
  static function unittest_lookahead () {

    $linter = PhlintTest::create();

    $linter->addSource('
      class X {
        function z () {
          $y = new Y();
        }
      }
    ');

    $linter->addSource('
      class Y {}
    ');

    PhlintTest::assertNoIssues($linter->analyze('
      $x = new X();
    '));

  }

  /**
   * Test that autoloading from the middle of the code does not reset
   * the traverse state.
   * @test @internal
   */
  static function unittest_traverseStateOnAutoload () {

    $linter = PhlintTest::create();

    $linter->addAutoloader(function ($symbol) {
      if ($symbol == 'A') {
        context('code')->load([[
          'path' => '',
          'source' => '
            class A {
              function bar () {}
            }
          ',
          'isLibrary' => false,
        ]]);
      }
      if ($symbol == 'B') {
        context('code')->load([[
          'path' => '',
          'source' => '
            class B {
              function baz () {}
            }
          ',
          'isLibrary' => false,
        ]]);
      }
    });

    PhlintTest::assertNoIssues($linter->analyze('
      function foo ($x) {
        $x["b"] = new B();

        /** @var A */
        $o = $x["a"];
        $o->bar();
      }
    '));

  }

  /**
   * Test namespaced functions symbol linking.
   *
   * @test @internal
   */
  static function unittest_namespacedFunctions () {

    PhlintTest::assertNoIssues('

      namespace {
        function someFunction () {}
      }

      namespace example {
        function someFunction () {}
        function additionalFunction () {}
      }

      namespace {
        someFunction();
        \someFunction();
        \example\someFunction();
        \example\additionalFunction();
      }

    ');

    PhlintTest::assertNoIssues('

      namespace {
        function someFunction () {}
      }

      namespace example {
      }

      namespace example {
        someFunction();
      }

    ');

    /*
    // Function calls inside namespaces are not bound to their namespace.
    // @todo: Revisit, should be fixed in the NameResolver?
    PhlintTest::assertNoIssues('

      namespace {
      }

      namespace example {
        function someFunction () {}
      }

      namespace example {
        someFunction();
      }

    ');
    /**/

    PhlintTest::assertIssues('
      namespace example {
        someFunction();
      }
    ', [
      'Unable to invoke undefined *example\someFunction* for the expression *someFunction()* on line 2.',
    ]);

  }

  /**
   * Symbol imports.
   *
   * @test @internal
   */
  static function unittest_symbolImports () {

    PhlintTest::assertNoIssues('
      namespace a {
        b\A::f();
        $x = new b\A();
      }

      namespace a\b {
        class A {
          function f () {}
        }
      }
    ');

  }

  /**
   * Test referencing classes through variables.
   *
   * @test @internal
   */
  static function unittest_variableClass () {

    PhlintTest::assertIssues('
      $undefinedVariable1::$undefinedVariable2();
    ', [
      'Variable *$undefinedVariable1* used before initialized on line 1.',
      'Variable *$undefinedVariable2* used before initialized on line 1.',
      // @todo: Re-enable after known issues are fixed.
      #'Unable to invoke undefined *$undefinedVariable1::$undefinedVariable2();*.',
    ]);

  }

  /**
   * Test trait method lookup.
   * @test @internal
   */
  static function unittest_traitMethod () {

    PhlintTest::assertNoIssues('
      trait T {
        function foo () {}
      }
      class C {
        use T;
      }
      $c = new C();
      $c->foo();
    ');

    PhlintTest::assertNoIssues('
      namespace a {
        trait T {
          function foo () {}
        }
      }
      namespace b {
        class C {
          use \a\T;
        }
        $c = new C();
        $c->foo();
      }
    ');

  }

  /**
   * Test import alias lookup.
   * @test @internal
   */
  static function unittest_aliasLookup () {
    PhlintTest::assertNoIssues('
      namespace a\b {
        class D {}
      }
      namespace c {
        use \a\b as i;
        $x = new i\D;
      }
    ');
  }

}
