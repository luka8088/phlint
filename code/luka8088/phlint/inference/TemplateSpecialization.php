<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\Code;
use \luka8088\phlint\inference;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\phpLanguage\Evaluation;
use \PhpParser\Node;

class TemplateSpecialization {

  function getIdentifier () {
    return 'templateSpecialization';
  }

  function getDependencies () {
    return [
      'constraint',
      'symbol',
      'type',
      'value',
    ];
  }

  function getPass () {
    return 50;
  }

  function visitNode ($node) {

    if (!$node->getAttribute('isReachable', true))
      return;

    if (NodeConcept::isInvocationNode($node)) {

      foreach (NodeFacade::getSymbols($node) as $symbol) {

        if (!isset(context('code')->symbols[$symbol]))
          continue;

        // @todo: Implement.
        if ($symbol == 'c_closure' || $symbol == 'c_closure()')
          continue;

        foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {

          if ($definitionNode->getAttribute('isSpecializationTemp', false))
            continue;

          $specializationType = $symbol . TemplateSpecialization::specializationSignature($node);
          $specializationSymbol = str_replace('.', '__', $specializationType);

          // @todo: Remove.
          $specializationSymbol .= '_' . $definitionNode->getAttribute('startLine');


          $definitionDefinitionNode = null;
          $contextSymbol = Scope::contextScope(Symbol::parent($symbol));
          $contextSymbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $contextSymbol);

          if (isset(context('code')->symbols[$contextSymbol])) {
            foreach (context('code')->symbols[$contextSymbol]['definitionNodes'] as $potentialDefinitionNode)
              NodeTraverser::traverse([$potentialDefinitionNode], [function ($existingNode) use (&$definitionDefinitionNode, $definitionNode, $potentialDefinitionNode) {
                if ($existingNode === $definitionNode)
                  $definitionDefinitionNode = $potentialDefinitionNode;
              }]);
          }

          if ($contextSymbol && !$definitionDefinitionNode)
            continue;

          if ($definitionDefinitionNode && count($definitionDefinitionNode->getAttribute('types', [])) == 0 && !($definitionDefinitionNode instanceof Node\Stmt\Class_))
            continue;

          if ($definitionDefinitionNode && count($definitionDefinitionNode->getAttribute('types', [])) == 1) {
            $specializationType = $definitionDefinitionNode->getAttribute('types', [])[0] . '.' . Symbol::unqualified($symbol) . TemplateSpecialization::specializationSignature($node);
            $specializationSymbol = str_replace('.', '__', $specializationType);

            // @todo: Remove.
            $specializationSymbol .= '_' . $definitionNode->getAttribute('startLine');

          }

          $node->setAttribute('invocationTypes', [$specializationType]);

          if (!isset(context('code')->symbols[$specializationSymbol])) {

            context('code')->symbols[$specializationSymbol] = [
              'id' => $specializationSymbol,
              'phpId' => isset(context('code')->symbols[$symbol]) ? context('code')->symbols[$symbol]['phpId'] : $specializationSymbol,
              'aliasOf' => '',
              'definitionNodes' => [],
            ];

            /**
             * Specializing functions without a body makes no difference as they
             * behave the same - they don't do anything.
             * This is only for optimization purposes.
             */
            if (false && count($definitionNode->stmts) == 0)
              $specializationNode = $definitionNode;
            else {

              assert(NodeConcept::isExecutionContextNode($definitionNode));

              $specializationNode = TemplateSpecialization::generateSpecialization($node, $definitionNode);

              $specializationNode->setAttribute('types', [$specializationType]);

              $specializationNode->setAttribute('isSpecializationTemp', true);

              if ($specializationNode !== $definitionNode)
                context('code')->symbols[$symbol]['definitionNodes'][] = $specializationNode;

              context('code')->symbols[$specializationSymbol]['definitionNodes'][] = $specializationNode;

              Code::infer([$specializationNode]);

              $constraints = [];
              foreach ($specializationNode->stmts as $statement)
                if ($statement->getAttribute('isConstraint', false))
                  $constraints[] = $statement;

              $satisfiesConstraints = true;
              foreach ($constraints as $statement)
                foreach (Evaluation::evaluate($statement->args[0]) as $value)
                  if ($value['type'] == 't_bool' && !$value['value'])
                    $satisfiesConstraints = false;

              if (!$satisfiesConstraints)
                $specializationNode->stmts = $constraints;

            }

          }

        }

      }

    }

  }

  static function isTemplate ($node) {

    assert(NodeConcept::isExecutionContextNode($node));

    $isSpecialization = false;

    foreach ($node->params as $parameter)
      if (count(NodeFacade::getTypes($parameter)) == 0 && count(NodeFacade::getValues($parameter)) <= ($parameter->default ? 1 : 0))
        $isSpecialization = true;

    if (count($node->getAttribute('returnTypes', [])) == 0 && count($node->getAttribute('returnValues', [])) == 0)
      $isSpecialization = true;

    return !$isSpecialization;
  }

  static function specializationSignature ($node) {

    if (NodeConcept::isInvocationNode($node))
      return '(' . implode(', ', array_map(function ($argument) {
        return implode('|', array_merge(NodeFacade::getTypes($argument), array_map(function ($value) {
          return $value['type'] . ':' . $value['value'];
        }, NodeFacade::getValues($argument))));
      }, $node->args)) . ')';

    return '';

  }

  static function generateSpecialization ($invocationNode, $definitionNode) {

    $specializationNode = NodeConcept::deepClone($definitionNode);

    $isSpecialization = false;

    foreach ($specializationNode->params as $parameter)
      if (count(NodeFacade::getTypes($parameter)) == 0 && count(NodeFacade::getValues($parameter)) <= ($parameter->default ? 1 : 0))
        $isSpecialization = true;

    if (count($specializationNode->getAttribute('returnTypes', [])) == 0 && count($specializationNode->getAttribute('returnValues', [])) == 0)
      $isSpecialization = true;

    if ($isSpecialization)
      NodeTraverser::traverse($specializationNode, [function ($specializationNode) {
        $specializationNode->setAttribute('isSpecialization', true);
      }]);

    if (NodeConcept::isExecutionContextNode($specializationNode)) {

        foreach ($invocationNode->args as $offset => $argument) {
          if (isset($specializationNode->params[$offset])) {
            $specializationNode->params[$offset]->setAttribute('types', NodeFacade::getTypes($argument));
            if (count(NodeFacade::getValues($argument)) > 0)
              $specializationNode->params[$offset]->setAttribute('values', NodeFacade::getValues($argument));
            else
              $specializationNode->params[$offset]->setAttribute('values', [['type' => 't_mixed', 'value' => '']]);
          }

        }

    }

    return $specializationNode;

  }

}
