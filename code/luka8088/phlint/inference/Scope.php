<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \PhpParser\Node;
use \Traversable;

/**
 * Scope inference.
 *
 * Two overlapping terms can easily be confusing - `scope` and `context`.
 * Contexts are isolated (and they form a new isolated scope)
 * while other scopes are not isolated.
 *
 * For example:
 *   function x () {
 *     $y = 0;
 *     if (rand(0, 1)) {
 *       $y = 2;
 *     }
 *   }
 *
 * In the example function `x` forms it's own context and variables cannot be
 * implicitly referenced outside of the function.
 * On the other hand, `if` forms it's own scope but it's not isolated
 * and referencing variables that are outside of it can be referenced.
 * Technically in PHP `if` doesn't actually form a new scope, and
 * any initialization/referencing works in the exact same way as it would
 * if the `if` would not be there. Even so these two have been split up
 * here on purpose as some analyses require it.
 */
class Scope {

  function getIdentifier () {
    return 'scope';
  }

  function getPass () {
    return 10;
  }

  protected $scopeStack = [];

  protected function resetState () {
    $this->scopeStack = [new ArrayObject([
      'id' => 'n_',
      'node' => new \luka8088\phlint\SourceNode([]),
      'idCounter' => 0,
      'context' => new ArrayObject([
        'id' => '',
      ]),
      'parent' => null,
    ])];
  }

  function beforeTraverse($nodes) {
      $this->resetState();
      context('code')->scopes['n_'] = end($this->scopeStack);
  }

  function enterNode ($node) {

    /**
     * "&&" condition forms a new scope on the right side as its execution is
     * influenced by the left side.
     *
     * Consider the expression: `$bar = isset($foo) && $foo;`
     *
     * This expression will never produce a "PHP Notice: Undefined variable"
     * and thus a conditional guarantee that `$foo` is defined can be applied
     * to the right side.
     *
     * Also the expression: `isset($foo) && $foo();`
     * Is functionally equal to `if (isset($foo) { $foo(); }`
     *
     * Considering these behaviors it made most sense to make the
     * right side form a new scope.
     */
    if ($node instanceof Node\Expr\BinaryOp\BooleanAnd)
      $node->right->setAttribute('isScope', true);

    if ($node->getAttribute('scope', '') && !NodeConcept::isDefinitionNode($node))
      $this->scopeStack[] = context('code')->scopes[$node->getAttribute('scope', '')];

    if (NodeConcept::isScopeNode($node) || $node->getAttribute('isScope', false)) {

      /**
       * Node\Stmt\Else_ and Node\Stmt\ElseIf_ form as special case because in AST
       * they are inside Node\Stmt\If_ but when looking at their scopes they are
       * independent. For that reason the original (Node\Stmt\If_) scope needs to
       * closed early.
       */
      if (($node instanceof Node\Stmt\Else_) || ($node instanceof Node\Stmt\ElseIf_))
        if (count($this->scopeStack) > 0 && end($this->scopeStack)['node'] instanceof Node\Stmt\If_)
          array_pop($this->scopeStack);

      $scopeNodeId = $this->identifier($node);

      $scope = new ArrayObject([
        'id' => Symbol::concat($node->getAttribute('scope', '') ? $node->getAttribute('scope', '') : end($this->scopeStack)['id'], $scopeNodeId),
        'node' => $node,
        'idCounter' => 0,
        'context' => end($this->scopeStack)['context'],
        'parent' => end($this->scopeStack),
      ]);

      if (NodeConcept::isContextNode($node))
        $scope['context'] = new \ArrayObject([
          'id' => implode('.', array_merge(
            end($this->scopeStack)['context']['id'] ? [end($this->scopeStack)['context']['id']] : [],
            NodeConcept::isContextNode($node) ? [$scopeNodeId] : []
          )),
          'node' => $node,
          'idCounter' => 0,
        ]);

      $this->scopeStack[] = $scope;

      assert($scope['id'] != '');

      // @todo: Enable.
      #assert(!isset(context('code')->scopes[$scope['id']]));

      context('code')->scopes[$scope['id']] = $scope;

    }

  }

  function identifier ($node) {

    $identifier = !($node instanceof Node\Stmt\Catch_) && !NodeConcept::isInvocationNode($node) ? ltrim(Symbol::identifier($node), '.') : '';

    if (!$identifier && $node->getAttribute('scope', ''))
      $identifier = Symbol::unqualified($node->getAttribute('scope', ''));

    if (!$identifier) {
      end($this->scopeStack)['idCounter'] += 1;
      $identifier = 's_' . end($this->scopeStack)['idCounter'];
    }

    return $identifier;

  }

  function leaveNode ($node) {
    if (count($this->scopeStack) > 0 && end($this->scopeStack)['node'] === $node)
      array_pop($this->scopeStack);
  }

  function visitNode ($node) {

    $scope = end($this->scopeStack)['id'];

    if (NodeConcept::isDefinitionNode($node) && !($node instanceof Node\Const_))
      $scope = Scope::parent($scope);

    $node->setAttribute('scope', $scope);

  }

  /**
   * Is it a context scope?
   * Context scopes are scopes of functions, classes, etc. In other words scopes which
   * are isolated in a way that, for example, variables outside of them are not directly
   * accessible.
   *
   * @param string $scope
   * @return bool
   */
  static function isContext ($scope) {
    return in_array(substr('.' . $scope, strrpos('.' . $scope, '.') + 1, 1), ['f', 'c', '']);
  }

  static function parent ($scope) {
    return strpos($scope, '.') === false ? '' : substr($scope, 0, strrpos($scope, '.'));
  }

  /**
   * Return a closest named ancestor scope.
   *
   * @param string $symbol
   * @return string
   */
  static function namedAncestor ($scope) {
    $ancestorScope = $scope;
    while (true) {
      $ancestorScope = Scope::parent($ancestorScope);
      if (!$ancestorScope)
        break;
      if (in_array(substr(Symbol::unqualified($ancestorScope), 0, 1), ['', 'c', 'd', 'f', 'n']))
        return $ancestorScope;
    }
    return '';
  }

  static function symbolScope ($symbol) {
    return ltrim(substr('.' . $symbol, 0, strrpos('.' . $symbol, '.')), '.');
  }

  /**
   * Return context scope for $scope.
   *
   * @param string $scope
   * @return string
   */
  static function contextScope ($scope) {
    $currentScope = $scope;
    while ($currentScope) {
      if (in_array(Symbol::symbolIdentifierGroup($currentScope), ['class', 'function']))
        return $currentScope;
      $currentScope = Scope::parent($currentScope);
    }
    return '';
  }

  /**
   * Return namespace scope for $scope.
   *
   * @param string $scope
   * @return string
   */
  static function namespaceScope ($scope) {
    $currentScope = $scope;
    while ($currentScope) {
      if (Symbol::symbolIdentifierGroup($currentScope) == 'namespace')
        return $currentScope;
      $currentScope = Scope::parent($currentScope);
    }
    return '';
  }

  /**
   * Return scopes which are visible from $scope.
   *
   * @param string $scope
   * @return Traversable
   */
  static function visibleScopes ($scope) {
    $visibleScope = $scope;
    while (true) {
      yield $visibleScope;
      if (Scope::isContext($visibleScope))
        break;
      if (!$visibleScope)
        break;
      $visibleScope = Scope::parent($visibleScope);
    }
  }

  /**
   * Get node scopes.
   * This function is meant to be used by rules and other inferences
   * as it implements some lightweight deduction logic.
   *
   * @param Node $node
   * @return string[]
   */
  static function get (Node $node) {

    $scopes = [];

    if ($node->getAttribute('scope', ''))
      $scopes[] = $node->getAttribute('scope', '');

    return array_unique($scopes);

  }

}
