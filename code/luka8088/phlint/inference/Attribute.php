<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Result;
use \PhpParser\Comment;
use \PhpParser\Node;

/**
 * @see /documentation/attribute/index.md
 */
class Attribute {

  function getIdentifier () {
    return 'attribute';
  }

  function visitNode ($node) {

    $unwrap = function ($comment) {
      if (strpos(ltrim($comment), '//') === 0)
        return substr(ltrim($comment), 2);
      if (strpos(ltrim($comment), '/**') === 0)
        return preg_replace('/(?is)(\A\/\*\*|(?<=\n)[ \t]*\*(?!\/\z)|\*\/\z)/', '', $comment);
      return $comment;
    };

    $toString = function ($comment) {
      if ($comment instanceof Comment) {
        return $comment->getText();
      }
      return $comment;
    };

    $comments = array_map($unwrap, array_map($toString, $node->getAttribute('comments', [])));

    $attributes = [];

    foreach ($comments as $comment) {
      if (preg_match('/(?is)(?<![a-z0-9_\-])@([a-z0-9_\-]*)/', $comment, $match)) {
        $attribute = $match[1];
        if ($attribute == 'var') {
          $variableRegex = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
          $typeRegex = '[a-zA-Z_\x7f-\xff\\\\][a-zA-Z0-9_\x7f-\xff\\\\\[\]]*';
          preg_match(
            '/(?is)(?<![a-z0-9_\-])@([a-z0-9_\-]*)([ \t]+(' . $typeRegex . ')([ \t]+(\$[a-z0-9_\-]*))?)?/',
            $comment,
            $match
          );
          $attribute = [
            'name' => $match[1],
            'arguments' => array_values(array_filter([
              isset($match[3]) ? $match[3] : '',
              isset($match[5]) ? $match[5] : '',
            ])),
          ];
        }
        $attributes[] = $attribute;
      }
    }

    foreach ($node->getAttribute('comments', []) as $commentNode) {

      $commentLine = is_object($commentNode) ? $commentNode->getLine() : 0;

      $comment = $unwrap($toString($commentNode));

      preg_match_all('/
        (?is)
        (?<![a-z0-9_\-])@
        (
          ([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)
          \s*
          ((
            \(
              [^\(\)\@]*(?-1)?
            \)?
            |
            \)
          )*)
          (((?!\n\n)(?!\r\n\r\n)[a-zA-Z0-9_\x7f-\xff\$ \t\r\n])*)?
        )
      /x', $comment, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);

      foreach ($matches as $match) {

        $attributeLine = $commentLine ? $commentLine + count(explode("\n", substr($comment, 0, $match[1][1]))) - 1 : 0;

        /**
         * Position of the "description" for each PHP Documentator attributes.
         *
         * @see https://www.phpdoc.org/docs/latest/references/phpdoc/tags/index.html
         * @see https://www.phpdoc.org/docs/latest/references/phpdoc/types.html
         */
        $phpdocAttributesDescription = [
          'category' => 1,
          'copyright' => 1,
          'deprecated' => 2,
          'example' => 1,
          'param' => 3,
          'property' => 3,
          'property-read' => 3,
          'property-write' => 3,
          'return' => 2,
          'see' => 2,
          'since' => 2,
          'throws' => 2,
          'todo' => 1,
          'uses' => 2,
          'var' => 3,
        ];

        if (in_array($match[2][0], array_keys($phpdocAttributesDescription)) && isset($match[5][0])) {
          $match[1][0] = $match[2][0] . '(' . implode(', ', array_map(function ($argument) {
            return var_export($argument, true);
          }, preg_split('/[ \t\r\n]+/', trim($match[5][0]), $phpdocAttributesDescription[$match[2][0]]))) . ')';
        }

        try {
          $attribute = context('code')->parse('<?php _' . $match[1][0] . ';');
        } catch (\PhpParser\Error $exception) {

          // @todo: Handle and remove.
          continue;

          $exception->setStartLine($attributeLine);
          throw $exception;
        }

        assert(is_array($attribute) && count($attribute) == 1);
        $attribute = $attribute[0];

        $attribute->name->set(substr($attribute->name->toString(), 1));

        if ($node->getAttribute('path', ''))
          $attribute->setAttribute('path', $node->getAttribute('path', ''));

        $attribute->setAttribute(
          'startLine',
          $attributeLine
        );

        $attribute->setAttribute(
          'endLine',
          $attribute->getAttribute('startLine') + $attribute->getAttribute('endLine') - 1
        );

        $attribute->setAttribute('displayPrint', trim($match[0][0]));

        $attributes[] = $attribute;

      }

    }

    $node->setAttribute('attributes', $attributes);

  }

  /**
   * Get node analysis-time known attributes.
   * This function is meant to be used by rules and other inferences
   * as it implements some lightweight deduction logic.
   *
   * @param Node $node
   * @return mixed[]
   */
  static function get ($node) {

    $attributes = [];

    foreach ($node->getAttribute('attributes', []) as $attribute)
      $attributes[] = $attribute;

    return array_values($attributes);

  }

}
