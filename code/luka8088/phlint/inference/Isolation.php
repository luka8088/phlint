<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\inference;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\phpLanguage;
use \luka8088\phlint\Result;
use \PhpParser\Node;

/**
 * Isolation inference.
 *
 * @see /documentation/glossary/isolation.md
 */
class Isolation {

  function getIdentifier () {
    return 'isolation';
  }

  function getDependencies () {
    return [
      'execution',
      'scope',
      'symbolLink',
    ];
  }

  function getPass () {
    return 60;
  }

  protected $contextStack = [];

  protected function resetState () {
    $this->contextStack = [];
  }

  function enterNode ($node) {
    if ($node instanceof Node\Stmt\Function_)
      $this->contextStack[] = $node;
  }

  function leaveNode ($node) {
    if ($node instanceof Node\Stmt\Function_)
      array_pop($this->contextStack);
  }

  function visitNode ($node) {

    if (!$node->getAttribute('isReachable', true))
      return;

    if ($node instanceof Node\Expr\FuncCall) {
      $isolationBreaches = [];
      foreach (NodeFacade::getSymbols($node) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode)
            if ($node->getAttribute('invocationTypes', []) == $definitionNode->getAttribute('types', []))
              foreach ($definitionNode->getAttribute('isolationBreaches', []) as $isolationBreach)
                $isolationBreaches[] = $isolationBreach;
      if (count($isolationBreaches) > 0)
        $this->addIsolationBreach(
          $node,
          'Calling non-isolated function *' . NodeConcept::displayPrint($node) . '*.',
          $isolationBreaches
        );
    }

    if ($node instanceof Node\Expr\StaticPropertyFetch)
      $this->addIsolationBreach($node, 'Accessing global variable *' . NodeConcept::displayPrint($node) . '*.');

    if (($node instanceof Node\Expr\Variable) && in_array($node->name, phpLanguage\Fixture::$superglobals))
      $this->addIsolationBreach($node, 'Accessing superglobal *' . NodeConcept::displayPrint($node) . '*.');

    if (($node instanceof Node\Scalar\MagicConst\Dir) ||
        ($node instanceof Node\Scalar\MagicConst\File) ||
        ($node instanceof Node\Scalar\MagicConst\Line))
      $this->addIsolationBreach($node, 'Using magic constant *' . NodeConcept::displayPrint($node) . '*.');

    if (($node instanceof Node\Stmt\Function_) &&
        !($node->getAttribute('isSourceAvailable', false)) &&
        !in_array($node->name, phpLanguage\Fixture::$isolatedPhpFunctions) &&
        !in_array($node->name, phpLanguage\Fixture::$purePhpFunctions))
      $this->addIsolationBreach(
        $node,
        isset(phpLanguage\Fixture::$nonIsolatedPhpFunctionsCause[$node->name]) ?
          phpLanguage\Fixture::$nonIsolatedPhpFunctionsCause[$node->name] :
          'Non-isolated PHP function.'
      );

    if ($node instanceof Node\Stmt\StaticVar)
      $this->addIsolationBreach($node, 'Declaring static variable *' . NodeConcept::displayPrint($node) . '*.');

    if (in_array('__isolationBreach', $node->getAttribute('attributes', []))) {
      preg_match('/(?is)\@__isolationBreach\(\'([^\']*)\'\)/', $node->getAttribute('comments')[0]->getText(), $match);
      if (isset($match[1]))
        $this->addIsolationBreach($node, $match[1]);
    }

  }

  function addIsolationBreach ($node, $message, $causes = []) {
    if (count($this->contextStack) == 0)
      return;
    $contextNode = end($this->contextStack);
    $isolationBreaches = $contextNode->getAttribute('isolationBreaches', []);
    $isolationBreaches[] = [
      'message' => is_string($message) ? Result::expandMessageMeta($message, $node) : $message['message'],
      'causes' => array_merge(is_string($message) ? [] : $message['causes'], $causes),
    ];
    $contextNode->setAttribute('isolationBreaches', Result::mergeMessages($isolationBreaches));
  }

}
