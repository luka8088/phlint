<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\inference\Attribute;
use \luka8088\phlint\inference\Scope;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\phpLanguage;
use \PhpParser\Comment;
use \PhpParser\Node;

class Type {

  function getIdentifier () {
    return 'type';
  }

  function getPass () {
    return [30, 40, 60];
  }

  function getDependencies () {
    return [
      'attribute',
      'scope',
      'symbol',
      'typeLiteral',
    ];
  }

  public $symbolTypes = [];

  function resetState () {
    $this->symbolTypes = [];
  }

  function beforeTraverse () {
    $this->resetState();

    context('code')->symbols['t_mixed'] = [
      'id' => 't_mixed',
      'phpId' => 'mixed',
      'aliasOf' => '',
      'definitionNodes' => [],
    ];

    foreach (phpLanguage\Fixture::$implicitTypeConversions as $type1 => $types2) {
      if (!isset(context('code')->symbols[Symbol::fullyQualifiedIdentifier($type1, 'type')]))
        context('code')->symbols[Symbol::fullyQualifiedIdentifier($type1, 'type')] = [
          'id' => Symbol::fullyQualifiedIdentifier($type1, 'type'),
          'phpId' => $type1,
          'aliasOf' => '',
          'definitionNodes' => [],
        ];
      foreach ($types2 as $type2)
        if (!isset(context('code')->symbols[Symbol::fullyQualifiedIdentifier($type2, 'type')]))
          context('code')->symbols[Symbol::fullyQualifiedIdentifier($type2, 'type')] = [
            'id' => Symbol::fullyQualifiedIdentifier($type2, 'type'),
            'phpId' => $type2,
            'aliasOf' => '',
            'definitionNodes' => [],
          ];
    }

  }

  function visitNode ($node) {

    $this->inferConditionalGuarantees($node);

    $this->inferSymbolTypes($node);

    $this->inferTypes($node);

    self::interTypesDisplay($node);

  }

  function leaveNode ($node) {

    $this->inferReturnTypes($node);

  }

  function inferConditionalGuarantees ($node) {

    foreach (phpLanguage\ConditionalGuarantee::evaluate($node) as $guarantee) {

      if (count($guarantee['includesTypes']) > 0) {

        $types = [];

        foreach ($guarantee['includesTypes'] as $includesTypesNode)
          foreach (NodeFacade::getSymbols($includesTypesNode) as $type)
            $types[] = $type;

        foreach (Scope::get($guarantee['scope']) as $scope)
          foreach (NodeFacade::getSymbols($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            $this->symbolTypes[$scopedSymbol] = $types;
          }

      }

    }

  }

  function inferSymbolTypes ($node) {

    if ($node instanceof Node\Expr\Assign)
      $this->registerSymbolsTypes($node->var, NodeFacade::getTypes($node->expr));

    if ($node instanceof Node\Expr\Assign) {

      if ($node->var instanceof Node\Expr\ArrayDimFetch) {

        if (!$node->var->dim || count($node->var->var->getAttribute('isInitialization', [])) > 0)
        $this->registerSymbolsTypes($node->var->var, array_filter(array_map(function ($type) { if ($type == 't_undefined') return ''; return $type . '[]'; }, NodeFacade::getTypes($node->expr))));

      }

      if ($node->var instanceof Node\Expr\Variable) {
        foreach ($node->var->getAttribute('attributes', []) as $attribute) {
          if (is_array($attribute) && isset($attribute['name']) && $attribute['name'] == 'var' && isset($attribute['arguments'][0])) {
            foreach (Scope::get($node->var) as $scope) {
              $typeSymbol = Symbol::lookupSinglePhpId($attribute['arguments'][0], $scope);
              if ($typeSymbol == 't_string')
                $typeSymbol = 't_autoString';
              if ($typeSymbol == 't_string[]')
                $typeSymbol = 't_autoString[]';
              if ($typeSymbol == 't_int')
                $typeSymbol = 't_autoInteger';
              if ($typeSymbol == 't_int[]')
                $typeSymbol = 't_autoInteger[]';
              if ($typeSymbol)
                $this->registerSymbolsTypes($node->var, [$typeSymbol]);
            }
          }
        }
      }

    }

    if ($node instanceof Node\Expr\ClosureUse)
      $this->registerSymbolsTypes($node, NodeFacade::getTypes($node));

    if ($node instanceof Node\Param)
      $this->registerSymbolsTypes($node, NodeFacade::getTypes($node));

    if ($node instanceof Node\Param)
      foreach (Attribute::get($node) as $attribute) {
        if (is_array($attribute) && isset($attribute['name']) && $attribute['name'] == 'var' && isset($attribute['arguments'][0])) {
          foreach (Scope::get($node) as $scope) {
            $typeSymbol = Symbol::lookupSinglePhpId($attribute['arguments'][0], $scope);
            if ($typeSymbol == 't_string')
              $typeSymbol = 't_autoString';
            if ($typeSymbol == 't_string[]')
              $typeSymbol = 't_autoString[]';
            if ($typeSymbol == 't_int')
              $typeSymbol = 't_autoInteger';
            if ($typeSymbol == 't_int[]')
              $typeSymbol = 't_autoInteger[]';
            if ($typeSymbol)
              $this->registerSymbolsTypes($node, [$typeSymbol]);
          }
        }
      }

    if ($node instanceof Node\Stmt\Foreach_) {
      if ($node->keyVar)
        $this->registerSymbolsTypes($node->keyVar, NodeFacade::getTypes($node->keyVar));
      if ($node->expr && $node->valueVar) {
        $valueTypes = [];
        foreach (NodeFacade::getTypes($node->expr) as $type)
          if (substr($type, -2) == '[]')
            $valueTypes[] = substr($type, 0, -2);
        $this->registerSymbolsTypes($node->valueVar, array_unique(array_merge(NodeFacade::getTypes($node->valueVar), $valueTypes)));
      }
    }

    if ($node instanceof Node\Stmt\Return_) {
      $scope = Scope::contextScope($node->getAttribute('scope', ''));
      if ($scope)
        $this->registerSymbolsTypes([$scope . '.r'], NodeFacade::getTypes($node->expr));
    }

  }

  function inferTypes ($node) {

    if (NodeConcept::isRhsSymbolNode($node))
      $node->setAttribute('types', $this->lookup($node));

    if ($node instanceof Node\Expr\Array_) {
      $itemTypes = [];
      foreach ($node->items as $itemNode) {
        $itemNodeTypes = NodeFacade::getTypes($itemNode);
        if (count($itemNodeTypes) == 0)
          $itemTypes[] = '';
        foreach ($itemNodeTypes as $itemNodeType)
          $itemTypes[] = $itemNodeType;
      }
      $common = Type::common(array_unique($itemTypes));
      if ($common)
        $node->setAttribute('types', [$common . '[]']);
    }

  }

  function inferReturnTypes ($node) {

    if (NodeConcept::isExecutionContextNode($node)) {
      $returnTypes = [];
      if ($node->returnType && Symbol::identifier($node->returnType, 'auto'))
        $returnTypes[] = Symbol::identifier($node->returnType, 'auto');
      foreach ($this->lookup(array_map(function ($symbol) { return $symbol . '.r'; }, NodeFacade::getSymbols($node))) as $returnType)
        $returnTypes[] = $returnType;
      $node->setAttribute('returnTypes', array_unique($returnTypes));
    }

  }

  function registerSymbolsTypes ($symbols, $types) {

    if ($symbols instanceof Node)
      $symbols = NodeFacade::getSymbols($symbols);

    foreach ($symbols as $symbol) {

      if (!$symbol)
        continue;

      $this->symbolTypes[$symbol] = [];

      foreach (Symbol::visibleScopes($symbol) as $scopeSymbol) {
        if (!isset($this->symbolTypes[$scopeSymbol]))
          $this->symbolTypes[$scopeSymbol] = [];
        $this->symbolTypes[$scopeSymbol] = array_unique(array_merge($this->symbolTypes[$scopeSymbol], $types));
      }

    }

  }

  function lookup ($symbols) {

    if (is_object($symbols) && NodeConcept::isInvocationNode($symbols)) {

      $types = [];

      foreach (NodeFacade::getSymbols($symbols) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode)
            if ($symbols->getAttribute('invocationTypes', []) == $definitionNode->getAttribute('types', []))
              if (NodeConcept::isExecutionContextNode($definitionNode)) {
                $types = [];
                if ($definitionNode->returnType && Symbol::identifier($definitionNode->returnType, 'auto'))
                  $types[] = Symbol::identifier($definitionNode->returnType, 'auto');
                foreach ($definitionNode->getAttribute('returnTypes', []) as $type)
                  $types[] = $type;
                foreach ($this->lookup(array_map(function ($symbol) { return $symbol . '.r'; }, NodeFacade::getSymbols($definitionNode))) as $type)
                  $types[] = $type;
                #$this->registerSymbolsTypes($symbols, $types);
              }

      return array_unique($types);

    }

    if ($symbols instanceof Node)
      return $this->lookup(NodeFacade::getSymbols($symbols));

    $types = [];

    foreach ($symbols as $symbol) {
      $unqualifiedSymbol = Symbol::unqualified($symbol);
      foreach (Scope::visibleScopes(Scope::symbolScope($symbol)) as $visibleScope) {
        $lookupSymbol = Symbol::concat($visibleScope, $unqualifiedSymbol);
        if (isset($this->symbolTypes[$lookupSymbol])) {
          foreach ($this->symbolTypes[$lookupSymbol] as $type)
            $types[] = $type;
          break;
        }
      }
    }

    return array_unique($types);

  }

  static $_implicitTypeConversionsMap = [];

  static function implicitTypeConversionsMap () {

    if (count(Type::$_implicitTypeConversionsMap) == 0) {

      $populate = function ($type, $implicitlyConvertibleTypes) use (&$populate) {
        foreach ($implicitlyConvertibleTypes as $implicitlyConvertibleType) {
          Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')] = array_unique(array_merge(
            Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')],
            [Symbol::identifier($implicitlyConvertibleType, 'auto')]
          ));
          if (isset(phpLanguage\Fixture::$implicitTypeConversions[$implicitlyConvertibleType]))
            $populate($type, phpLanguage\Fixture::$implicitTypeConversions[$implicitlyConvertibleType]);
        }
      };

      foreach (phpLanguage\Fixture::$implicitTypeConversions as $type => $implicitlyConvertibleTypes) {
        Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')] = [
          Symbol::identifier($type, 'auto'),
        ];
        $populate($type, $implicitlyConvertibleTypes);
      }

    }

    return Type::$_implicitTypeConversionsMap;

  }

  static function common ($types) {

    $types = array_unique($types);

    if (count($types) == 0)
      return '';

    if (count($types) > count(array_filter($types)))
      return '';

    static $cachex = [];

    if (isset($cachex[serialize($types)]))
      return $cachex[serialize($types)];

    foreach ($types as $t)
      if ($t)
        assert(substr($t, 1, 1) == '_', $t);

    $isCompatibleCandidate = function ($typeCandidate) use ($types) {
      foreach ($types as $type) {
        if (!context('code')->isImplicitlyConvertible($type, $typeCandidate))
          return false;
      }
      return true;
    };

    $compatibleCandidates = [];

    foreach ($types as $type) {
      if ($isCompatibleCandidate($type))
        $compatibleCandidates[] = $type;
      if (isset(Type::implicitTypeConversionsMap()[$type]))
        foreach (Type::implicitTypeConversionsMap()[$type] as $typeCandidate)
          if ($isCompatibleCandidate($typeCandidate))
            $compatibleCandidates[] = $typeCandidate;
    }

    $compatibleCandidates = array_unique($compatibleCandidates);

    $tooGeneralcandidates = [];

    foreach ($compatibleCandidates as $compatibleCandidate1)
      foreach ($compatibleCandidates as $compatibleCandidate2)
        if ($compatibleCandidate1 != $compatibleCandidate2)
          if (context('code')->isImplicitlyConvertible($compatibleCandidate1, $compatibleCandidate2))
            $tooGeneralcandidates[] = $compatibleCandidate2;

    $tooGeneralcandidates = array_unique($tooGeneralcandidates);

    $successfulCandidates = array_values(array_diff($compatibleCandidates, $tooGeneralcandidates));

    if (count($successfulCandidates) > 0) {
      $cachex[serialize($types)] = $successfulCandidates[0];
      return $successfulCandidates[0];
    }

    $cachex[serialize($types)] = '';
    return '';

  }

  /** @internal @test */
  static function unittest_common () {
    context('code', new \luka8088\phlint\Code(), function () {
      assert(Type::common(['t_X', 't_X', 't_X']) == 't_X');
      assert(Type::common(['t_X', 't_Y', 't_Y', 't_X']) == '');
      assert(Type::common(['t_int', 't_float']) == 't_float');
      assert(Type::common(['t_autoInteger', 't_autoFloat']) == 't_autoFloat');
      assert(Type::common(['t_int', 't_int']) == 't_int');
      assert(Type::common(['t_int', 't_string']) == 't_autoString');
      assert(Type::common(['t_int', 't_float', 't_autoInteger', 't_int']) == 't_autoFloat');
    });
  }

  static function interTypesDisplay ($node) {

    $typesDisplay = [];

    foreach (NodeFacade::getTypes($node) as $type) {

      if (isset(context('code')->symbols[$type]) && context('code')->symbols[$type]['phpId']) {
        $typesDisplay[] = context('code')->symbols[$type]['phpId'];
        continue;
      }

      $typesDisplay[] = str_replace('t_', '', $type);

    }

    if (count($typesDisplay) > 0)
      $node->setAttribute('typesDisplay', array_unique($typesDisplay));

  }

  static function isCompatibleInvocation ($invocationType, $declarationType) {

    if (($invocationType instanceof Node\Expr\FuncCall) && ($declarationType instanceof Node\Stmt\Function_)) {

      foreach ($declarationType->params as $index => $parameterNode) {

        if (!$parameterNode->default && !isset($invocationType->args[$index]))
          return false;

        if (!isset($invocationType->args[$index]))
          continue;

        foreach (NodeFacade::getTypes($invocationType->args[$index]) as $argumentType)
          foreach (NodeFacade::getTypes($parameterNode) as $parameterType)
            if (!context('code')->isImplicitlyConvertible($argumentType, $parameterType))
              return false;

        #var_dump(NodeFacade::getTypes($parameter));
        #var_dump(NodeFacade::getTypes($invocationType->args[$index]));
        #exit;
      }

      return true;
    }

    #var_dump('x');
    #exit;

    return false;

  }

}
