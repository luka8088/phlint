<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\inference;
use \luka8088\phlint\inference\Scope;
use \luka8088\phlint\inference\SymbolLink;
use \luka8088\phlint\Internal;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeFacade;
use \luka8088\phlint\phpLanguage;
use \PhpParser\Node;

class Symbol {

  function getIdentifier () {
    return 'symbol';
  }

  function getDependencies () {
    return [
      'scope',
      'symbolLink',
    ];
  }

  function getPass () {
    return 20;
  }

  protected $symbolVisibilityMap = [];

  function resetState () {
    $this->symbolVisibilityMap = [];
  }

  function beforeTraverse ($nodes) {
      $this->resetState();
  }

  function visitNode ($node) {

    if (NodeConcept::isDeclarationNode($node) || NodeConcept::isDefinitionNode($node) || NodeConcept::isVariableNode($node))
      $node->setAttribute('symbols', [Symbol::fullyQualifiedIdentifier($node)]);

    if (NodeConcept::isDeclarationNode($node) || NodeConcept::isDefinitionNode($node))
      $this->register(Symbol::fullyQualifiedIdentifier($node), $node);

    $this->inferImport($node);

    assert(!isset(context('code')->symbols['']));

  }

  /**
   * Register import aliases in the current scope and sets the to alias another scope.
   *
   * For example:
   *
   *   namespace A\B {
   *     class C {}
   *   }
   *
   *   namespace D\E {
   *
   *     // Registers 'n_d.n_e.n_a.n_b' as aliasOf 'n_a.n_b'.
   *     // Registers 'n_d.n_e.n_a.c_b' as aliasOf 'n_a.c_b'.
   *     // Either might be valid.
   *     use \A\B;
   *
   *     // Registers 'n_d.n_e.n_a.n_b.n_c' as aliasOf 'n_a.n_b.n_c'.
   *     // Registers 'n_d.n_e.n_a.n_b.c_c' as aliasOf 'n_a.n_b.c_c'.
   *     // Either might be valid.
   *     use \A\B\C;
   *
   *   }
   */
  function inferImport ($node) {

    if ($node instanceof Node\Stmt\Use_) {

      foreach ($node->uses as $useNode) {

        $importTypes = [];

        switch ($useNode->type ? $useNode->type : $node->type) {
          case Node\Stmt\Use_::TYPE_NORMAL:  $importTypes = ['namespace', 'class']; break;
          case Node\Stmt\Use_::TYPE_FUNCTION:  $importTypes = ['function']; break;
          case Node\Stmt\Use_::TYPE_CONSTANT:  $importTypes = ['constant']; break;
          default: assert(false);
        }

        foreach ($importTypes as $importType) {
          $aliasSymbol = Symbol::concat(
            Scope::namespaceScope($node->getAttribute('scope', '')),
            Symbol::identifier($useNode->alias, $importType)
          );
          $this->register($aliasSymbol);
          context('code')->symbols[$aliasSymbol]['aliasOf'] = Symbol::identifier($useNode->name, $importType);
          context('code')->symbols[$aliasSymbol]['phpId'] = $useNode->name->toString();
        }

      }

    }

  }

  function register ($symbol, $node = null) {

    $symbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $symbol);

    assert(is_string($symbol) && $symbol != '');
    assert(strpos($symbol, '.') !== 0);

    if (!isset(context('code')->symbols[$symbol]))
      context('code')->symbols[$symbol] = [
        'id' => $symbol,
        'phpId' => '',
        'aliasOf' => '',
        'definitionNodes' => [],
        'linkedNodes' => [],
      ];

    if ($node) {

      $isNodeAttached = false;
      foreach (context('code')->symbols[$symbol]['definitionNodes'] as $existingDefinitionNode)
        if ($existingDefinitionNode === $node)
          $isNodeAttached = true;
      if (!$isNodeAttached)
        context('code')->symbols[$symbol]['definitionNodes'][] = $node;

      if (!context('code')->symbols[$symbol]['phpId']) {

        $phpId = context('code')->symbols[$symbol]['phpId'];

        /**
         * Added only for optimization purposes.
         * Namespaces in PHP are always top level.
         */
        if (!$phpId && ($node instanceof Node\Stmt\Namespace_))
          $phpId = Symbol::name($node);

        if (!$phpId) {
          $namedScope = $symbol;
          while ($namedScope) {
            $namedScope = Scope::namedAncestor($namedScope);
            if (isset(context('code')->symbols[$namedScope]))
              break;
          }
          $phpId = ($namedScope ? context('code')->symbols[$namedScope]['phpId'] : '') . '\\' . Symbol::name($node);
        }

        context('code')->symbols[$symbol]['phpId'] = ltrim($phpId, '\\');

      }

    }

  }

  function registerPhpId ($symbol, $node) {

    assert(false);

    assert(is_string($symbol) && $symbol != '');

    if (!isset(context('code')->symbols[$symbol]))
      context('code')->symbols[$symbol] = [
        'id' => $symbol,
        'phpId' => '',
        'aliasOf' => '',
        'definitionNodes' => [],
      ];

    $phpId = '';

    /**
     * Added only for optimization purposes.
     * Namespaces in PHP are always top level.
     */
    if (!$phpId && ($node instanceof Node\Stmt\Namespace_))
      $phpId = Symbol::name($node);

    if (!$phpId) {
      $namedScope = $symbol;
      while ($namedScope) {
        $namedScope = Scope::namedAncestor($namedScope);
        if (isset(context('code')->symbols[$namedScope]))
          break;
      }
      $phpId = ($namedScope ? context('code')->symbols[$namedScope]['phpId'] : '') . '\\' . Symbol::name($node);
    }

    context('code')->symbols[$symbol]['phpId'] = ltrim($phpId, '\\');

  }

  static function name ($node) {

    if (is_string($node))
      return $node;

    if ($node instanceof Node\Name)
      return $node->toString();

    if ($node instanceof Node\Stmt\Class_)
      return Symbol::name($node->name);

    if ($node instanceof Node\Expr\FuncCall)
      return Symbol::name($node->name);

    if ($node instanceof Node\Expr\Variable)
      return '$' . Symbol::name($node->name);

    if ($node instanceof Node\Stmt\StaticVar)
      return '$' . Symbol::name($node->name);

    if ($node instanceof Node\Expr\ClosureUse)
      return '$' . Symbol::name($node->var);

    if ($node instanceof Node\Stmt\Interface_)
      return Symbol::name($node->name);

    if ($node instanceof Node\Stmt\Namespace_)
      return Symbol::name($node->name);

    return '';

  }

  static function fullyQualifiedIdentifier ($node, $symbolIdentifierGroup = '', $scope = '') {

    $identifier = Symbol::identifier($node, $symbolIdentifierGroup);

    $isRelative = function ($identifier) { return strpos($identifier, '.') !== 0; };

    if ($isRelative($identifier)) {
      if (!$scope && ($node instanceof Node))
        $scope = $node->getAttribute('scope', '');
      if ($scope == 'n_')
        $scope = '';
      $identifier = '.' . ($scope ? $scope . '.' : '') . $identifier;
    }

    return substr($identifier, 1);

  }

  /**
   * Generates a static symbol identifier from a Node or a node name.
   *
   * @param Node|string $node Node or a node name.
   * @param string $group If $node is a `Node\Name` it is not always possible
   *   to determinate the symbol identifier group solely based on it. That is
   *   why this argument can be used for hinting.
   *
   * @see /documentation/glossary/symbolIdentifierGroup.md
   */
  static function identifier ($node, $group = '') {

    if (is_string($node)) {
      if (strpos($node, '\\') !== false && in_array($group, ['class', 'constant', 'function', 'namespace'])) {
        $namespace = array_map(function ($part) {
          return Symbol::identifier($part, 'namespace');
        }, array_slice(explode('\\', trim($node, '\\')), 0, -1));
        $construct = array_map(function ($part) use ($group) {
          return Symbol::identifier($part, $group);
        }, array_slice(explode('\\', trim($node, '\\')), -1));
        return (strpos($node, '\\') === 0 ? '.' : '') . implode('.', array_merge($namespace, $construct));
      }
      if ($group == 'auto') {
        if (in_array(strtolower(str_replace('[]', '', $node)), phpLanguage\Fixture::$typeDeclarationNonClassKeywords))
          return Symbol::identifier($node, 'type');
        // @todo: Rewrite
        if (in_array(str_replace('[]', '', $node), ['autoFloat', 'autoString', 'autoInteger']))
          return Symbol::identifier($node, 'type');
        return Symbol::identifier($node, 'class');
      }
      if ($group == 'class')
        return 'c_' . strtolower($node);
      if ($group == 'constant')
        return 'd_' . $node;
      if ($group == 'function')
        return 'f_' . strtolower($node);
      if ($group == 'namespace')
        return 'n_' . strtolower($node);
      if ($group == 'type')
        if (strtolower(str_replace('[]', '', $node)) == strtolower('autoFloat'))
          return 't_autoFloat';
        else if (strtolower(str_replace('[]', '', $node)) == strtolower('autoString'))
          return 't_autoString';
        else if (strtolower(str_replace('[]', '', $node)) == strtolower('autoInteger'))
          return 't_autoInteger';
        else
          return 't_' . strtolower($node);
      if ($group == 'variable') {
        if (in_array($node, phpLanguage\Fixture::$superglobals))
          return '.v_' . $node;
        return 'v_' . $node;
      }
      return '';
    }

    #if ($node instanceof \luka8088\phlint\SourceNode)
    #  return 'g';

    if ($node instanceof Node\Const_)
      return Symbol::identifier($node->name, 'constant');

    if ($node instanceof Node\Expr\Closure)
      return Symbol::identifier('anonymous_' . sha1(NodeConcept::sourcePrint($node)), 'function');

    if ($node instanceof Node\Expr\ClosureUse)
      return Symbol::identifier($node->var, 'variable');

    if ($node instanceof Node\Expr\ConstFetch)
      return Symbol::identifier($node->name, 'constant');

    /**
     * Symbol invocations share the symbol identifier with the invoked symbol.
     * In expression `$x = $x;` both symbols have the same identifier.
     * Hence `function x () {}` has the same symbol identifier as `x();`.
     */
    if ($node instanceof Node\Expr\FuncCall)
      return Symbol::identifier($node->name, 'function');

    if ($node instanceof Node\Expr\New_)
      return Symbol::identifier($node->class, 'class');

    if ($node instanceof Node\Expr\StaticCall)
      return Symbol::identifier($node->class, 'class') . '.' . Symbol::identifier($node->name, 'function');

    if ($node instanceof Node\Expr\StaticPropertyFetch)
      return Symbol::identifier($node->class, 'class') . '.' . Symbol::identifier($node->name, 'variable');

    if ($node instanceof Node\Expr\Variable)
      return Symbol::identifier($node->name, 'variable');

    if ($node instanceof Node\Name) {
      if ($node instanceof Node\Name\FullyQualified)
        return '.' . Symbol::identifier(new Node\Name($node->parts), $group);
      $namespace = array_map(function ($part) {
        return Symbol::identifier($part, 'namespace');
      }, array_slice($node->parts, 0, -1));
      $construct = array_map(function ($part) use ($group) {
        return Symbol::identifier($part, $group);
      }, array_slice($node->parts, -1));
      return implode('.', array_merge($namespace, $construct));
    }

    if ($node instanceof Node\Param)
      return Symbol::identifier($node->name, 'variable');

    if ($node instanceof Node\Stmt\Catch_)
      return Symbol::identifier($node->var, 'variable');

    if ($node instanceof Node\Stmt\Class_)
      return Symbol::identifier($node->name, 'class');

    if ($node instanceof Node\Stmt\ClassMethod)
      return Symbol::identifier($node->name, 'function');

    if ($node instanceof Node\Stmt\Function_)
      return Symbol::identifier($node->name, 'function');

    if ($node instanceof Node\Stmt\Interface_)
      return Symbol::identifier($node->name, 'class');

    if ($node instanceof Node\Stmt\Namespace_)
      return '.' . Symbol::identifier($node->name ? $node->name : '', 'namespace');

    if ($node instanceof Node\Stmt\PropertyProperty)
      return Symbol::identifier($node->name, 'variable');

    if ($node instanceof Node\Stmt\StaticVar)
      return Symbol::identifier($node->name, 'variable');

    if ($node instanceof Node\Stmt\Trait_)
      return Symbol::identifier($node->name, 'class');

    if ($node instanceof Node\Stmt\UseUse)
      return Symbol::identifier($node->name, $group);

    return '';

  }

  static function symbolIdentifierGroup ($symbol) {
    $symbol = Symbol::unqualified($symbol);
    if (strpos($symbol, 'c') === 0)
      return 'class';
    if (strpos($symbol, 'd') === 0)
      return 'constant';
    if (strpos($symbol, 'f') === 0)
      return 'function';
    if (strpos($symbol, 'n') === 0)
      return 'namespace';
    if (strpos($symbol, 'r') === 0)
      return '';
    if (strpos($symbol, 's') === 0)
      return '';
    if (strpos($symbol, 't') === 0)
      return 'type';
    if (strpos($symbol, 'v') === 0)
      return 'variable';
    assert(false);
  }

  /**
   * @see /documentation/glossary/symbolIdentifierGroup.md
   * @see http://www.php.net/manual/en/language.namespaces.fallback.php
   */
  static function lookup ($node, $symbolIdentifierGroup = '', $scope = '') {

    assert($node instanceof Node);

    if (($node instanceof Node) && !$scope)
      $scope = $node->getAttribute('scope', '');

    if ($scope == 'n_')
      $scope = '';

    $symbols = [];

    if ($node instanceof Node\Arg)
      return Symbol::lookup($node->value, $symbolIdentifierGroup, $scope);

    if ($node instanceof Node\Expr\FuncCall) {
      #var_dump(Symbol::lookup($node->name, $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'));
      return Symbol::lookup($node->name, $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function');
    }

    if ($node instanceof Node\Expr\MethodCall) {

      $containerSymbols = [];

      foreach (NodeFacade::getValues($node->var) as $containerValue) {
        if ($containerValue['type'] == 't_mixed')
          continue;
        if ($containerValue['type'] == '' && $containerValue['value'] === null) {
          $containerSymbols[] = 't_mixed';
          continue;
        }
        $containerSymbols[] = $containerValue['type'];
      }

      foreach (NodeFacade::getTypes($node->var) as $containerType) {
        if ($containerType == 't_mixed')
          continue;
        $containerSymbols[] = $containerType;
      }

      #var_dump(NodeFacade::getTypes($node->var));

      #var_dump(Symbol::lookup($node->var, 'class', $scope));
      #var_dump($node->class, 'class', $scope);
      #var_dump(Symbol::lookup($node->class, 'class', $scope));
      #foreach (NodeFacade::getTypes($node->var) as $container)
      foreach ($containerSymbols as $container)
        foreach (NodeFacade::getValues($node->name) as $member) {

          #var_dump($container);

          $memberSymbol = Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function');

          #var_dump($memberSymbol);

          #var_dump(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));
          #var_dump($member);
          #var_dump(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));
          #$symbol = Symbol::lookupSingle(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));

          $symbol = Symbol::lookupSingle(Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'), $container);

          // @todo: Remove
          if (strpos($container, 't_') === 0 || $symbol == Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'))
            $symbol = Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'));

          #var_dump($symbol);

          if (!$symbol)
            $symbol = Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'));

          #var_dump($symbol);
          #var_dump($container);
          #var_dump(context('code')->symbols[$container]['phpId']);
          #var_dump((isset(context('code')->symbols[$container]) ? context('code')->symbols[$container]['phpId'] : $container) . '::' . $member['value']);

          // @todo: Rewrite
          if (!isset(context('code')->symbols[$symbol]))
            context('code')->symbols[$symbol] = [
              'id' => $symbol,
              'phpId' => (isset(context('code')->symbols[$container]) ? context('code')->symbols[$container]['phpId'] : $container) . '::' . $member['value'],
              'aliasOf' => '',
              'definitionNodes' => [],
            ];

          $symbols[] = $symbol;
        }
    }

    if ($node instanceof Node\Expr\StaticCall) {
      #var_dump($node->class, 'class', $scope);
      #var_dump(Symbol::lookup($node->class, 'class', $scope));
      #foreach (Symbol::lookup($node->class, 'class', $scope) as $container)

      $containerSymbols = [];

      if ($node->class instanceof Node\Name) {
        #$symbol = Symbol::lookupSingle(Symbol::identifier($node->class, 'class'), $node->class->getAttribute('scope', ''));
        #if ($symbol)
        #  $containerSymbols[] = $symbol;
        foreach (Symbol::lookup($node->class, 'class') as $symbol)
          $containerSymbols[] = $symbol;
      }

      foreach (NodeFacade::getValues($node->class) as $containerValue) {
        Symbol::autoload($containerValue['value']);
        $containerSymbols[] = Symbol::identifier($containerValue['value'], 'class');
      }

      #var_dump($node->class);
      #exit;

      foreach ($containerSymbols as $container) {
      #foreach (NodeFacade::getValues($node->class) as $classValue) {
        #$container = Symbol::identifier($classValue['value'], 'class');
        foreach (NodeFacade::getValues($node->name) as $member) {

          $memberSymbol = Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function');

          #var_dump(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));
          #var_dump($member);
          #var_dump(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));
          #$symbol = Symbol::lookupSingle(Symbol::concat($container, Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function')));

          $symbol = Symbol::lookupSingle(Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'), $container);

          if (!$symbol)
            $symbol = Symbol::concat(ltrim($container, '.'), Symbol::identifier($member['value'], $symbolIdentifierGroup ? $symbolIdentifierGroup : 'function'));

          // @todo: Rewrite
          if (!isset(context('code')->symbols[$symbol]))
            context('code')->symbols[$symbol] = [
              'id' => $symbol,
              'phpId' => (isset(context('code')->symbols[$container]) ? context('code')->symbols[$container]['phpId'] : $container) . '::' . $member['value'],
              'aliasOf' => '',
              'definitionNodes' => [],
            ];

          $symbols[] = $symbol;
        }
      }
    }

    if ($node instanceof Node\Expr\Variable) {
      if (in_array($symbolIdentifierGroup, ['function'])) {
        foreach (NodeFacade::getTypes($node) as $type) {
          if ($type == 't_string')
            continue;
          if ($type == 't_mixed')
            continue;
          $symbols[] = $type;
        }
      }
      if (in_array($symbolIdentifierGroup, ['class', 'constant', 'function', 'namespace', 'type'])) {
        #var_dump(NodeFacade::getValues($node));
        foreach (NodeFacade::getValues($node) as $value) {

          if ($value['type'] == 't_mixed')
            continue;

          $symbols[] = Symbol::identifier($value['value'], $symbolIdentifierGroup);
          #var_dump($symbols);

          $symbol = Symbol::identifier($value['value'], $symbolIdentifierGroup);

          // @todo: Rewrite
          if (!isset(context('code')->symbols[$symbol]))
            context('code')->symbols[$symbol] = [
              'id' => $symbol,
              'phpId' => $value['value'],
              'aliasOf' => '',
              'definitionNodes' => [],
            ];

        }
      }
    }

    if ($node instanceof Node\Name) {

      $namespaceSymbol = Scope::namespaceScope($scope);

      Symbol::autoload(ltrim((isset(context('code')->symbols[$namespaceSymbol]) ? context('code')->symbols[$namespaceSymbol]['phpId'] : $namespaceSymbol) . '\\' . Symbol::name($node), '\\'));

      #var_dump($node, Symbol::identifier($node, $symbolIdentifierGroup), $scope);
      $symbol = Symbol::lookupSingle(Symbol::identifier($node, $symbolIdentifierGroup), $scope, $node->toString());
      #var_dump($symbol);
      #if ($symbol)
      #  return [$symbol];
      #return [];

      if (!$symbol) {

        $namespaceSymbol = Scope::namespaceScope($scope);
        #$symbol = Symbol::fullyQualifiedIdentifier($node, $symbolIdentifierGroup);
        $symbol = Symbol::concat($namespaceSymbol, Symbol::identifier($node, $symbolIdentifierGroup));

        // @todo: Rewrite
        if (!isset(context('code')->symbols[$symbol]))
          context('code')->symbols[$symbol] = [
            'id' => $symbol,
            'phpId' => ltrim((isset(context('code')->symbols[$namespaceSymbol]) ? context('code')->symbols[$namespaceSymbol]['phpId'] : $namespaceSymbol) . '\\' . Symbol::name($node), '\\'),
            'aliasOf' => '',
            'definitionNodes' => [],
          ];

        #Symbol::autoload(context('code')->symbols[$symbol]['phpId']);

      }

      $symbols[] = $symbol;
    }

    if (NodeConcept::isValueLiteral($node))
      foreach (NodeFacade::getValues($node) as $value)
        $symbols[] = Symbol::identifier($value['value'], $symbolIdentifierGroup);

    return $symbols;

  }

  static function lookupSingle ($symbol, $scope = '', $phpId = '') {

    if (!$symbol)
      return '';

    if (Symbol::isAbsolute($symbol))
      return Symbol::lookupSingle(substr($symbol, 1), '', $phpId);

    if (substr($symbol, 0, 2) == 't_')
      return $symbol;

    if ($scope == 'n_')
      $scope = '';

    $symbolIdentifierGroup = Symbol::symbolIdentifierGroup($symbol);

    #if (false)
    if (!$scope) {
      #var_dump($node . ' -> ' . context('code')->symbols[$node]['phpId'] . ' / ' . $symbolIdentifierGroup);
      #if (in_array($symbolIdentifierGroup, ['class']))
      #  Symbol::autoload(context('code')->symbols[$node]['phpId']);
      #var_dump($symbol);
      #var_dump((isset(context('code')->symbols[$symbol]) && count(context('code')->symbols[$symbol]['definitionNodes']) > 0));
      #var_dump(array_keys(context('code')->symbols));
      if (substr($symbol, 0, 2) == 't_')
        return $symbol;
      if ($phpId)
        Symbol::autoload($phpId);
      if (isset(context('code')->symbols[$symbol]))
        Symbol::autoload(context('code')->symbols[$symbol]['phpId']);
      if (isset(context('code')->symbols[$symbol]) && count(context('code')->symbols[$symbol]['definitionNodes']) > 0)
        return $symbol;
      return '';
    }

    /**
     * Functions and constants fallback to global only.
     * As documented in http://ie2.php.net/manual/en/language.namespaces.fallback.php#example-258
     */
    if (in_array(Symbol::symbolIdentifierGroup($symbol), ['constant', 'function']) && !in_array(Symbol::symbolIdentifierGroup($scope), ['class']))
      #var_dump($symbol, $scope);
      foreach ([Symbol::concat($scope, $symbol), $symbol] as $potentialSymbol) {
        $linkedSymbol = Symbol::lookupSingle($potentialSymbol);
        if ($linkedSymbol)
          return $linkedSymbol;
      }

    if (in_array(Symbol::symbolIdentifierGroup($symbol), ['constant', 'function', 'variable'])) {

      #$containerSymbol = Symbol::parent($symbol);
      #$memberSymbol = Symbol::unqualified($symbol);

      $containerSymbol = $scope;
      $memberSymbol = $symbol;

      if (in_array(Symbol::symbolIdentifierGroup($containerSymbol), ['class'])) {
        $linkedSymbol = Symbol::lookupSingle(Symbol::concat($containerSymbol, $memberSymbol));
        if ($linkedSymbol)
          return $linkedSymbol;
      }

      if (isset(context('code')->symbols[$containerSymbol]))
        foreach (context('code')->symbols[$containerSymbol]['definitionNodes'] as $containerNode) {

          foreach ($containerNode->getAttribute('traits', []) as $traitSymbol) {

            $linkedSymbol = Symbol::lookupSingle(Symbol::concat($traitSymbol, $memberSymbol));
            if ($linkedSymbol)
              return $linkedSymbol;
          }

          if (isset($containerNode->extends))
            foreach (is_array($containerNode->extends) ? $containerNode->extends : [$containerNode->extends] as $extendsNode) {

              $simbolx = Symbol::fullyQualifiedIdentifier($extendsNode, 'class', $extendsNode->getAttribute('scope', ''));

              // @todo: Rewrite
              if (!isset(context('code')->symbols[$simbolx]))
                context('code')->symbols[$simbolx] = [
                  'id' => $simbolx,
                  'phpId' => ltrim((!Symbol::isAbsolute(Symbol::identifier($extendsNode, 'class')) ? (isset(context('code')->symbols[$extendsNode->getAttribute('scope', '')]) ? context('code')->symbols[$extendsNode->getAttribute('scope', '')]['phpId'] : $extendsNode->getAttribute('scope', '')) : '') . '\\' . $extendsNode->toString(), '\\'),
                  'aliasOf' => '',
                  'definitionNodes' => [],
                ];

              if (isset(context('code')->symbols[$simbolx]))
                Symbol::autoload(context('code')->symbols[$simbolx]['phpId']);

              $extendsSymbol = Symbol::lookupSingle(Symbol::identifier($extendsNode, 'class'), $extendsNode->getAttribute('scope', ''), $extendsNode->toString());
              if ($extendsSymbol) {
                $linkedSymbol = Symbol::lookupSingle($memberSymbol, $extendsSymbol);
                if ($linkedSymbol)
                  return $linkedSymbol;
              }

              if (false)
              foreach (Symbol::lookup($extendsNode, 'class', Scope::parent($containerNode->getAttribute('scope', ''))) as $extendsSymbol) {
                $linkedSymbol = Symbol::lookupSingle(Symbol::concat($extendsSymbol, $memberSymbol));
                if ($linkedSymbol)
                  return $linkedSymbol;
              }
            }
        }
    }


    if (in_array(Symbol::symbolIdentifierGroup($symbol), ['class', 'namespace'])) {

      $namespaceScope = Scope::namespaceScope($scope);

      if ($phpId)
        Symbol::autoload(($namespaceScope ? trim(context('code')->symbols[$namespaceScope]['phpId'], '\\') . '\\' : '') . trim($phpId, '\\'));

      $linkedSymbol = Symbol::lookupSingle(Symbol::concat(Scope::namespaceScope($scope), $symbol));
      if ($linkedSymbol)
        return $linkedSymbol;

      /**
       * Alias to import is a symbol in the current scope that represents an alias
       * to a symbol in another scope that needs to be imported.
       *
       * See also Symbol::inferImport()
       *
       * For example:
       *
       *   namespace A\B {
       *     use \C\D as E;
       *     $x = new E(); // $aliasSymbol = 'n_a.n_b.c_e'; $importedSymbol = 'n_c.c_d';
       *     $x = new E\F(); // $aliasSymbol = 'n_a.n_b.n_e'; $importedSymbol = 'n_c.n_d.c_f';
       *   }
       */
      $aliasSymbol = Symbol::concat($namespaceScope, substr($symbol . '.', 0, strpos($symbol . '.', '.')));

      if (isset(context('code')->symbols[$aliasSymbol])) {

        $importedSymbol = Symbol::concat(
          context('code')->symbols[$aliasSymbol]['aliasOf'],
          substr($symbol, strpos($symbol . '.', '.') + 1)
        );

        $importedPhpId = context('code')->symbols[$aliasSymbol]['phpId'] . '\\' .
          substr($phpId, strpos($phpId . '\\', '\\') + 1);

        $linkedSymbol = Symbol::lookupSingle($importedSymbol, '', $importedPhpId);
        if ($linkedSymbol)
          return $linkedSymbol;

      }
    }

    return '';

  }

  static function lookupSinglePhpId ($phpId, $scope = '') {
    return Symbol::lookupSingle(Symbol::identifier($phpId, 'auto'), $scope, $phpId);
  }

  static function concat ($a, $b, $glue = '.') {
    // @todo: Remove
    $a = $a == 'n_' ? '' : $a;
    return $a . ($a && $b ? $glue : '') . $b;
  }

  static function isAbsolute ($symbol) {
    return substr($symbol, 0, 1) == '.';
  }

  static function isContext ($scope) {
    return strpos($scope, '.') !== false && in_array(substr($scope, strrpos($scope, '.') + 1, 1), ['f', 'c']);
  }

  static function autoload ($symbol) {
    if ($symbol instanceof Node)
      $symbol = $symbol->getAttribute('phpId');
    $symbol = ltrim($symbol, '\\');
    #var_dump('TRY LOAD ..................: ' . $symbol);
    if (isset(context('code')->autoloadLookups[$symbol]))
      return;
    #var_dump('NEW LOAD ..................: ' . $symbol);
    context('code')->autoloadLookups[$symbol] = true;
    foreach (context('code')->autoloaders as $autoloader) {
      $autoloader['autoloader']->__invoke($symbol);
    }
  }

  static function parent ($scope) {
    return strpos($scope, '.') === false ? '' : substr($scope, 0, strrpos($scope, '.'));
  }

  /**
   * Get unqualified symbol identifier.
   * This function is meant to be used by rules and other inferences.
   *
   * @param string $symbol
   * @return string
   */
  static function unqualified ($symbol) {
    return substr('.' . $symbol, strrpos('.' . $symbol, '.') + 1);
  }

  /**
   * Return potential symbols that would be visible if $symbol would be looked up.
   *
   * @param string $symbol
   * @return Traversable
   */
  static function visibleScopes ($symbol) {
    $unqualifiedSymbol = Symbol::unqualified($symbol);
    foreach (Scope::visibleScopes(Scope::symbolScope($symbol)) as $visibleScope)
      yield Symbol::concat($visibleScope, $unqualifiedSymbol);
  }

}
