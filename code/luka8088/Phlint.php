<?php

namespace luka8088;

use \ArrayAccess;
use \luka8088\ExtensionCall;
use \luka8088\ExtensionInterface;
use \luka8088\phlint\Code;
use \luka8088\phlint\Internal;
use \luka8088\phlint\Output;
use \luka8088\phlint\Result;

class Phlint implements ArrayAccess {

  /** @internal */
  protected $extensionInterface = null;

  /** @internal */
  public $parser = null;

  /** @internal */
  public $inferences = [];

  /** @internal */
  public $rules = [];

  /** @internal */
  public $enabledRules = [];

  /** @internal */
  public $code = [];

  static function create () {
    return new static();
  }

  function __construct () {

    $this->extensionInterface = new ExtensionInterface([
      'beginAnalysis' => /**  @\luka8088\ExtensionCall("phlint.analysis.begin") */ function () {},
      'endAnalysis' => /** @ExtensionCall("phlint.analysis.end") */ function () {},
      'analysisIssueFound' => /** @ExtensionCall("phlint.analysis.issueFound") */ function () {},
    ]);

    $this->output = new Output(null);

    $this->registerInference(new \luka8088\phlint\inference\Attribute());
    $this->registerInference(new \luka8088\phlint\inference\Constraint());
    $this->registerInference(new \luka8088\phlint\inference\Execution());
    $this->registerInference(new \luka8088\phlint\inference\ExecutionBarrier());
    $this->registerInference(new \luka8088\phlint\inference\Isolation());
    $this->registerInference(new \luka8088\phlint\inference\Purity());
    $this->registerInference(new \luka8088\phlint\inference\Scope());
    $this->registerInference(new \luka8088\phlint\inference\Symbol());
    $this->registerInference(new \luka8088\phlint\inference\SymbolInitialization());
    $this->registerInference(new \luka8088\phlint\inference\SymbolLink());
    $this->registerInference(new \luka8088\phlint\inference\TemplateSpecialization());
    $this->registerInference(new \luka8088\phlint\inference\Trusted());
    $this->registerInference(new \luka8088\phlint\inference\Type());
    $this->registerInference(new \luka8088\phlint\inference\Value());

    $this->registerRule(new \luka8088\phlint\rule\EnforceIsolatedAttribute());
    $this->registerRule(new \luka8088\phlint\rule\EnforcePureAttribute());
    $this->registerRule(new \luka8088\phlint\rule\EvaluateAsserts());
    $this->registerRule(new \luka8088\phlint\rule\EvaluateConstraints());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitCaseInsensitiveNaming());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitDuplicateArrayDeclarationKeys());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitExit());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitIncompatibleInvocationArguments());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitInvalidTypeDeclaration());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitNonExistingSymbolInvocation());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitRedefining());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitShortOpenTag());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitUnusedImports());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitVariableAppendInitialization());
    $this->registerRule(new \luka8088\phlint\rule\ProhibitVariableVariables());
    $this->registerRule(new \luka8088\phlint\rule\RequireCompatibleConcept());
    $this->registerRule(new \luka8088\phlint\rule\RequireVariableInitialization());

    $this->enableRule('default');

  }

  /**
   * @param $path Path to a file or folder with files to analyze.
   */
  function addPath ($path, $isLibrary = false) {

    $this->code[] = [
      'path' => $path,
      'source' => '',
      #'ast' => null,
      'isLibrary' => $isLibrary,
    ];

    return $this;

  }

  /**
   * @param $source PHP source code to analyze.
   */
  function addSource ($source, $isLibrary = false) {

    $this->code[] = [
      'path' => '',
      'source' => $source,
      #'ast' => null,
      'isLibrary' => $isLibrary,
    ];

    return $this;

  }

  /** @internal */
  public $autoloaders = [];

  function addAutoloader ($autoloader, $isLibrary = false) {
    $this->autoloaders[] = [
      'autoloader' => $autoloader,
      'isLibrary' => $isLibrary,
    ];
    return $this;
  }

  /** @internal */
  public $output = null;

  function addOutput ($output) {
    $this->output = new Output($output);
    return $this;
  }

  /**
   * @param $path Path to a file or folder with files to analyze.
   */
  function analyzePath ($path = '') {

    if ($path) {
      $this->addPath($path);
    }

    $result = $this->_analyze();

    if ($path)
      array_pop($this->code);

    return $result;
  }

  /**
   * @param $source PHP source code to analyze.
   */
  function analyze ($source = '') {

    if ($source) {
      $this->addSource($source);
    }

    $result = $this->_analyze();

    if ($source)
      array_pop($this->code);

    return $result;
  }

  protected function _analyze () {

    $self = $this;

    $this->extensionInterface->beginAnalysis();

    $code = new Code();
    $result = new Result();

    $result->extensionInterface = $this->extensionInterface;

    // Todo: Rewrite
    static $phpStandardCode = [];

    context('output', $this->output, function () use ($self, $code, $result, &$phpStandardCode) {

      context('result', $result, function () use ($self, $code, $result, &$phpStandardCode) {

        context('code', $code, function () use ($self, $code, $result, &$phpStandardCode) {

          $beginTimestamp = microtime(true);

          $rules = [];
          $inferences = [];

          $collectInferences = function ($requiredInferences) use (&$collectInferences, &$inferences, $self) {
            foreach ($self->inferences as $specificInference)
              if (in_array($specificInference->getIdentifier(), $requiredInferences)) {
                if (method_exists($specificInference, 'getDependencies'))
                  $collectInferences($specificInference->getDependencies());
                $hasInference = false;
                foreach ($inferences as $existingInference)
                  if ($existingInference === $specificInference)
                    $hasInference = true;
                if (!$hasInference)
                  $inferences[] = $specificInference;
              }
          };

          foreach ($self->rules as $specificRule)
            if (in_array($specificRule->getIdentifier(), $this->enabledRules)) {
              $hasRule = false;
              foreach ($rules as $existingRule)
                if ($existingRule === $specificRule)
                  $hasRule = true;
              if (!$hasRule)
                $rules[] = $specificRule;
              if (method_exists($specificRule, 'getInferences'))
                $collectInferences($specificRule->getInferences());
            }

          $code->inferences = $inferences;
          $code->autoloaders = $self->autoloaders;

          context('output')->__invoke("Importing php standard definitions ...\n");

          $phpStandardDefinitionsAst = Internal::importDefinitions('standard');

          // Todo: Rewrite
          if (count($phpStandardCode) == 0) {
            context('code', new Code(), function () use ($inferences, &$phpStandardDefinitionsAst, &$phpStandardCode) {
              context('code')->inferences = $inferences;
              Code::infer($phpStandardDefinitionsAst);
              $phpStandardCode = [
                'symbols' => context('code')->symbols,
                'scopes' => context('code')->scopes,
                'interfaceSymbolMap' => context('code')->interfaceSymbolMap,
              ];
            });
          }
          context('code')->symbols = array_merge($phpStandardCode['symbols'], context('code')->symbols);
          context('code')->scopes = array_merge($phpStandardCode['scopes'], context('code')->scopes);
          context('code')->interfaceSymbolMap = array_merge($phpStandardCode['interfaceSymbolMap'], context('code')->interfaceSymbolMap);

          $code->addAst($phpStandardDefinitionsAst, true);

          context('output')->__invoke("Loading code ...\n");

          foreach ($code->autoloaders as $autoloader)
            if (method_exists($autoloader['autoloader'], 'initialize'))
              $autoloader['autoloader']->initialize();

          context('output')->indent();
          $code->load($self->code);
          context('output')->unindent();

          context('output')->__invoke("Analyzing code ...\n");

          #$rules = new \PhpParser\NodeTraverser();

          #foreach ($self->rules as $rule) {
          #  $rules->addVisitor($rule);
          #}

          #foreach ($code->asts as $index => $ast) {
          #  if ($code->astsIsLibrary[$index])
          #    continue;
          #  $rules->traverse($ast);
          #}

          foreach ($code->asts as $index => $ast) {
            if ($code->astsIsLibrary[$index])
              continue;
            Code::analyze($ast, $rules);
          }

          $libraryResult = new Result();

          $libraryResult->extensionInterface = new ExtensionInterface([
            'beginAnalysis' => /**  @ExtensionCall("phlint.analysis.begin") */ function () {},
            'endAnalysis' => /** @ExtensionCall("phlint.analysis.end") */ function () {},
            'analysisIssueFound' => /** @ExtensionCall("phlint.analysis.issueFound") */ function () {},
          ]);

          context('result', $libraryResult, function () use ($self, $code, $result, $rules, &$phpStandardCode) {
            context('output', function () {}, function () use ($self, $code, $result, $rules, &$phpStandardCode) {
              foreach (context('code')->symbols as $symbol => $_)
                foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {
                  if ($definitionNode->getAttribute('isSpecialization', false))
                    continue;
                  $hasSpecializations = count(array_filter(context('code')->symbols[$symbol]['definitionNodes'], function ($definitionNode) {
                    return $definitionNode->getAttribute('isSpecialization', false);
                  })) > 0;
                  if (!$hasSpecializations)
                    continue;
                  Code::analyze([$definitionNode], $rules);
                }
            });
          });
          context('result')->libraryIssues = $libraryResult->getIssues();

          foreach (context('code')->symbols as $symbol => $_)
            foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {
              if (!$definitionNode->getAttribute('isSpecialization', false))
                continue;
              Code::analyze([$definitionNode], $rules);
            }

          #context('output')->__invoke("Result:\n" . $result->toString() . "\n");

          $this->_recordMemoryUsage();

          context('output')->__invoke(
            'Done in ' . number_format((microtime(true) - $beginTimestamp) * 1000, 2) . "ms\n" .
            'Maximum memory usage is ' . number_format($this->maximumMemoryUsage / (1024 * 1024), 2) . "MB\n"
          );

        });

      });

    });

    $this->extensionInterface->endAnalysis();

    return $result;

  }

  public $maximumMemoryUsage = 0;

  protected function _recordMemoryUsage () {
    $this->maximumMemoryUsage = max($this->maximumMemoryUsage, memory_get_usage());
  }

  function registerExtension ($extension) {
    $this->extensionInterface[] = $extension;
  }

  function offsetExists ($offset) {
    assert(false);
  }

  function offsetGet ($offset) {
    assert(false);
  }

  function offsetSet ($offset, $value) {
    assert($offset === null);
    $this->registerExtension($value);
  }

  function offsetUnset ($offset) {
    assert(false);
  }

  function registerInference ($inference) {
    $this->inferences[] = $inference;
  }

  /** @deprecated */
  function addInference ($inference) {
    $this->registerInference($inference);
  }

  function registerRule ($rule) {
    $this->rules[] = $rule;
  }

  function addRule ($rule) {
    $this->registerRule($rule);
    $this->enableRule($rule->getIdentifier());
  }

  function enableRule ($rule) {
    $class = '\\luka8088\\phlint\\rule\\' . ucfirst($rule);
    if (class_exists($class))
      $this->registerRule(new $class());
    foreach ($this->rules as $specificRule)
      if (self::doesRuleMatch($specificRule, $rule))
        $this->enabledRules[] = $specificRule->getIdentifier();
  }

  function disableRule ($rule) {
    foreach ($this->rules as $specificRule)
      if (self::doesRuleMatch($specificRule, $rule))
        $this->enabledRules = array_filter($this->enabledRules, function ($enabledRule) use ($specificRule) {
          return !($enabledRule == $specificRule->getIdentifier());
        });
  }

  static function doesRuleMatch ($specificRule, $rule) {
    return
      $specificRule->getIdentifier() == $rule ||
      (method_exists($specificRule, 'getCategories') && in_array($rule, $specificRule->getCategories())) ||
      $rule == 'all' ||
      false
    ;
  }

  /** @deprecated */
  function removeRule ($rule) {
    $newRules = [];
    foreach ($this->rules as $existingRule) {
      if ($rule === $existingRule)
        continue;
      if (is_a($existingRule, $rule, true))
        continue;
      if (is_a($existingRule, '\\luka8088\\phlint\\rule\\' . ucfirst($rule), true))
        continue;
      $newRules[] = $existingRule;
    }
    $this->rules = $newRules;
  }

}
