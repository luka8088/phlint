<?php

namespace phlint\extension;

use \luka8088\ExtensionCall;

class JUnitResult {

  protected $output = null;

  function __construct ($output) {
    $this->output = $output;
  }

  /** @ExtensionCall("phlint.analysis.begin") */
  function beginAnalysis () {
    rewind($this->output);
    fwrite(
      $this->output,
      '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
      '  <testsuites>' . "\n" .
      '    <testsuite name="Phlint">' . "\n"
    );
  }

  /** @ExtensionCall("phlint.analysis.issueFound") */
  function issueFound ($issue) {
    fwrite(
      $this->output,
      '      <testcase name="' . self::xmlEncode($issue->getMessage()) . '">' . "\n" .
      '        <failure message="' . self::xmlEncode($issue->getMessage()) . '">' .
                self::xmlEncode(rtrim($issue->getCause() . "\n" . $issue->getTraces(), "\n")) .
              '</failure>' . "\n" .
      '      </testcase>' . "\n"
    );
  }

  /** @ExtensionCall("phlint.analysis.end") */
  function endAnalysis () {
    fwrite(
      $this->output,
      '  </testsuite>' . "\n" .
      '</testsuites>' . "\n"
    );
  }

  static function xmlEncode ($string) {
    return htmlspecialchars($string, ENT_QUOTES | ENT_DISALLOWED);
  }

}
