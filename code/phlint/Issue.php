<?php

namespace phlint;

class Issue {

  public $message = '';
  public $node = null;
  public $traces = [];
  public $causes = [];

  function __construct ($message, $node, $traces = [], $causes = []) {
    $this->message = $message;
    $this->node = $node;
    $this->traces = $traces;
    $this->causes = $causes;
  }

  function toString () {
    return self::print($this);
  }

  function getMessage () {
    return self::expandMessageMeta($this);
  }

  function getCause () {
    return self::printCause($this);
  }

  function getTraces () {
    return self::printTraces($this);
  }

  static function print ($message) {
    $printed = self::expandMessageMeta($message);
    foreach (is_object($message) ? $message->causes : $message['causes'] as $offset => $causeMessage)
      $printed .= "\n  " . 'Cause #' . ($offset + 1) . ': ' . str_replace("\n", "\n  ", self::print($causeMessage));
    foreach (is_object($message) ? $message->traces : $message['traces'] as $trace) {
      $printed .= "\n  " . 'Trace:';
      foreach (array_reverse($trace) as $offset => $traceMessage)
        $printed .= "\n    " . '#' . ($offset + 1) . ': ' . self::expandMessageMeta($traceMessage);
    }
    return $printed;
  }

  static function printCause ($message) {
    $printed = '';
    foreach (is_object($message) ? $message->causes : $message['causes'] as $offset => $causeMessage)
      $printed .= "\n  " . 'Cause #' . ($offset + 1) . ': ' . str_replace("\n", "\n  ", self::print($causeMessage));
    return substr($printed, 1);
  }

  static function printTraces ($message) {
    $printed = '';
    foreach (is_object($message) ? $message->traces : $message['traces'] as $trace) {
      $printed .= "\n  " . 'Trace:';
      foreach (array_reverse($trace) as $offset => $traceMessage)
        $printed .= "\n    " . '#' . ($offset + 1) . ': ' . self::expandMessageMeta($traceMessage);
    }
    return substr($printed, 1);
  }

  static function expandMessageMeta ($message) {
    $node = is_object($message) ? $message->node : $message['node'];
    $message = is_object($message) ? $message->message : $message['message'];
    if (isset($node->getAttributes()['path'])) {
      $path = $node->getAttributes()['path'];
      if (realpath($path))
        $path = realpath($path);
      return rtrim($message, '.') . ' in *' . $path . ($node->getLine() > 0 ? ':' . $node->getLine() : '') . '*.';
    }
    if ($node->getLine() > 0)
      return rtrim($message, '.') . ' on line ' . $node->getLine() . '.';
    return $message;
  }

}
