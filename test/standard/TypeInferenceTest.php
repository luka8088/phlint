<?php

use \luka8088\phlint\Test as PhlintTest;

class TypeInferenceTest {

  /**
   * Test type inference through foreach and plus operation.
   * @test @internal
   */
  static function unittest_foreachPlus () {
    PhlintTest::assertIssues('

      function myRangeSum ($values) {
        $result = 0;
        foreach ($values as $value) {
          $result += $value;
        }
        return $result;
      }

      $a = [1, 2.2, "2", 3, "a3.3"];

      $b = [];

      foreach ($a as $x)
        $b[] = $x;

      $r = myRangeSum($b);

      $c = [$r, $a[0], 3];

      $r2 = myRangeSum($c);

    ', [
      '
        Provided variable *$value* of type *autoString* is not compatible in the expression *$result += $value* on line 5.
          Trace #1: Function *myRangeSum(autoString[] $values)* specialized for the expression *myRangeSum($b)* on line 17.
      ',
      '
        Provided variable *$value* of type *autoString* is not compatible in the expression *$result += $value* on line 5.
          Trace #1: Function *myRangeSum(autoString[] $values)* specialized for the expression *myRangeSum($c)* on line 21.
      ',
    ]);
  }

  /**
   * Test merging of types on multiple assign statements.
   * @test @internal
   */
  static function unittest_mergeTypes () {

    PhlintTest::assertIssues('

      class A {
        function foo () { return 1; }
      }

      class B {
        function bar () { return 2; }
      }

      function getObject () {
        if (rand(0, 1))
          $o = new A();
        else
          $o = new B();
        $o->bar();
      }

    ', [
      'Unable to invoke undefined *A::bar* for the expression *$o->bar()* on line 15.',
    ]);

  }

  /**
   * Test that inline function return types do not leak outside of function scope.
   * @test @internal
   */
  static function unittest_typeLeaking () {
    PhlintTest::assertNoIssues('

      function bar () {
        $arr = ["a", "b"];
        usort($arr, function ($a, $b) {
          if ($a === null || $b === null)
            return 0;
          return strcmp($a, $b);
        });
        return $arr;
      }

      foreach (bar() as $v) {}

    ');
  }

}
