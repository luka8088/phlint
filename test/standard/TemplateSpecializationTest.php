<?php

use \luka8088\phlint\autoload\Mock as MockAutoload;
use \luka8088\phlint\Test as PhlintTest;

class TemplateSpecializationTest {

  /**
   * Test that calling a function before its definition causes no issue.
   * @test @internal
   */
  static function unittest_test () {
    PhlintTest::assertNoIssues('
      $x = foo(1);
      function foo ($i) {
        return $i;
      }
    ');
  }

  /**
   * Test that invocations are linked to the correct specializations.
   * @test @internal
   */
  static function unittest_link () {

    PhlintTest::assertNoIssues('

      const X1 = 1;
      const X2 = 2;

      function foo ($x = X1) {
        if ($x === X2)
          $y = $GLOBALS["x"];
      }

      /** @pure */
      function bar () {
        foo();
      }

      bar();

    ');

    PhlintTest::assertNoIssues('

      const X1 = 1;
      const X2 = 2;

      function foo ($x = X1) {
        if ($x === X2)
          $y = $GLOBALS["x"];
      }

      /** @pure */
      function bar () {
        foo();
      }

      /** @pure */
      function baz () {
        foo();
      }

      foo();
      bar();
      baz();

    ');

  }

  /**
   * Test alternative templates.
   * @test @internal
   */
  static function unittest_alternativeTemplates () {
    PhlintTest::assertIssues('
      if (rand(0, 1)) {
        function foo ($bar) {
          $x = 2;
          return $bar + 1;
        }
      } else {
        function foo ($bar) {
          $y = $x;
          return $bar - 1;
        }
      }
      foo(1);
      foo("Hello");
    ', [
      '
        Provided variable *$bar* of type *string* is not compatible in the expression *$bar + 1* on line 4.
          Trace #1: Function *foo("Hello")* specialized for the expression *foo("Hello")* on line 13.
      ',
      'Variable *$x* used before initialized on line 8.',
      '
        Provided variable *$bar* of type *string* is not compatible in the expression *$bar - 1* on line 9.
          Trace #1: Function *foo("Hello")* specialized for the expression *foo("Hello")* on line 13.
      ',
    ]);
  }

  /**
   * Test specialization by return type.
   *
   * @test @internal
   */
  static function unittest_returnTypeSpecialization () {

    $linter = PhlintTest::create();

    $linter->addAutoloader(new MockAutoload([
      'X\Y\A' => '
        namespace X\Y;
        class A extends B {}
      ',
      'X\Y\B' => '
        namespace X\Y;
        class B extends C {
          function foo () {}
        }
      ',
      'X\Y\C' => '
        namespace X\Y;
        class C {
          static function createA () {
            $fun = new A();
            $fun->foo();
            $fun->bar();
            return $fun;
          }
        }
      ',
    ]));

    PhlintTest::assertIssues($linter->analyze('
      namespace Z;
      use \X\Y as I;
      $baz = I\C::createA();
      $baz->foo();
      $baz->bar();
    '), [
      '
        Unable to invoke undefined *X\Y\A::bar* for the expression *$fun->bar()* in *mock:X\Y\C:6*.
          Trace #1: Method *static function createA()* specialized for the expression *I\C::createA()* on line 3.
      ',
      '
        Unable to invoke undefined *X\Y\A::bar* for the expression *$baz->bar()* on line 5.
      ',
    ]);

  }

  /**
   * Test sub-templating.
   *
   * @test @internal
   */
  static function unittest_subTemplating () {
    PhlintTest::assertIssues('
      function foo ($x) {
        return function ($y) use ($x) {
          return $x + $y;
        };
      }
      $intFoo = foo(1);
      $intFoo("world");
      $stringFoo = foo("Hello");
      $stringFoo(1);
    ', [
      '
        Provided variable *$x* of type *string* is not compatible in the expression *$x + $y* on line 3.
      ',
      '
        Provided variable *$y* of type *string* is not compatible in the expression *$x + $y* on line 3.
          Trace #1: Function *("world") use (1)* specialized for the expression *$intFoo("world")* on line 7.
      ',
    ]);
  }

  /**
   * Test sub-template nesting.
   *
   * @test @internal
   */
  static function unittest_subTemplateNesting () {
    PhlintTest::assertIssues('
      function foo ($a) {
        return function ($b) use ($a) {
          return function ($c) use ($a, $b) {
            return function ($d) use ($a, $b, $c) {
              return function ($e) use ($a, $b, $c, $d) {
                return $a + $b + $c + $d + $e;
              };
            };
          };
        };
      }
      $stringFoo = foo("Hello");
      $intFoo = $stringFoo(1);
      $arrayFoo = $intFoo([]);
      $boolFoo = $arrayFoo(false);
      $boolFoo(null);
    ', [
      // @todo: Improve the messages, context and stack are missing at the moment.
      '
        Provided variable *$a* of type *string* is not compatible in the expression *$a + $b* on line 6.
      ',
      '
        Provided variable *$d* of type *bool* is not compatible in the expression *$a + $b + $c + $d* on line 6.
      ',
      '
        Provided variable *$e* of type ** is not compatible in the expression *$a + $b + $c + $d + $e* on line 6.
          Trace #1: Function *(NULL) use ("Hello", 1, $c, false)* specialized for the expression *$boolFoo(null)* on line 16.
      ',
    ]);
  }

}
