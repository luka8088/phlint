<?php

use \luka8088\phlint\Test as PhlintTest;

class SanityTest {

  /**
   * Test that code doesn't break on empty `list` item.
   * @see http://www.php.net/manual/en/function.list.p
   * @test @internal
   */
  static function unittest_emptyListVariable () {
    PhlintTest::assertNoIssues('
      list(, $foo) = explode(" ", "Hello world");
    ');
  }

  /**
   * Test that code doesn't break on empty `return`.
   * @see http://php.net/manual/en/function.return.php
   * @test @internal
   */
  static function unittest_emptyReturn () {
    PhlintTest::assertNoIssues('
      function foo () {
        return;
      }
    ');
  }

  /**
   * Test that code doesn't break on empty ternary operator condition.
   * @see http://ie1.php.net/manual/en/language.operators.comparison.php#language.operators.comparison.ternary
   * @test @internal
   */
  static function unittest_emptyTernaryCondition () {
    PhlintTest::assertNoIssues('
      function foo () {
        $x = true ?: false;
      }
    ');
  }

}
