<?php

use \luka8088\phlint\Test as PhlintTest;

class EvaluateAssertsTest {

  /**
   * Test parameter assertions.
   * @test @internal
   */
  static function unittest_parametersAssertations () {
    PhlintTest::assertIssues('
      function foo ($a, $b) {
        assert(is_numeric($a));
        assert(is_numeric($b));
      }
      foo(1, "a");
    ', [
      '
        Assertion failure for the expression *assert(is_numeric($b))* on line 3.
          Trace #1: Function *foo(1, "a")* specialized for the expression *foo(1, "a")* on line 5.
      ',
    ]);
  }

}
