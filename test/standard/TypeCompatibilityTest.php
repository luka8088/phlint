<?php

use \luka8088\phlint\Test as PhlintTest;

class TypeCompatibilityTest {

  /**
   * Test type compatibility with class and interface inheritance.
   * @test @internal
   */
  static function unittest_classInheritanceCompatibility () {
    PhlintTest::assertIssues('

      class A extends B {}
      class B implements I {}
      class C implements J {}
      interface I extends J {}
      interface J {}

      function foo (I $a) {}

      foo(new A());
      foo(new C());

    ', [
      'Provided argument *new C()* of type *C* is not compatible in the expression *foo(new C())* on line 11.',
    ]);
  }

  /**
   * Test type compatibility with class and interface inheritance on a standard library example.
   * @test @internal
   */
  static function unittest_classInheritanceCompatibilityInStandardLibrary () {
    PhlintTest::assertNoIssues('
      $foo = new DateTime();
      $foo->diff($foo);
    ');
  }

  /**
   * @test @internal
   */
  static function unittest_classInheritanceCompabibilityWithMethodSpecialization () {

    // @todo: Implement;
    return;

    PhlintTest::assertIssues('

      class A extends B {}

      class B implements I {
        function foo (J $x) {}
      }

      class C {
        function foo (C $x) {}
      }

      interface I extends J {}

      interface J {
        function foo (J $x) {}
      }

      function baz ($a, $b) {
        $a->foo($b);
        $a->bar($b);
      }

      baz(new A(), new A());
      baz(new C(), new A());
      baz(new A(), new C());
      baz(new C(), new C());

    ', [
      '
        Unable to invoke undefined *A::bar* for the expression *$a->bar($b)* on line 20.
          Trace #1: Function *baz(A $a, A $b)* specialized for the expression *baz(new A(), new A())* on line 23.
      ',
      '
        Unable to invoke undefined *C::bar* for the expression *$a->bar($b)* on line 20.
          Trace #1: Function *baz(C $a, A $b)* specialized for the expression *baz(new C(), new A())* on line 24.
      ',
      '
        Unable to invoke undefined *A::bar* for the expression *$a->bar($b)* on line 20.
          Trace #1: Function *baz(A $a, C $b)* specialized for the expression *baz(new A(), new C())* on line 25.
      ',
      '
        Unable to invoke undefined *C::bar* for the expression *$a->bar($b)* on line 20.
          Trace #1: Function *baz(C $a, C $b)* specialized for the expression *baz(new C(), new C())* on line 26.
      ',
    ]);
  }

  /**
   * @test @internal
   */
  static function unittest_typeCompatibility () {

    // @todo: Implement.
    return;

    PhlintTest::assertIssues('

      class A {

        function bar () {
          $b = B::getInstance();
          return $b->baz();
        }

      }

      class B {

        public static function getInstance () {
          static $instance = null;
          if (!$instance)
            $instance = new B();
          return $instance;
        }

        function baz () {
          $data = [];
          foreach ([1, 2, 3] as $v)
            $data[] = $v;
          return $data;
        }

      }

      $obj = new A();

      $foo = sprintf("%0.2f", $obj->bar());

    ', [
    ]);
  }

}
