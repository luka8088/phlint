<?php

use \luka8088\phlint\Test as PhlintTest;

class VariableInitializationTest {

  /**
   * That variable initialization for conditional connectives.
   * @test @internal
   */
  static function unittest_conditionalConnectives () {
    PhlintTest::assertIssues('
      function foo () {
        $a = ($b = rand(0, 1)) && ($c = $b);
        $d = $c;
      }
    ', [
      'Variable *$c* used before initialized on line 3.',
    ]);
  }

  /**
   * Regression test for the issue:
   *   Variable *$b* used before initialized on line 2.
   *
   * @test @internal
   */
  static function unittest_parametersInConditions () {
    PhlintTest::assertIssues('
      function foo ($a, $b) {
        if (is_null($a) && is_null($b) && is_null($c)) {}
      }
    ', [
      'Variable *$c* used before initialized on line 2.',
    ]);
  }

  /**
   * Regression test for the issue:
   *   Variable *$b* used before initialized on line 3.
   *
   * @test @internal
   */
  static function unittest_outReferenceInCondition () {
    PhlintTest::assertIssues('
      function foo ($a) {
        if ($a && bar($b) && (rand(0, $z) && ($x = 2) && ($y = 3))) {
          if ($b && $y && $z) {}
        }
      }
      function bar (/** @out */ &$r) {
        $r = 2;
        return true;
      }
    ', [
      'Variable *$z* used before initialized on line 2.',
      'Variable *$z* used before initialized on line 3.',
    ]);
  }

  /**
   * Test that conditionally defining a variable in switch
   * and using it afterwards produces an appropriate issue.
   *
   * @test @internal
   */
  static function unittest_conditionallyDefineVariableInSwitch () {
    PhlintTest::assertIssues('
      switch (rand(0, 1)) {
        case 0:
          $foo = 1;
      }
      $bar = $foo;
    ', [
      'Variable *$foo* used before initialized on line 5.',
    ]);
  }

  /**
   * Regression test for the issue:
   *   Variable *$foo* used before initialized on line 6.
   *
   * @test @internal
   */
  static function unittest_unconditionallyDefineVariableInSwitch () {
    PhlintTest::assertNoIssues('
      switch (rand(0, 1)) {
        case 0:
        default:
          $foo = 1;
      }
      $bar = $foo;
    ');
  }

}
