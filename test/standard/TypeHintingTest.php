<?php

use \luka8088\phlint\Test as PhlintTest;

class TypeHintingTest {

  /**
   * Hinting a type that does not exist.
   * @test @internal
   */
  static function unittest_hintingNonExistingType () {
    PhlintTest::assertIssues('
      function foo () : resource {}
      foo();
    ', [
      'Type declaration requires undefined *resource* on line 1.',
    ]);
  }

}
