<?php

use \luka8088\phlint\autoload\Composer;
use \luka8088\phlint\Test as PhlintTest;

class AutoloadMinimumTest {

  /**
   * Test that only a minimum set of library classes are loaded.
   * This is done for optimization purposes only.
   *
   * @test @internal
   */
  static function unittest_autoloadMinimum () {

    PhlintTest::mockFilesystem(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/', [
      '/vendor/composer/autoload_psr4.php' =>
        '<?php return ' . var_export([
          'A' => [sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/vendor/company/project/A'],
        ], true) . ';',
      '/vendor/company/project/A/B/D.php' => '<?php
        namespace A\B;
        class D extends E {
        }
      ',
      '/vendor/company/project/A/B/E.php' => '<?php
        namespace A\B;
        class E extends F {
          function foo () {}
        }
      ',
      '/vendor/company/project/A/B/F.php' => '<?php
        namespace A\B;
        class F {}
        -<Syntax error!>-
        This file should never be loaded and this syntax error should never be reported.
      ',
    ]);

    $linter = PhlintTest::create();

    $linter->addAutoloader(new Composer(sys_get_temp_dir() . '/nmlkTjEJbQcZF3Sc6KYUCla3/composer.json'));

    PhlintTest::assertIssues($linter->analyze('
      namespace X;
      use \A\B as C;
      $baz = new C\D();
      $baz->foo();
    '), [
    ]);

  }

}
